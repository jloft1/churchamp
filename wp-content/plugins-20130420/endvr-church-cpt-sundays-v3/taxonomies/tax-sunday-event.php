<?php
//hook into the init action and call endvr_create_taxonomy_sundayevent when it fires
add_action( 'init', 'endvr_create_taxonomy_sundayevent', 0 );

//create sundayevent taxomony
function endvr_create_taxonomy_sundayevent() 
{

	$labels = array(
		'name' 				=> _x( 'Event Type', 'taxonomy general name' ),
		'singular_name' 		=> _x( 'Event Type', 'taxonomy singular name' ),
		'search_items' 		=> __( 'Search for Event Type' ),
		'all_items' 			=> __( 'All Event Types' ),
		'parent_item' 			=> __( 'Parent Event Type' ),
		'parent_item_colon' 	=> __( 'Parent Event Type:' ),
		'edit_item' 			=> __( 'Edit Event Type' ), 
		'update_item' 			=> __( 'Update Event Type' ),
		'add_new_item' 		=> __( 'Add New Event Type' ),
		'new_item_name' 		=> __( 'New Event Type Name' ),
		'menu_name'			=> __( 'Event Type' )
	);
	 	
	$args = array(
		'hierarchical' 		=> true, // if set to true, the taxonomy will function like categories, and false = functionality like tags
		'labels' 				=> $labels,
		'show_ui' 			=> true,
		'show_admin_column' 	=> true, // this will add a sundayevent column to the index list for the ministries post type in the admin panel
		'query_var' 			=> 'sundayevent',
		'rewrite' 			=> array( 'slug' => 'sundays/schedule' )	
	);
	
	register_taxonomy( 'sundayevent', array( 'sundays' ), $args );
	//flush_rewrite_rules();
}