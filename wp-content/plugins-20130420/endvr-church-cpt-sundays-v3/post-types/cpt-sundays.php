<?php

// define the custom content type
// @ source: http://codex.wordpress.org/Function_Reference/register_post_type
add_action('init', 'endvr_init_sundays');
function endvr_init_sundays() 
{
  $labels = array(
    'name' 				=> _x('Sundays', 'post type general name'),
    'singular_name' 		=> _x('Sunday Event', 'post type singular name'),
    'add_new' 				=> _x('Add New', 'sunday event'),
    'add_new_item' 			=> __('Add New Sunday Event'),
    'edit_item' 			=> __('Edit Sunday Event'),
    'new_item' 			=> __('New Sunday Event'),
    'view_item' 			=> __('View Sunday Event'),
    'search_items' 			=> __('Search Sundays'),
    'not_found' 			=> __('No Sundays found'),
    'not_found_in_trash' 	=> __('No Sundays found in Trash'), 
    'parent_item_colon' 		=> __('Parent Directory')
  ); 
  $args = array(
    'labels' 				=> $labels,
    'public' 				=> true, // several of the other variables derive their values from 'public' by default
    'publicly_queryable' 	=> true,
    'show_ui' 				=> true,
    'show_in_nav_menus'		=> true,
    'show_in_menu' 			=> true,
    'show_in_admin_bar'		=> true,     
    'menu_position' 		=> 34, // should be 21-24 to appear between Pages and Comments in admin menu
    'menu_icon' 			=> '', // keep this line because it adds the .menu-icon-sundays class to the <li> HTML    
    'capability_type' 		=> 'page',
    'hierarchical' 			=> true,
    'supports' 			=> array( 'title','thumbnail','editor','page-attributes' ),
    'taxonomies' 			=> array( 'sundayevent', 'ministry' ),
    'has_archive' 			=> 'sundays', // must be explicitly set in order for rewrite rules to work
    'rewrite' 				=> array( 'slug' => 'sundays', 'with_front' => 'false' ),    
    'query_var' 			=> true,
    'can_export'			=> true
  );   
  register_post_type('sundays',$args);
}

// add filter to insure the text Sunday Event, or sunday event, is displayed when user updates a sunday event
add_filter( 'post_updated_messages', 'endvr_updated_messages_sundays' );
function endvr_updated_messages_sundays( $messages ) {
global $post, $post_ID;
  $messages['sundays'] = array(
    0 	=> '', // Unused. Messages start at index 1.
    1 	=> sprintf( __('Sunday Event updated. <a href="%s">View sunday event</a>'), esc_url( get_permalink($post_ID) ) ),
    2 	=> __('Custom field updated.'),
    3 	=> __('Custom field deleted.'),
    4 	=> __('Sunday Event updated.'),
    		// translators: %s: date and time of the revision
    5 	=> isset($_GET['revision']) ? sprintf( __('Sunday Event restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 	=> sprintf( __('Sunday Event published. <a href="%s">View sunday event</a>'), esc_url( get_permalink($post_ID) ) ),
    7 	=> __('Sunday Event saved.'),
    8 	=> sprintf( __('Sunday Event submitted. <a target="_blank" href="%s">Preview sunday event</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 	=> sprintf( __('Sunday Event scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview sunday event</a>'),
      	// translators: Publish box date format, see http://php.net/date
      	date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 	=> sprintf( __('Sunday Event draft updated. <a target="_blank" href="%s">Preview sunday event</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}

// assign custom text instructions to replace "Enter title here"
function endvr_replace_title_label_sundays( $title ) {
     $screen = get_current_screen();
     if  ( 'sundays' == $screen->post_type ) {
          $title = 'Enter Sunday Event Title Here...';
          return $title;
     }
}
add_filter( 'enter_title_here', 'endvr_replace_title_label_sundays' ); 

// assign a custom label to the Sub Title
// @source: http://codex.wordpress.org/Function_Reference/remove_filter
function endvr_subtitle_label_sundays() {
	echo 'Input a Subtitle for this Sunday Event (Optional)';
}
function endvr_replace_subtitle_label_sundays() {
     $screen = get_current_screen();
     if  ( 'sundays' == $screen->post_type ) {
     	remove_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_default' );
     	add_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_sundays' );
     }  
}
add_action( 'admin_head', 'endvr_replace_subtitle_label_sundays' );

// assign a custom label to the Post Editor + place it in a sortable Meta Box
// @source: http://wordpress.org/support/topic/move-custom-meta-box-above-editor?replies=17
// @source: http://software.troydesign.it/php/wordpress/move-wp-visual-editor.html
add_action( 'add_meta_boxes', 'endvr_add_meta_box_editor_sundays', 0 );
function endvr_add_meta_box_editor_sundays() {
     $screen = get_current_screen();
     if  ( 'sundays' == $screen->post_type ) {
		global $_wp_post_type_features;
		foreach ($_wp_post_type_features as $type => &$features) {
			if (isset($features['editor']) && $features['editor']) {
				unset($features['editor']);
				add_meta_box(
					'endvr_editor_description_sundays',
					__('Sunday Event Summary/Description'),
					'endvr_meta_box_editor_sundays',
					$type, 'normal', 'core'
				);
			}
		}
	}
	add_action( 'admin_head', 'endvr_admin_head_sundays' ); //white background
}
function endvr_admin_head_sundays() {
	?>
	<style type="text/css">
		.wp-editor-container{background-color:#fff;}
	</style>
	<?php
}
function endvr_meta_box_editor_sundays( $post ) {
	echo '<div class="wp-editor-wrap">';
	wp_editor($post->post_content, 'content', array('dfw' => true, 'tabindex' => 1) );
	echo '</div>';
}

// redefine the way the content type's admin panel index listing is displayed
// @source: http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column  
// @source: http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/   
add_filter( 'manage_edit-sundays_columns', 'endvr_set_custom_edit_columns_sundays' );
add_action( 'manage_sundays_posts_custom_column',  'endvr_custom_column_sundays', 10, 2 );
 
function endvr_set_custom_edit_columns_sundays($columns) {
	unset($columns['date']);
	return $columns 
		+ array(	
		);
}		

function endvr_custom_column_sundays($column, $post_id) {
    switch ( $column ) {
      case '':
      	echo get_post_meta( $post_id , '' , true );
        break;     
    }
} 

// flush rewrite rules upon activation
// @source: http://codex.wordpress.org/Function_Reference/register_post_type#Flushing_Rewrite_on_Activation
function endvr_rewrite_flush_sundays() {
	endvr_init_sundays();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'endvr_rewrite_flush_sundays' );         