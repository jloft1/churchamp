<?php
/**
 * @package endvr-church-cpt-sundays
 */
/*
Plugin Name: Endeavr - Church (Sundays Module v3)
Plugin URI: http://endeavr.com
Description: Essential functions for a standard Endeavr church website. [Sundays Module]
Author: Jason Loftis (JLOFT)
Author URI: http://jloft.com
Version: 3.0
*/

// Taxonomies
// It's important to register the taxonomies before the post type in order for the rewrite rules to work properly
include_once ( 'taxonomies/tax-sunday-event.php' );

// Post Types
include_once ( 'post-types/cpt-sundays.php' );

// Metaboxes
include_once ( 'metaboxes/mb-sundays.php' );

// Admin Styles
function endvr_admin_head_style_sundays() {
	echo '<link rel="stylesheet" type="text/css" href="' .plugins_url('styles/style-sundays.css', __FILE__). '">';
}
add_action('admin_head', 'endvr_admin_head_style_sundays');