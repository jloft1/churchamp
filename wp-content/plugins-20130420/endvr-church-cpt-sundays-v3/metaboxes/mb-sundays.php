<?php

/**
 *  Register Field Groups
 *
 *  The register_field_group function accepts 1 array which holds the relevant data to register a field group
 *  You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_sunday-events',
		'title' => 'Sunday Events',
		'fields' => array (
			0 => array (
				'key' => 'field_76',
				'label' => 'Sunday Event Details',
				'name' => '_endvr_sunday_event_details',
				'type' => 'repeater',
				'sub_fields' => array (
					0 => array (
						'key' => 'field_77',
						'label' => 'Event Title',
						'name' => '_endvr_sunday_event_title',
						'type' => 'text',
						'instructions' => 'What is the name/title of this event?',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'none',
					),
					2 => array (
						'key' => 'field_81',
						'label' => 'Event Time',
						'name' => '_endvr_sunday_event_time',
						'type' => 'text',
						'instructions' => 'Input the time in this format: 10:00 AM - 12:00 PM.',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'none',
					),
					3 => array (
						'key' => 'field_83',
						'label' => 'Event Date',
						'name' => '_endvr_sunday_event_date',
						'type' => 'text',
						'instructions' => 'If one-time event with specific date(s) (i.e. February 15-17).',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'none',
					),
					4 => array (
						'key' => 'field_85',
						'label' => 'Event Location',
						'name' => '_endvr_sunday_event_location',
						'type' => 'text',
						'instructions' => 'Input the location of the event.',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'none',
					),
					5 => array (
						'key' => 'field_87',
						'label' => 'Event Demographic',
						'name' => '_endvr_sunday_event_demographic',
						'type' => 'text',
						'instructions' => 'Who should attend the event or how should it be characterized (i.e. 6th-12th or Contemporary Worship).',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'none',
					),										
					6 => array (
						'key' => 'field_89',
						'label' => 'Event Description',
						'name' => '_endvr_sunday_event_description',
						'type' => 'wysiwyg',
						'instructions' => 'Describe the event. Think about what a first time visitor would need to know.',
						'column_width' => '',
						'default_value' => '',
						'toolbar' => 'basic',
						'media_upload' => 'no',
					),
					7 => array (
						'key' => 'field_91',
						'label' => 'Event Logo/Image',
						'name' => '_endvr_sunday_event_image',
						'type' => 'file',
						'instructions' => 'Attach an image or logo representing the event.',
						'column_width' => '',
						'save_format' => 'url',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Sunday Event',
			),
		),
		'location' => array (
			'rules' => array (
				0 => array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sundays',
					'order_no' => 0,
				),
			),
			'allorany' => 'all',
		),
		'options' => array (
			'position' => 'advanced',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}