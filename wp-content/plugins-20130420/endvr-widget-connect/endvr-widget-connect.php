<?php
/**
 * @package endvr-widget-connect
 */
/*
Plugin Name: Endeavr Connect Widget
Plugin URI: http://endeavr.com/dev/wp/plugins/endvr-widget-connect/
Description: This is a widget for displaying contact informationa nd social links.
Author: Jason Loftis (JLOFT)
Author URI: http://jloft.com
Version: 1.0

Tutorial on Obfuscating Email in WordPress:
http://briteweb.com/blog/wordpress/protect-email-from-spambots-wordpress/
*/

class endvr_widget_connect extends WP_Widget {

    function endvr_widget_connect() {
        parent::WP_Widget(false, $name = 'Contact Info + Social Links');
    }

    function widget($args, $instance) {
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        ?>
            <?php echo $before_widget; ?>
                <?php if ( $title )
                    echo $before_title . $title . $after_title;  else echo '<div class="widget-body clear">'; ?>

                    <?php if ( get_option('phone') ) : ?>
                    <div class="endvr_contact connect_phone">
                    <i class="connect_icon">&nbsp;</i>
                    PH: &nbsp; <?php echo get_option('phone'); ?>
                    </div>
                    <?php endif; ?>
                    
                    <?php if ( get_option('fax') ) : ?>
                    <div class="endvr_contact connect_fax">
                    <i class="connect_icon">&nbsp;</i>
                    FX: &nbsp; <?php echo get_option('fax'); ?>
                    </div>
                    <?php endif; ?>
                    
                    <?php if ( get_option('address') ) : ?>
                    <div class="endvr_contact connect_address">
                    <i class="connect_icon">&nbsp;</i>
                    <?php echo get_option('address'); ?>
                    </div>
                    <?php endif; ?>

                    <?php if ( get_option('email') ) : ?>
                    <div class="endvr_contact connect_email">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="mailto:<?php $emailaddy = get_option('email'); echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
                    </div>
                    <?php endif; ?>

                    <?php if ( get_option('contact_form_url') ) : ?>
                    <div class="endvr_contact connect_contact_form">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="<?php echo get_option('contact_form_url'); ?>" title="Contact Us" target="_self">Online Contact Form</a>
                    </div>
                    <?php endif; ?>
                    
                    <hr />
                    
                    <?php if ( get_option('feed_url') ) : ?>
                    <div class="endvr_social connect_feed">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="<?php echo ( get_option('feed_url') )? get_option('feed_url') : get_bloginfo('rss2_url'); ?>">RSS Feed</a>
                    <span><?php echo get_option('feed_text'); ?></span>
                    </div> 
                    <?php endif; ?>                                       
                    
                    <?php if ( get_option('twitter_url') ) : ?>
                    <div class="endvr_social connect_twitter">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="<?php echo get_option('twitter_url'); ?>" title="Twitter" target="_blank">Twitter</a>
                    <span><?php echo get_option('twitter_text'); ?></span>
                    </div>
                    <?php endif; ?>

                    <?php if ( get_option('facebook_url') ) : ?>
                    <div class="endvr_social connect_facebook">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="<?php echo get_option('facebook_url'); ?>" title="Facebook" target="_blank">Facebook</a>
                    <span><?php echo get_option('facebook_text'); ?></span>
                    </div>
                    <?php endif; ?>

                    <?php if ( get_option('linkedin_url') ) : ?>
                    <div class="endvr_social connect_linkedin">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="<?php echo get_option('linkedin_url'); ?>" title="LinkedIn" target="_blank">LinkedIn</a>
                    <span><?php echo get_option('linkedin_text'); ?></span>
                    </div>
                    <?php endif; ?>

                    <?php if ( get_option('flickr_url') ) : ?>
                    <div class="endvr_social connect_flickr">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="<?php echo get_option('flickr_url'); ?>" title="Flickr" target="_blank">Flickr group</a>
                    <span><?php echo get_option('flickr_text'); ?></span>
                    </div>
                    <?php endif; ?>

                    <?php if ( get_option('delicious_url') ) : ?>
                    <div class="endvr_social connect_delicious">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="<?php echo get_option('delicious_url'); ?>" title="Delicious" target="_blank">Delicious</a>
                    <span><?php echo get_option('delicious_text'); ?></span>
                    </div>
                    <?php endif; ?>

                    <?php if ( get_option('scribd_url') ) : ?>
                    <div class="endvr_social connect_scribd">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="<?php echo get_option('scribd_url'); ?>" title="Scribd" target="_blank">Scribd</a>
                    <span><?php echo get_option('scribd_text'); ?></span>
                    </div>
                    <?php endif; ?>

                    <?php if ( get_option('tumblr_url') ) : ?>
                    <div class="endvr_social connect_tumblr">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="<?php echo get_option('tumblr_url'); ?>" title="Tumblr" target="_blank">Tumblr</a>
                    <span><?php echo get_option('tumblr_text'); ?></span>
                    </div>
                    <?php endif; ?>

                    <?php if ( get_option('vimeo_url') ) : ?>
                    <div class="endvr_social connect_vimeo">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="<?php echo get_option('vimeo_url'); ?>" title="Vimeo" target="_blank">Vimeo</a>
                    <span><?php echo get_option('vimeo_text'); ?></span>
                    </div>
                    <?php endif; ?>

                    <?php if ( get_option('youtube_url') ) : ?>
                    <div class="endvr_social connect_youtube">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="<?php echo get_option('youtube_url'); ?>" title="YouTube" target="_blank">YouTube</a>
                    <span><?php echo get_option('youtube_text'); ?></span>
                    </div>
                    <?php endif; ?>

            <?php echo $after_widget; ?>
        <?php
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        
        update_option('phone', $_POST['phone']);
        update_option('fax', $_POST['fax']);
        update_option('address', $_POST['address']);
        update_option('email', $_POST['email']);
        update_option('contact_form_url', $_POST['contact_form_url']);
         
        update_option('feed_url', $_POST['feed_url']);
        update_option('twitter_url', $_POST['twitter_url']);
        update_option('facebook_url', $_POST['facebook_url']);
        update_option('linkedin_url', $_POST['linkedin_url']);
        update_option('flickr_url', $_POST['flickr_url']);
        update_option('delicious_url', $_POST['delicious_url']);
        update_option('scribd_url', $_POST['scribd_url']);
        update_option('tumblr_url', $_POST['tumblr_url']);
        update_option('vimeo_url', $_POST['vimeo_url']);
        update_option('youtube_url', $_POST['youtube_url']);
        
        update_option('feed_text', $_POST['feed_text']);
        update_option('twitter_text', $_POST['twitter_text']);
        update_option('facebook_text', $_POST['facebook_text']);
        update_option('linkedin_text', $_POST['linkedin_text']);
        update_option('flickr_text', $_POST['flickr_text']);
        update_option('delicious_text', $_POST['delicious_text']);
        update_option('scribd_text', $_POST['scribd_text']);
        update_option('tumblr_text', $_POST['tumblr_text']);
        update_option('vimeo_text', $_POST['vimeo_text']);
        update_option('youtube_text', $_POST['youtube_text']);
        
        return $instance;
    }

    function form($instance) {

        $title = isset($instance['title'] ) ? esc_attr( $instance['title'] ) : '';
        ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>

            <script type="text/javascript">
                (function($) {
                    $(function() {
                        $('.social_options').hide();
                        $('.social_title').toggle(
                            function(){ $(this).next().slideDown(100) },
                            function(){ $(this).next().slideUp(100) }
                        );
                    })
                })(jQuery)
            </script>
            
            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Phone</a>
                <p class="social_options">
                    <label for="phone">Phone Number:</label>
                    <input type="text" name="phone" id="phone" class="widefat" value="<?php echo get_option('phone'); ?>"/>
                </p>
            </div> 
            
            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Fax</a>
                <p class="social_options">
                    <label for="fax">Fax Number:</label>
                    <input type="text" name="fax" id="fax" class="widefat" value="<?php echo get_option('fax'); ?>"/>
                </p>
            </div>
            
            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Address</a>
                <p class="social_options">
                    <label for="address">Postal Address:</label>
                    <input type="text" name="address" id="address" class="widefat" value="<?php echo get_option('address'); ?>"/>
                </p>
            </div>
            
            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Email</a>
                <p class="social_options">
                    <label for="email">Email Address:</label>
                    <input type="text" name="email" id="email" class="widefat" value="<?php echo get_option('email'); ?>"/>
                </p>
            </div>
            
            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Contact Form</a>
                <p class="social_options">
                    <label for="contact_form_url">Contact Form's Page URL:</label>
                    <input type="text" name="contact_form_url" id="contact_form_url" class="widefat" value="<?php echo get_option('contact_form_url'); ?>"/>
                </p>
            </div>                                                           

            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">RSS Feed</a>
                <p class="social_options">
                    <label for="feed_url">Feed url:</label>
                    <input type="text" name="feed_url" id="feed_url" class="widefat" value="<?php echo get_option('feed_url'); ?>"/>
                    <label for="feed_text">Description:</label>
                    <input type="text" name="feed_text" id="feed_text" class="widefat" value="<?php echo get_option('feed_text'); ?>"/>
                </p>
            </div>

            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Twitter</a>
                <p class="social_options">
                    <label for="twitter_url">Profile url:</label>
                    <input type="text" name="twitter_url" id="twitter_url" class="widefat" value="<?php echo get_option('twitter_url'); ?>"/>
				<label for="twitter_text">Description:</label>
                    <input type="text" name="twitter_text" id="twitter_text" class="widefat" value="<?php echo get_option('twitter_text'); ?>"/>                    
                </p>
            </div>

            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Facebook</a>
                <p class="social_options">
                    <label for="facebook_url">Profile url:</label>
                    <input type="text" name="facebook_url" id="facebook_url" class="widefat" value="<?php echo get_option('facebook_url'); ?>"/>
                    <label for="facebook_text">Description:</label>
                    <input type="text" name="facebook_text" id="facebook_text" class="widefat" value="<?php echo get_option('facebook_text'); ?>"/>
                </p>
            </div>
            
            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">LinkedIn</a>
                <p class="social_options">
                    <label for="linkedin_url">Profile url:</label>
                    <input type="text" name="linkedin_url" id="linkedin_url" class="widefat" value="<?php echo get_option('linkedin_url'); ?>"/>
                    <label for="linkedin_text">Description:</label>
                    <input type="text" name="linkedin_text" id="linkedin_text" class="widefat" value="<?php echo get_option('linkedin_text'); ?>"/>
                </p>
            </div>

            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Flickr</a>
                <p class="social_options">
                    <label for="flickr_url">Profile url:</label>
                    <input type="text" name="flickr_url" id="flickr_url" class="widefat" value="<?php echo get_option('flickr_url'); ?>"/>
                    <label for="flickr_text">Description:</label>
                    <input type="text" name="flickr_text" id="flickr_text" class="widefat" value="<?php echo get_option('flickr_text'); ?>"/>
                </p>
            </div>

            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Delicious</a>
                <p class="social_options">
                    <label for="delicious_url">Profile url:</label>
                    <input type="text" name="delicious_url" id="delicious_url" class="widefat" value="<?php echo get_option('delicious_url'); ?>"/>
                    <label for="delicious_text">Description:</label>
                    <input type="text" name="delicious_text" id="delicious_text" class="widefat" value="<?php echo get_option('delicious_text'); ?>"/>
                </p>
            </div>

            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Scribd</a>
                <p class="social_options">
                    <label for="scribd_url">Profile url:</label>
                    <input type="text" name="scribd_url" id="scribd_url" class="widefat" value="<?php echo get_option('scribd_url'); ?>"/>
                    <label for="scribd_text">Description:</label>
                    <input type="text" name="scribd_text" id="scribd_text" class="widefat" value="<?php echo get_option('scribd_text'); ?>"/>
                </p>
            </div>

            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Tumblr</a>
                <p class="social_options">
                    <label for="tumblr_url">Profile url:</label>
                    <input type="text" name="tumblr_url" id="tumblr_url" class="widefat" value="<?php echo get_option('tumblr_url'); ?>"/>
                    <label for="tumblr_text">Description:</label>
                    <input type="text" name="tumblr_text" id="tumblr_text" class="widefat" value="<?php echo get_option('tumblr_text'); ?>"/>
                </p>
            </div>

            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Vimeo</a>
                <p class="social_options">
                    <label for="vimeo_url">Profile url:</label>
                    <input type="text" name="vimeo_url" id="vimeo_url" class="widefat" value="<?php echo get_option('vimeo_url'); ?>"/>
                    <label for="vimeo_text">Description:</label>
                    <input type="text" name="vimeo_text" id="vimeo_text" class="widefat" value="<?php echo get_option('vimeo_text'); ?>"/>
                </p>
            </div>

            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="social_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Youtube</a>
                <p class="social_options">
                    <label for="youtube_url">Profile url:</label>
                    <input type="text" name="youtube_url" id="youtube_url" class="widefat" value="<?php echo get_option('youtube_url'); ?>"/>
                    <label for="youtube_text">Description:</label>
                    <input type="text" name="youtube_text" id="youtube_text" class="widefat" value="<?php echo get_option('youtube_text'); ?>"/>
                </p>
            </div>
        <?php
    }

}
add_action('widgets_init', create_function('', 'return register_widget("endvr_widget_connect");'));

?>