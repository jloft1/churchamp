<?php
//hook into the init action and call endvr_create_taxonomy_ministry when it fires
add_action( 'init', 'endvr_create_taxonomy_ministry', 0 );

//create ministry taxomony
function endvr_create_taxonomy_ministry() 
{

	$labels = array(
		'name' 				=> _x( 'Ministry', 'taxonomy general name' ),
		'singular_name' 		=> _x( 'Ministry', 'taxonomy singular name' ),
		'search_items' 		=> __( 'Search for Ministry' ),
		'all_items' 			=> __( 'All Ministry' ),
		'parent_item' 			=> __( 'Parent Ministry' ),
		'parent_item_colon' 	=> __( 'Parent Ministry:' ),
		'edit_item' 			=> __( 'Edit Ministry' ), 
		'update_item' 			=> __( 'Update Ministry' ),
		'add_new_item' 		=> __( 'Add New Ministry' ),
		'new_item_name' 		=> __( 'New Ministry Name' ),
		'menu_name'			=> __( 'Ministry' )
	);
	 	
	$args = array(
		'hierarchical' 		=> true, // if set to true, the taxonomy will function like categories, and false = functionality like tags
		'labels' 				=> $labels,
		'show_ui' 			=> true,
		'show_admin_column' 	=> true, // this will add a ministry column to the index list for the ministries post type in the admin panel
		'query_var' 			=> 'ministry',
		'rewrite' 			=> array( 'slug' => 'ministries/ministry' )	
	);
	
	register_taxonomy( 'ministry', array( 'ministries', 'sundays' ), $args );
	//flush_rewrite_rules();
}