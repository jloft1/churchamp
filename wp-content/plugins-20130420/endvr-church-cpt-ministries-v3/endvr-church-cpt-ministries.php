<?php
/**
 * @package endvr-church-cpt-ministries
 */
/*
Plugin Name: Endeavr - Church (Ministries Module v3)
Plugin URI: http://endeavr.com
Description: Essential functions for a standard Endeavr church website. [Ministries Module]
Author: Jason Loftis (JLOFT)
Author URI: http://jloft.com
Version: 3.0
*/

// Taxonomies
// It's important to register the taxonomies before the post type in order for the rewrite rules to work properly
include_once ( 'taxonomies/tax-ministry.php' );

// Post Types
include_once ( 'post-types/cpt-ministries.php' );

// Metaboxes
include_once ( 'metaboxes/mb-ministries.php' );

// Admin Styles
function endvr_admin_head_style_ministries() {
	echo '<link rel="stylesheet" type="text/css" href="' .plugins_url('styles/style-ministries.css', __FILE__). '">';
}
add_action('admin_head', 'endvr_admin_head_style_ministries');