<?php

// define the custom content type
// @ source: http://codex.wordpress.org/Function_Reference/register_post_type
add_action('init', 'endvr_init_ministries');
function endvr_init_ministries() 
{
  $labels = array(
    'name' 				=> _x('Ministries', 'post type general name'),
    'singular_name' 		=> _x('Ministry', 'post type singular name'),
    'add_new' 				=> _x('Add New', 'ministry'),
    'add_new_item' 			=> __('Add New Ministry'),
    'edit_item' 			=> __('Edit Ministry'),
    'new_item' 			=> __('New Ministry'),
    'view_item' 			=> __('View Ministry'),
    'search_items' 			=> __('Search Ministries'),
    'not_found' 			=> __('No Ministries found'),
    'not_found_in_trash' 	=> __('No Ministries found in Trash'), 
    'parent_item_colon' 		=> __('Parent Directory')
  ); 
  $args = array(
    'labels' 				=> $labels,
    'public' 				=> true, // several of the other variables derive their values from 'public' by default
    'publicly_queryable' 	=> true,
    'show_ui' 				=> true,
    'show_in_nav_menus'		=> true,
    'show_in_menu' 			=> true,
    'show_in_admin_bar'		=> true,     
    'menu_position' 		=> 33, // should be 21-24 to appear between Pages and Comments in admin menu
    'menu_icon' 			=> '', // keep this line because it adds the .menu-icon-ministries class to the <li> HTML    
    'capability_type' 		=> 'page',
    'hierarchical' 			=> true,
    'supports' 			=> array( 'title','thumbnail','editor','page-attributes' ),
    'taxonomies' 			=> array( 'ministry' ),
    'has_archive' 			=> 'ministries', // must be explicitly set in order for rewrite rules to work
    'rewrite' 				=> array( 'slug' => 'ministries', 'with_front' => 'false' ),    
    'query_var' 			=> true,
    'can_export'			=> true
  );   
  register_post_type('ministries',$args);
}

// add filter to insure the text Ministry, or ministry, is displayed when user updates a ministry
add_filter( 'post_updated_messages', 'endvr_updated_messages_ministries' );
function endvr_updated_messages_ministries( $messages ) {
global $post, $post_ID;
  $messages['ministries'] = array(
    0 	=> '', // Unused. Messages start at index 1.
    1 	=> sprintf( __('Ministry updated. <a href="%s">View ministry</a>'), esc_url( get_permalink($post_ID) ) ),
    2 	=> __('Custom field updated.'),
    3 	=> __('Custom field deleted.'),
    4 	=> __('Ministry updated.'),
    		// translators: %s: date and time of the revision
    5 	=> isset($_GET['revision']) ? sprintf( __('Ministry restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 	=> sprintf( __('Ministry published. <a href="%s">View ministry</a>'), esc_url( get_permalink($post_ID) ) ),
    7 	=> __('Ministry saved.'),
    8 	=> sprintf( __('Ministry submitted. <a target="_blank" href="%s">Preview ministry</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 	=> sprintf( __('Ministry scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview ministry</a>'),
      	// translators: Publish box date format, see http://php.net/date
      	date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 	=> sprintf( __('Ministry draft updated. <a target="_blank" href="%s">Preview ministry</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}

// assign custom text instructions to replace "Enter title here"
function endvr_replace_title_label_ministries( $title ) {
     $screen = get_current_screen();
     if  ( 'ministries' == $screen->post_type ) {
          $title = 'Enter Ministry Title Here...';
          return $title;
     }
}
add_filter( 'enter_title_here', 'endvr_replace_title_label_ministries' ); 

// assign a custom label to the Sub Title
// @source: http://codex.wordpress.org/Function_Reference/remove_filter
function endvr_subtitle_label_ministries() {
	echo 'Input a Subtitle for this Ministry (Optional)';
}
function endvr_replace_subtitle_label_ministries() {
     $screen = get_current_screen();
     if  ( 'ministries' == $screen->post_type ) {
     	remove_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_default' );
     	add_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_ministries' );
     }  
}
add_action( 'admin_head', 'endvr_replace_subtitle_label_ministries' );

// assign a custom label to the Post Editor + place it in a sortable Meta Box
// @source: http://wordpress.org/support/topic/move-custom-meta-box-above-editor?replies=17
// @source: http://software.troydesign.it/php/wordpress/move-wp-visual-editor.html
add_action( 'add_meta_boxes', 'endvr_add_meta_box_editor_ministries', 0 );
function endvr_add_meta_box_editor_ministries() {
     $screen = get_current_screen();
     if  ( 'ministries' == $screen->post_type ) {
		global $_wp_post_type_features;
		foreach ($_wp_post_type_features as $type => &$features) {
			if (isset($features['editor']) && $features['editor']) {
				unset($features['editor']);
				add_meta_box(
					'endvr_editor_description_ministries',
					__('Ministry Summary/Description'),
					'endvr_meta_box_editor_ministries',
					$type, 'normal', 'core'
				);
			}
		}
	}
	add_action( 'admin_head', 'endvr_admin_head_ministries' ); //white background
}
function endvr_admin_head_ministries() {
	?>
	<style type="text/css">
		.wp-editor-container{background-color:#fff;}
	</style>
	<?php
}
function endvr_meta_box_editor_ministries( $post ) {
	echo '<div class="wp-editor-wrap">';
	wp_editor($post->post_content, 'content', array('dfw' => true, 'tabindex' => 1) );
	echo '</div>';
}

// redefine the way the content type's admin panel index listing is displayed
// @source: http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column  
// @source: http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/   
add_filter( 'manage_edit-ministries_columns', 'endvr_set_custom_edit_columns_ministries' );
add_action( 'manage_ministries_posts_custom_column',  'endvr_custom_column_ministries', 10, 2 );
 
function endvr_set_custom_edit_columns_ministries($columns) {
	unset($columns['date']);
	return $columns 
		+ array(	
		);
}		

function endvr_custom_column_ministries($column, $post_id) {
    switch ( $column ) {
      case '':
      	echo get_post_meta( $post_id , '' , true );
        break;     
    }
} 

// flush rewrite rules upon activation
// @source: http://codex.wordpress.org/Function_Reference/register_post_type#Flushing_Rewrite_on_Activation
function endvr_rewrite_flush_ministries() {
	endvr_init_ministries();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'endvr_rewrite_flush_ministries' );         