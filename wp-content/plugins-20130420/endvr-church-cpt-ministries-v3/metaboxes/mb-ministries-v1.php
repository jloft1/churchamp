<?php

/**
 * Register field groups
 * The register_field_group function accepts 1 array which holds the relevant data to register a field group
 * You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 * This code must run every time the functions.php file is read
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => '514a4942dc2c7',
		'title' => 'Ministry Events',
		'fields' =>
		array (
			0 => 
			array (
				'key' => '_endvr_ministry_event_details',
				'label' => 'Ministry Event Details',
				'name' => '_endvr_ministry_event_details',
				'type' => 'repeater',
				'order_no' => 0,
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 
				array (
					'status' => 0,
					'rules' => 
					array (
						0 => 
						array (
							'field' => 'null',
							'operator' => '==',
						),
					),
					'allorany' => 'all',
				),
				'sub_fields' => 
				array (
					'field_37' => 
					array (
						'label' => 'Event Title',
						'name' => '_endvr_ministry_event_title',
						'type' => 'text',
						'instructions' => 'What is the name/title of this event?',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'none',
						'order_no' => 0,
						'key' => '_endvr_ministry_event_title',
					),				
					'field_39' => 
					array (
						'label' => 'Event Date',
						'name' => '_endvr_ministry_event_date',
						'type' => 'text',
						'instructions' => 'If one-time event with specific date(s) (i.e. February 15-17)',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'none',
						'order_no' => 1,
						'key' => '_endvr_ministry_event_date',
					),
					'field_41' => 
					array (
						'label' => 'Event Day',
						'name' => '_endvr_ministry_event_day',
						'type' => 'text',
						'instructions' => 'Input the full day name (i.e. Sunday or Wednesday)',
						'column_width' => '',
						'default_value' => 'Sunday',
						'formatting' => 'none',
						'order_no' => 1,
						'key' => '_endvr_ministry_event_day',
					),					
					'field_43' => 
					array (
						'label' => 'Event Time',
						'name' => '_endvr_ministry_event_time',
						'type' => 'text',
						'instructions' => 'Input the time in this format: 10:00 AM - 12:00 PM',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'none',
						'order_no' => 3,
						'key' => '_endvr_ministry_event_time',
					),
					'field_45' => 
					array (
						'label' => 'Event Location',
						'name' => '_endvr_ministry_event_location',
						'type' => 'text',
						'instructions' => 'Input the location of the event.',
						'column_width' => '',
						'default_value' => '',
						'formatting' => 'none',
						'order_no' => 4,
						'key' => '_endvr_ministry_event_location',
					),					
					'field_47' => 
					array (
						'label' => 'Event Description',
						'name' => '_endvr_ministry_event_description',
						'type' => 'wysiwyg',
						'instructions' => 'Describe the event. Think about what a first time visitor would need to know.',
						'column_width' => '',
						'default_value' => '',
						'toolbar' => 'basic',
						'media_upload' => 'no',
						'the_content' => 'yes',
						'order_no' => 5,
						'key' => '_endvr_ministry_event_description',
					),
					'field_49' => 
					array (
						'label' => 'Event Logo/Image',
						'name' => '_endvr_ministry_event_image',
						'type' => 'file',
						'instructions' => 'Attach an image or logo representing the event.',
						'column_width' => '',
						'save_format' => 'url',
						'order_no' => 6,
						'key' => '_endvr_ministry_event_image',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Ministry Event',
			),
		),
		'location' => 
		array (
			'rules' => 
			array (
				0 => 
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'ministries',
					'order_no' => 0,
				),
			),
			'allorany' => 'all',
		),
		'options' => 
		array (
			'position' => 'advanced',
			'layout' => 'default',
			'hide_on_screen' => 
			array (
			),
		),
		'menu_order' => 0,
	));
}