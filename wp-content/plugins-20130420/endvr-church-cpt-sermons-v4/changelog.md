Endeavr - Church Sermons Module (Changelog)
===========================================

Version 4.0.0
-------------

### 2013-04-03

Added a "gettext" function to filter the publish meta box labels from "Published On" to "Sermon Date".
Added back the date column in the index view for the Sermons CPT.