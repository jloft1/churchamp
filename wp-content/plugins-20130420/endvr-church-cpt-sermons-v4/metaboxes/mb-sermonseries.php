<?php

/**
 *  Register Field Groups
 *
 *  The register_field_group function accepts 1 array which holds the relevant data to register a field group
 *  You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_taxonomy-image',
		'title' => 'Taxonomy Image',
		'fields' => array (
			0 => array (
				'key' => 'field_514c603d11243',
				'label' => 'Taxonomy Image',
				'name' => '_endvr_taxonomy_image_sermonseries',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
			),
		),
		'location' => array (
			'rules' => array (
				0 => array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'sermonseries',
					'order_no' => 0,
				),
			),
			'allorany' => 'all',
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'excerpt',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				5 => 'revisions',
				6 => 'slug',
				7 => 'author',
				8 => 'format',
				9 => 'featured_image',
				10 => 'categories',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}