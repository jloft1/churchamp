<?php
//hook into the init action and call endvr_create_taxonomy_sermonspeaker when it fires
add_action( 'init', 'endvr_create_taxonomy_sermonspeaker', 0 );

//create sermon speaker taxomony
function endvr_create_taxonomy_sermonspeaker() 
{

	$labels = array(
		'name' 				=> _x( 'Sermon Speaker', 'taxonomy general name' ),
		'singular_name' 		=> _x( 'Sermon Speaker', 'taxonomy singular name' ),
		'search_items' 		=> __( 'Search for Sermon Speaker' ),
		'all_items' 			=> __( 'All Sermon Speaker' ),
		'parent_item' 			=> __( 'Parent Sermon Speaker' ),
		'parent_item_colon' 	=> __( 'Parent Sermon Speaker:' ),
		'edit_item' 			=> __( 'Edit Sermon Speaker' ), 
		'update_item' 			=> __( 'Update Sermon Speaker' ),
		'add_new_item' 		=> __( 'Add New Sermon Speaker' ),
		'new_item_name' 		=> __( 'New Sermon Speaker Name' ),
		'menu_name'			=> __( 'Sermon Speaker' )
	);
	 	
	$args = array(
		'hierarchical' 		=> true, // if set to true, the taxonomy will function like categories, and false = functionality like tags
		'labels' 				=> $labels,
		'show_ui' 			=> true,
		'show_admin_column' 	=> true, // this will add a sermon speaker column to the index list for the sermons post type in the admin panel
		'query_var' 			=> 'sermonspeaker',
		'rewrite' 			=> array( 'slug' => 'sermons/speaker' )	
	);
	
	register_taxonomy( 'sermonspeaker', array( 'sermons' ), $args );
	//flush_rewrite_rules();
}