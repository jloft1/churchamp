<?php
//hook into the init action and call endvr_create_taxonomy_sermonseries when it fires
add_action( 'init', 'endvr_create_taxonomy_sermonseries', 0 );

//create sermon series taxomony
function endvr_create_taxonomy_sermonseries() 
{

	$labels = array(
		'name' 				=> _x( 'Sermon Series', 'taxonomy general name' ),
		'singular_name' 		=> _x( 'Sermon Series', 'taxonomy singular name' ),
		'search_items' 		=> __( 'Search for Sermon Series' ),
		'all_items' 			=> __( 'All Sermon Series' ),
		'parent_item' 			=> __( 'Parent Sermon Series' ),
		'parent_item_colon' 	=> __( 'Parent Sermon Series:' ),
		'edit_item' 			=> __( 'Edit Sermon Series' ), 
		'update_item' 			=> __( 'Update Sermon Series' ),
		'add_new_item' 		=> __( 'Add New Sermon Series' ),
		'new_item_name' 		=> __( 'New Sermon Series Name' ),
		'menu_name'			=> __( 'Sermon Series' )
	);
	 	
	$args = array(
		'hierarchical' 		=> true, // if set to true, the taxonomy will function like categories, and false = functionality like tags
		'labels' 				=> $labels,
		'show_ui' 			=> true,
		'show_admin_column' 	=> true, // this will add a sermon series column to the index list for the sermons post type in the admin panel
		'query_var' 			=> 'sermonseries',
		'rewrite' 			=> array( 'slug' => 'sermons/series' )	
	);
	
	register_taxonomy( 'sermonseries', array( 'sermons' ), $args );
	//flush_rewrite_rules();
}