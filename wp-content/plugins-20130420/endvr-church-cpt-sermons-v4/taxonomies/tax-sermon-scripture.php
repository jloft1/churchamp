<?php
//hook into the init action and call endvr_create_taxonomy_sermonscripture when it fires
add_action( 'init', 'endvr_create_taxonomy_sermonscripture', 0 );

//create sermon scripture taxomony
function endvr_create_taxonomy_sermonscripture() 
{

	$labels = array(
		'name' 				=> _x( 'Sermon Scripture', 'taxonomy general name' ),
		'singular_name' 		=> _x( 'Sermon Scripture', 'taxonomy singular name' ),
		'search_items' 		=> __( 'Search for Sermon Scripture' ),
		'all_items' 			=> __( 'All Sermon Scripture' ),
		'parent_item' 			=> __( 'Parent Sermon Scripture' ),
		'parent_item_colon' 	=> __( 'Parent Sermon Scripture:' ),
		'edit_item' 			=> __( 'Edit Sermon Scripture' ), 
		'update_item' 			=> __( 'Update Sermon Scripture' ),
		'add_new_item' 		=> __( 'Add New Sermon Scripture' ),
		'new_item_name' 		=> __( 'New Sermon Scripture Name' ),
		'menu_name'			=> __( 'Sermon Scripture' )
	);
	 	
	$args = array(
		'hierarchical' 		=> true, // if set to true, the taxonomy will function like categories, and false = functionality like tags
		'labels' 				=> $labels,
		'show_ui' 			=> true,
		'show_admin_column' 	=> true, // this will add a sermon scripture column to the index list for the sermons post type in the admin panel
		'query_var' 			=> 'sermonscripture',
		'rewrite' 			=> array( 'slug' => 'sermons/scripture' )	
	);
	
	register_taxonomy( 'sermonscripture', array( 'sermons' ), $args );
	//flush_rewrite_rules();
}