<?php

// define the custom content type
add_action('init', 'sermons_init');
function sermons_init() 
{
  $labels = array(
    'name' => _x('Sermons', 'post type general name'),
    'singular_name' => _x('Sermon', 'post type singular name'),
    'add_new' => _x('Add New', 'sermon'),
    'add_new_item' => __('Add New Sermon'),
    'edit_item' => __('Edit Sermon'),
    'new_item' => __('New Sermon'),
    'view_item' => __('View Sermon'),
    'search_items' => __('Search Sermons'),
    'not_found' =>  __('No Sermons found'),
    'not_found_in_trash' => __('No Sermons found in Trash'), 
    'parent_item_colon' => __('Parent Directory')
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,     
    'menu_position' => 23,
    'menu_icon' => '', // keep this line because it adds the .menu-icon-sermons class to the <li> HTML    
    'query_var' => true,
    'rewrite' => array('slug' => 'sermons'),
    'capability_type' => 'post',
    'hierarchical' => true,
    'supports' => array('title','editor'),
    'taxonomies' => array('sermonseries'),
    'has_archive' => true
  ); 
  register_post_type('sermons',$args);
  flush_rewrite_rules(); 
}

//add filter to insure the text Sermon, or sermon, is displayed when user updates a sermon 
add_filter('post_updated_messages', 'sermons_updated_messages');
function sermons_updated_messages( $messages ) {
global $post, $post_ID;

  $messages['sermons'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Sermon updated. <a href="%s">View sermon</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Sermon updated.'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Sermon restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Sermon published. <a href="%s">View sermon</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Sermon saved.'),
    8 => sprintf( __('Sermon submitted. <a target="_blank" href="%s">Preview sermon</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Sermon scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview sermon</a>'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Sermon draft updated. <a target="_blank" href="%s">Preview sermon</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
}

// assign custom text instructions to replace "Enter title here"
function endvr_replace_sermons_title_label( $title ){
     $screen = get_current_screen();
     if  ( 'sermons' == $screen->post_type ) {
          $title = 'Enter Sermon Title Here...';
          return $title;
     }
}
add_filter( 'enter_title_here', 'endvr_replace_sermons_title_label' ); 

// assign a custom label to the Sub Title
function endvr_replace_sermons_subtitle_label( $subtitle ) {
     $screen = get_current_screen();
     if  ( 'sermons' == $screen->post_type ) {
          $subtitle = str_replace('a Subtitle for this Content','the Sermon\'s Subtitle',$subtitle);
          return $subtitle;
     }  
}
add_filter( 'endvr_subtitle_label', 'endvr_replace_sermons_subtitle_label' );

// assign a custom label to the Post Editor
// http://wordpress.org/support/topic/move-custom-meta-box-above-editor?replies=17
// http://software.troydesign.it/php/wordpress/move-wp-visual-editor.html

add_action( 'add_meta_boxes', 'endvr_add_meta_box_editor_sermons', 0 );
function endvr_add_meta_box_editor_sermons() {
     $screen = get_current_screen();
     if  ( 'sermons' == $screen->post_type ) {
		global $_wp_post_type_features;
		foreach ($_wp_post_type_features as $type => &$features) {
			if (isset($features['editor']) && $features['editor']) {
				unset($features['editor']);
				add_meta_box(
					'endvr_sermons_editor_description',
					__('Sermon Summary/Description'),
					'endvr_meta_box_editor_sermons',
					$type, 'normal', 'core'
				);
			}
		}
	}
	add_action( 'admin_head', 'endvr_admin_head_sermons'); //white background
}
function endvr_admin_head_sermons() {
	?>
	<style type="text/css">
		.wp-editor-container{background-color:#fff;}
	</style>
	<?php
}
function endvr_meta_box_editor_sermons( $post ) {
	echo '<div class="wp-editor-wrap">';
	wp_editor($post->post_content, 'content', array('dfw' => true, 'tabindex' => 1) );
	echo '</div>';
}

// redefine the way the content type's control panel index listing is displayed
// http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column  
// http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/   

add_filter("manage_edit-sermons_columns", "set_custom_edit_sermons_columns");
add_action("manage_sermons_posts_custom_column",  "custom_sermons_column", 10, 2);
 
function set_custom_edit_sermons_columns($columns){
	unset($columns['date']);
	return $columns 
		+ array(
			'_endvr_sermon_audio' 	=> __('Audio'),
			'_endvr_sermon_video' 	=> __('Video'),
			'_endvr_sermon_doc'		=> __('Outline'),
			'_endvr_sermon_ref' 	=> __('Reference'),		
		);
}		

function custom_sermons_column($column, $post_id){
    switch ( $column ) {
      case '_endvr_sermon_audio':
      	echo get_post_meta( $post_id , '_endvr_sermon_audio' , true );
        break;
      case '_endvr_sermon_video':
        	echo get_post_meta( $post_id , '_endvr_sermon_video' , true ); 
        break;
      case '_endvr_sermon_doc':
        	echo get_post_meta( $post_id , '_endvr_sermon_doc' , true ); 
        break; 
      case '_endvr_sermon_ref':
        	echo get_post_meta( $post_id , '_endvr_sermon_ref' , true ); 
        break;       
    }
} 

// custom rewrite rules
// http://wp.tutsplus.com/tutorials/creative-coding/the-rewrite-api-post-types-taxonomies/

function endvr_cpt_sermons_rewrite_rules() {
	add_rewrite_rule("^sermons/([^/]+)/([^/]+)/?",'index.php?post_type=sermons&sermonseries=$matches[1]&sermons=$matches[2]','top');
}
add_action('init','endvr_cpt_sermons_rewrite_rules'); 

function endvr_cpt_sermons_rewrite_rules_inject( $post_link, $id = 0 ) {

	$post = get_post($id);

	if ( is_wp_error($post) || 'sermons' != $post->post_type || empty($post->post_name) )
		return $post_link;

	// Get the taxonomy terms:
	$terms = get_the_terms($post->ID, 'sermonseries');

	if( is_wp_error($terms) || !$terms ) {
		$taxo = 'general';
	}
	else {
		$taxo_obj = array_pop($terms);
		$taxo = $taxo_obj->slug;
	}

	return home_url(user_trailingslashit( "sermons/$taxo/$post->post_name" ));
}
add_filter( 'post_type_link', 'endvr_cpt_sermons_rewrite_rules_inject' , 10, 2 );
