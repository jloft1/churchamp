<?php

// define the custom content type
// @ source: http://codex.wordpress.org/Function_Reference/register_post_type#Flushing_Rewrite_on_Activation
add_action( 'init', 'endvr_init_sermons' );
function endvr_init_sermons() 
{
  $labels = array(
    'name' 			=> _x( 'Sermons', 'post type general name' ),
    'singular_name' 	=> _x( 'Sermon', 'post type singular name' ),
    'add_new' 			=> _x( 'Add New', 'sermon' ),
    'add_new_item' 		=> __( 'Add New Sermon' ),
    'edit_item' 		=> __( 'Edit Sermon' ),
    'new_item' 		=> __( 'New Sermon' ),
    'view_item'		=> __( 'View Sermon' ),
    'search_items' 		=> __( 'Search Sermons' ),
    'not_found' 		=> __( 'No Sermons found' ),
    'not_found_in_trash' => __( 'No Sermons found in Trash' ), 
    'parent_item_colon' 	=> __( 'Parent Directory' )
  );
  $args = array(
    'labels' 			=> $labels,
    'public' 			=> true, // several of the other variables derive their values from 'public' by default
    'exclude_from_search'=> false,
    'publicly_queryable' => true,
    'show_ui' 			=> true,
    'show_in_nav_menus'	=> true,
    'show_in_menu' 		=> true,
    'show_in_admin_bar'	=> true,     
    'menu_position' 	=> 23, // should be 21-24 to appear between Pages and Comments in admin menu
    'menu_icon' 		=> '', // keep this line because it adds the .menu-icon-sermons class to the <li> HTML    
    'capability_type' 	=> 'post',
    'hierarchical' 		=> false,
    'supports' 		=> array( 'title','editor' ),
    'taxonomies' 		=> array( 'sermonseries', 'sermonspeaker' ),
    'has_archive' 		=> 'sermons', // must be explicitly set in order for rewrite rules to work
    'rewrite' 			=> array( 'slug' => 'sermons/%sermonseries%', 'with_front' => 'false' ),    
    'query_var' 		=> true,
    'can_export'		=> true
  ); 
  register_post_type( 'sermons',$args );
}

// add filter to insure the text Sermon, or sermon, is displayed when user updates a sermon
add_filter( 'post_updated_messages', 'endvr_updated_messages_sermons' );
function endvr_updated_messages_sermons( $messages ) {
global $post, $post_ID;
  $messages['sermons'] = array(
    0 	=> '', // Unused. Messages start at index 1.
    1 	=> sprintf( __('Sermon updated. <a href="%s">View sermon</a>'), esc_url( get_permalink($post_ID) ) ),
    2 	=> __('Custom field updated.'),
    3 	=> __('Custom field deleted.'),
    4 	=> __('Sermon updated.'),
    		// translators: %s: date and time of the revision
    5 	=> isset($_GET['revision']) ? sprintf( __('Sermon restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 	=> sprintf( __('Sermon published. <a href="%s">View sermon</a>'), esc_url( get_permalink($post_ID) ) ),
    7 	=> __('Sermon saved.'),
    8 	=> sprintf( __('Sermon submitted. <a target="_blank" href="%s">Preview sermon</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 	=> sprintf( __('Sermon scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview sermon</a>'),
      	// translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 	=> sprintf( __('Sermon draft updated. <a target="_blank" href="%s">Preview sermon</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}

// assign custom text instructions to replace "Enter title here"
function endvr_replace_title_label_sermons( $title ) {
     $screen = get_current_screen();
     if  ( 'sermons' == $screen->post_type ) {
          $title = 'Enter Sermon Title Here...';
          return $title;
     }
}
add_filter( 'enter_title_here', 'endvr_replace_title_label_sermons' ); 

// assign a custom label to the Sub Title
function endvr_replace_subtitle_label_sermons( $subtitle ) {
     $screen = get_current_screen();
     if  ( 'sermons' == $screen->post_type ) {
          $subtitle = str_replace( 'a Subtitle for this Content','the Sermon\'s Subtitle',$subtitle );
          return $subtitle;
     }  
}
add_filter( 'endvr_subtitle_label', 'endvr_replace_subtitle_label_sermons' );

// assign a custom label to the Post Editor + place it in a sortable Meta Box
// @source: http://wordpress.org/support/topic/move-custom-meta-box-above-editor?replies=17
// @source: http://software.troydesign.it/php/wordpress/move-wp-visual-editor.html
add_action( 'add_meta_boxes', 'endvr_add_meta_box_editor_sermons', 0 );
function endvr_add_meta_box_editor_sermons() {
     $screen = get_current_screen();
     if  ( 'sermons' == $screen->post_type ) {
		global $_wp_post_type_features;
		foreach ($_wp_post_type_features as $type => &$features) {
			if (isset($features['editor']) && $features['editor']) {
				unset($features['editor']);
				add_meta_box(
					'endvr_sermons_editor_description',
					__('Sermon Summary/Description'),
					'endvr_meta_box_editor_sermons',
					$type, 'normal', 'core'
				);
			}
		}
	}
	add_action( 'admin_head', 'endvr_admin_head_sermons' ); //white background
}
function endvr_admin_head_sermons() {
	?>
	<style type="text/css">
		.wp-editor-container{background-color:#fff;}
	</style>
	<?php
}
function endvr_meta_box_editor_sermons( $post ) {
	echo '<div class="wp-editor-wrap">';
	wp_editor($post->post_content, 'content', array('dfw' => true, 'tabindex' => 1) );
	echo '</div>';
}

// redefine the way the content type's admin panel index listing is displayed
// @source: http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column  
// @source: http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/   
add_filter( 'manage_edit-sermons_columns', 'endvr_set_custom_edit_columns_sermons' );
add_action( 'manage_sermons_posts_custom_column',  'endvr_custom_column_sermons', 10, 2 );
 
function endvr_set_custom_edit_columns_sermons($columns) {
	unset($columns['date']);
	return $columns 
		+ array(
			'_endvr_sermon_audio' 	=> __('Audio'),
			'_endvr_sermon_video' 	=> __('Video'),
			'_endvr_sermon_doc'		=> __('Outline'),
			'_endvr_sermon_ref' 	=> __('Reference'),		
		);
}		

function endvr_custom_column_sermons($column, $post_id) {
    switch ( $column ) {
      case '_endvr_sermon_audio':
      	echo get_post_meta( $post_id , '_endvr_sermon_audio' , true );
        break;
      case '_endvr_sermon_video':
        	echo get_post_meta( $post_id , '_endvr_sermon_video' , true ); 
        break;
      case '_endvr_sermon_doc':
        	echo get_post_meta( $post_id , '_endvr_sermon_doc' , true ); 
        break; 
      case '_endvr_sermon_ref':
        	echo get_post_meta( $post_id , '_endvr_sermon_ref' , true ); 
        break;       
    }
} 

// flush rewrite rules upon activation
// @source: http://codex.wordpress.org/Function_Reference/register_post_type#Flushing_Rewrite_on_Activation
function endvr_rewrite_flush_sermons() {
	endvr_init_sermons();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'endvr_rewrite_flush_sermons' );

// custom rewrite rules
// @source: http://wordpress.stackexchange.com/questions/21022/mixing-custom-post-type-and-taxonomy-rewrite-structures
// @fix: http://wordpress.stackexchange.com/questions/60570/array-pop-expects-array-boolean-given-for-event-type-term-slug
add_filter('post_type_link', 'endvr_permalink_sermonseries', 10, 4);
function endvr_permalink_sermonseries($post_link, $post, $leavename, $sample)
{
	if ( false !== strpos( $post_link, '%sermonseries%' ) ) {		
		$sermonseries = get_the_terms( $post->ID, 'sermonseries' );
		if ( !empty($sermonseries) ) { // this is necessary to avoid error if get_the_terms is empty which it will be when adding a new sermon
			$post_link = str_replace( '%sermonseries%', array_pop( $sermonseries )->slug, $post_link );
		}
	}
	return $post_link;
}
add_filter('post_type_link', 'endvr_permalink_sermonspeaker', 10, 4); // CURRENTLY NOT WORKING ???
function endvr_permalink_sermonspeaker($post_link, $post, $leavename, $sample)
{
	if ( false !== strpos( $post_link, '%sermonspeaker%' ) ) {
		$sermonspeaker = get_the_terms( $post->ID, 'sermonspeaker' );
		if ( !empty($sermonseries) ) { // this is necessary to avoid error if get_the_terms is empty which it will be when adding a new sermon
			$post_link = str_replace( '%sermonspeaker%', array_pop( $sermonspeaker )->slug, $post_link );
		}
	}
	return $post_link;
}