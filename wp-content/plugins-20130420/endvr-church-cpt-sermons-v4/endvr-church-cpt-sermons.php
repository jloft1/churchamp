<?php
/**
 * @package endvr-church-cpt-sermons
 */
/*
Plugin Name: Endeavr - Church (Sermons Module v4)
Plugin URI: http://endeavr.com
Description: Essential functions for a standard Endeavr church website. [Sermons Module]
Author: Jason Loftis (jLOFT)
Author URI: http://jloft.com
Version: 4.0.0
Notes: This version of the plugin rewrites the permalinks as follows...
post-type archive = /sermons/
post-type single = /sermons/series-name/sermons-name
taxonomy missioncountry = /sermons/series/
taxonomy missionagency = /sermons/speaker/
*/

// Taxonomies
// It's important to register the taxonomies before the post type in order for the rewrite rules to work properly
include_once ( 'taxonomies/tax-sermon-series.php' ); 
include_once ( 'taxonomies/tax-sermon-speaker.php' );
include_once ( 'taxonomies/tax-sermon-scripture.php' );

// Post Types
include_once ( 'post-types/cpt-sermons.php' );

// Metaboxes
include_once ( 'metaboxes/mb-sermons.php' );
include_once ( 'metaboxes/mb-sermonseries.php' );

// Admin Styles
function endvr_admin_head_style_sermons() {
	echo '<link rel="stylesheet" type="text/css" href="' .plugins_url('styles/style-sermons.css', __FILE__). '">';
}
add_action('admin_head', 'endvr_admin_head_style_sermons');