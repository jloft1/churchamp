<?php
//hook into the init action and call endvr_create_taxonomy_missioncountry when it fires
add_action( 'init', 'endvr_create_taxonomy_missioncountry', 0 );

//create mission country taxomony
function endvr_create_taxonomy_missioncountry() 
{

	$labels = array(
		'name' 				=> _x( 'Mission Country', 'taxonomy general name' ),
		'singular_name' 		=> _x( 'Mission Country', 'taxonomy singular name' ),
		'search_items' 		=> __( 'Search for Mission Country' ),
		'all_items' 			=> __( 'All Mission Countries' ),
		'parent_item' 			=> __( 'Parent Mission Country' ),
		'parent_item_colon' 	=> __( 'Parent Mission Country:' ),
		'edit_item' 			=> __( 'Edit Mission Country' ), 
		'update_item' 			=> __( 'Update Mission Country' ),
		'add_new_item' 		=> __( 'Add New Mission Country' ),
		'new_item_name' 		=> __( 'New Mission Country Name' ),
		'menu_name'			=> __( 'Mission Country' )
	);
	 	
	$args = array(
		'hierarchical' 		=> true, // if set to true, the taxonomy will function like categories, and false = functionality like tags
		'labels' 				=> $labels,
		'show_ui' 			=> true,
		'show_admin_column' 	=> true, // this will add a sermon series column to the index list for the missionaries post type in the admin panel
		'query_var' 			=> 'missioncountry',
		'rewrite' 			=> array( 'slug' => 'missions/missionaries/country' )	
	);
	
	register_taxonomy( 'missioncountry', array( 'missionaries' ), $args );
	//flush_rewrite_rules();
}