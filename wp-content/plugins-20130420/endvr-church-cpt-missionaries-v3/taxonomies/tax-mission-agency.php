<?php
//hook into the init action and call endvr_create_taxonomy_missionagency when it fires
add_action( 'init', 'endvr_create_taxonomy_missionagency', 0 );

//create mission country taxomony
function endvr_create_taxonomy_missionagency() 
{

	$labels = array(
		'name' 				=> _x( 'Mission Agency', 'taxonomy general name' ),
		'singular_name' 		=> _x( 'Mission Agency', 'taxonomy singular name' ),
		'search_items' 		=> __( 'Search for Mission Agency' ),
		'all_items' 			=> __( 'All Mission Agencies' ),
		'parent_item' 			=> __( 'Parent Mission Agency' ),
		'parent_item_colon' 	=> __( 'Parent Mission Agency:' ),
		'edit_item' 			=> __( 'Edit Mission Agency' ), 
		'update_item' 			=> __( 'Update Mission Agency' ),
		'add_new_item' 		=> __( 'Add New Mission Agency' ),
		'new_item_name' 		=> __( 'New Mission Agency Name' ),
		'menu_name'			=> __( 'Mission Agency' )
	);
	 	
	$args = array(
		'hierarchical' 		=> true, // if set to true, the taxonomy will function like categories, and false = functionality like tags
		'labels' 				=> $labels,
		'show_ui' 			=> true,
		'show_admin_column' 	=> true, // this will add a sermon series column to the index list for the missionaries post type in the admin panel
		'query_var' 			=> 'missionagency',
		'rewrite' 			=> array( 'slug' => 'missions/missionaries/agency' )	
	);
	
	register_taxonomy( 'missionagency', array( 'missionaries' ), $args );
	//flush_rewrite_rules();
}

// Add custom meta box to mission country taxonomy Edit Term page
// @source: http://pippinsplugins.com/adding-custom-meta-fields-to-taxonomies/
function endvr_taxonomy_add_new_meta_field_missionagency() {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
		<label for="term_meta[missionagency_website]"><?php _e( 'Mission Agency Website', 'endvr' ); ?></label>
		<input type="text" name="term_meta[missionagency_website]" id="term_meta[missionagency_website]" value="">
		<p class="description"><?php _e( 'Enter the mission agency\'s website URL.','endvr' ); ?></p>
	</div>
<?php
}
add_action( 'missionagency_add_form_fields', 'endvr_taxonomy_add_new_meta_field_missionagency', 10, 2 ); // {$taxonomy_name}_add_form_fields

// Add HTML to Edit Term page + check to see if the meta field has any data saved already, so that we can populate the field on load.
function endvr_taxonomy_edit_meta_field_missionagency($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "missionagency_$t_id" ); ?>
	<tr class="form-field">
	<th scope="row" valign="top"><label for="term_meta[missionagency_website]"><?php _e( 'Mission Agency Website', 'endvr' ); ?></label></th>
		<td>
			<input type="text" name="term_meta[missionagency_website]" id="term_meta[missionagency_website]" value="<?php echo esc_attr( $term_meta['missionagency_website'] ) ? esc_attr( $term_meta['missionagency_website'] ) : ''; ?>">
			<p class="description"><?php _e( 'Enter the mission agency\'s website URL.','endvr' ); ?></p>
		</td>
	</tr>
<?php
}
add_action( 'missionagency_edit_form_fields', 'endvr_taxonomy_edit_meta_field_missionagency', 10, 2 ); // {$taxonomy_name}_edit_form_fields

// Save extra taxonomy fields callback function.
function endvr_save_taxonomy_custom_meta_missionagency( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "missionagency_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "missionagency_$t_id", $term_meta );
	}
}  
add_action( 'edited_missionagency', 'endvr_save_taxonomy_custom_meta_missionagency', 10, 2 ); // edited_{$taxonomy_name}
add_action( 'create_missionagency', 'endvr_save_taxonomy_custom_meta_missionagency', 10, 2 ); // create_{$taxonomy_name}