<?php

// define the custom content type
// @ source: http://codex.wordpress.org/Function_Reference/register_post_type
add_action('init', 'endvr_init_missionaries');
function endvr_init_missionaries() 
{
  $labels = array(
    'name' 			=> _x('Missionaries', 'post type general name'),
    'singular_name' 	=> _x('Missionary', 'post type singular name'),
    'add_new' 			=> _x('Add New', 'missionary'),
    'add_new_item' 		=> __('Add New Missionary'),
    'edit_item' 		=> __('Edit Missionary'),
    'new_item' 		=> __('New Missionary'),
    'view_item' 		=> __('View Missionary'),
    'search_items' 		=> __('Search Missionaries'),
    'not_found' 		=> __('No Missionaries found'),
    'not_found_in_trash' => __('No Missionaries found in Trash'), 
    'parent_item_colon' 	=> __('Parent Directory')
  );
  $args = array(
    'labels' 			=> $labels,
    'public' 			=> true, // several of the other variables derive their values from 'public' by default
    'exclude_from_search'=> false,
    'publicly_queryable' => true,
    'show_ui' 			=> true,
    'show_in_nav_menus'	=> true,
    'show_in_menu' 		=> true,
    'show_in_admin_bar'	=> true,
    'menu_position' 	=> 32, // should be 21-24 to appear between Pages and Comments in admin menu  
    'menu_icon' 		=> '', // keep this line because it adds the .menu-icon-missionaries class to the <li> HTML     
    'capability_type' 	=> 'page',
    'hierarchical' 		=> true,
    'supports' 		=> array( 'title','editor' ),
    'taxonomies' 		=> array( 'missioncountry', 'missionagency' ),
    'has_archive' 		=> 'missions/missionaries', // must be explicitly set in order for rewrite rules to work
    'rewrite' 			=> array( 'slug' => 'missions/missionaries', 'with_front' => 'false' ),    
    'query_var' 		=> true,
    'can_export'		=> true
  ); 
  register_post_type('missionaries',$args);
}

// add filter to insure the text Missionary, or missionary, is displayed when user updates a missionary 
add_filter('post_updated_messages', 'endvr_updated_messages_missionaries');
function endvr_updated_messages_missionaries( $messages ) {
global $post, $post_ID;
  $messages['missionaries'] = array(
    0 	=> '', // Unused. Messages start at index 1.
    1 	=> sprintf( __('Missionary updated. <a href="%s">View missionary</a>'), esc_url( get_permalink($post_ID) ) ),
    2 	=> __('Custom field updated.'),
    3 	=> __('Custom field deleted.'),
    4 	=> __('Missionary updated.'),
    		// translators: %s: date and time of the revision
    5 	=> isset($_GET['revision']) ? sprintf( __('Missionary restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 	=> sprintf( __('Missionary published. <a href="%s">View missionary</a>'), esc_url( get_permalink($post_ID) ) ),
    7 	=> __('Missionary saved.'),
    8 	=> sprintf( __('Missionary submitted. <a target="_blank" href="%s">Preview missionary</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 	=> sprintf( __('Missionary scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview missionary</a>'),
      	// translators: Publish box date format, see http://php.net/date
      	date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 	=> sprintf( __('Missionary draft updated. <a target="_blank" href="%s">Preview missionary</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
     
// assign custom text instructions to replace "Enter title here"
function endvr_replace_title_label_missionaries( $title ){
     $screen = get_current_screen();
     if  ( 'missionaries' == $screen->post_type ) {
          $title = 'Enter Missionary Name Here (Last Name, First Name)...';
          return $title;
     }
}
add_filter( 'enter_title_here', 'endvr_replace_title_label_missionaries' );

// assign a custom label to the Sub Title (not that relevant as long as the remove meta box function is active below this)
// @source: http://codex.wordpress.org/Function_Reference/remove_filter
function endvr_subtitle_label_missionaries() {
	echo 'Input a Subtitle for this Missionary (Optional)';
}
function endvr_replace_subtitle_label_missionaries() {
     $screen = get_current_screen();
     if  ( 'missionaries' == $screen->post_type ) {
     	remove_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_default' );
     	add_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_missionaries' );
     }  
}
add_action( 'admin_head', 'endvr_replace_subtitle_label_missionaries' );

// this function removes the Sub Title metabox from this post type
// @source: http://www.webworkgarage.com/2012/07/how-to-remove-meta-boxes-from-custom-post-types-on-wordpress/
function endvr_remove_subtitle_meta_box_missionaries () {
	remove_meta_box( 'endvr_feature_subtitle', 'missionaries', 'normal' );
}
add_filter ( 'add_meta_boxes', 'endvr_remove_subtitle_meta_box_missionaries', 999 ); // note the 999 priority is crucial	

// assign a custom label to the Post Editor
// http://wordpress.org/support/topic/move-custom-meta-box-above-editor?replies=17
// http://software.troydesign.it/php/wordpress/move-wp-visual-editor.html
add_action( 'add_meta_boxes', 'endvr_add_meta_box_editor_missionaries', 0 );
function endvr_add_meta_box_editor_missionaries() {
     $screen = get_current_screen();
     if  ( 'missionaries' == $screen->post_type ) {
		global $_wp_post_type_features;
		foreach ($_wp_post_type_features as $type => &$features) {
			if (isset($features['editor']) && $features['editor']) {
				unset($features['editor']);
				add_meta_box(
					'endvr_editor_description_missionaries',
					__('Missionary Profile'),
					'endvr_meta_box_editor_missionaries',
					$type, 'normal', 'core'
				);
			}
		}
	}
	add_action( 'admin_head', 'endvr_admin_head_missionaries'); //white background
}
function endvr_admin_head_missionaries() {
	?>
	<style type="text/css">
		.wp-editor-container{background-color:#fff;}
	</style>
	<?php
}
function endvr_meta_box_editor_missionaries( $post ) {
	echo '<div class="wp-editor-wrap">';
	wp_editor($post->post_content, 'content', array('dfw' => true, 'tabindex' => 1) );
	echo '</div>';
}

// redefine the way the content type's control panel index listing is displayed
// http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column  
// http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/   

add_filter("manage_edit-missionaries_columns", "set_custom_edit_missionaries_columns");
add_action("manage_missionaries_posts_custom_column",  "custom_missionaries_column", 10, 2);
 
function set_custom_edit_missionaries_columns($columns){
	unset($columns['date']);
	return $columns 
		+ array(
			'_endvr_missionary_org' 		=> __('Organization'),		
			'_endvr_missionary_role' 		=> __('Position'),
			'_endvr_missionary_email'		=> __('Email'),
			'_endvr_missionary_photo_thumb' 	=> __('Thumbnail'),
			'_endvr_missionary_photo_full' 	=> __('Full Photo'),		
		);
}		

function custom_missionaries_column($column, $post_id){
    switch ( $column ) {
      case '_endvr_missionary_org':
      	echo get_post_meta( $post_id , '_endvr_missionary_org' , true );
        break;    
      case '_endvr_missionary_role':
      	echo get_post_meta( $post_id , '_endvr_missionary_role' , true );
        break;
      case '_endvr_missionary_email':
        	echo get_post_meta( $post_id , '_endvr_missionary_email' , true ); 
        break; 
      case '_endvr_missionary_photo_thumb': ?>
        	<img src="<?php echo get_post_meta( $post_id , '_endvr_missionary_photo_thumb' , true ); ?>" width="133"> 
        <?php
        break;  
      case '_endvr_missionary_photo_full': ?>
        	<img src="<?php echo get_post_meta( $post_id , '_endvr_missionary_photo_full' , true ); ?>"width="100"> 
        <?php
        break;                     
    }
}        

// flush rewrite rules upon activation
// @source: http://codex.wordpress.org/Function_Reference/register_post_type#Flushing_Rewrite_on_Activation
function endvr_rewrite_flush_missionaries() {
	endvr_init_missionaries();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'endvr_rewrite_flush_missionaries' );