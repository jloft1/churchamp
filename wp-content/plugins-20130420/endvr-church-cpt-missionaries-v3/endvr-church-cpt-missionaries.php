<?php
/**
 * @package endvr-church-cpt-missionaries
 */
/*
Plugin Name: Endeavr - Church (Missionary Directory Module v3)
Plugin URI: http://endeavr.com
Description: Essential functions for a standard Endeavr church website. [Missionary Directory Module]
Author: Jason Loftis (jLOFT)
Author URI: http://jloft.com
Version: 3.0
Notes: This version of the plugin rewrites the permalinks as follows...
post-type archive = /missions/missionaries/
post-type single = /missions/missionaries/missionary-name
taxonomy missioncountry = /missions/missionaries/country/
taxonomy missionagency = /missions/missionaries/agency/
*/

// Taxonomies
// It's important to register the taxonomies before the post type in order for the rewrite rules to work properly
include_once ( 'taxonomies/tax-mission-country.php' );
include_once ( 'taxonomies/tax-mission-agency.php' );

// Post Types
include_once ( 'post-types/cpt-missionaries.php' );

// Metaboxes
include_once ( 'metaboxes/mb-missionary-details.php' );
include_once ( 'metaboxes/mb-missionary-news.php' );
include_once ( 'metaboxes/mb-missionary-photos.php' );

// Admin Styles
function endvr_admin_head_style_missionaries() {
        echo '<link rel="stylesheet" type="text/css" href="' .plugins_url('styles/style-missionaries.css', __FILE__). '">';
}
add_action('admin_head', 'endvr_admin_head_style_missionaries');