<?php
/**
 * @package endvr-gmap
 */
/*
Plugin Name: Endeavr Widget - Google Map
Plugin URI: http://endeavr.com/dev/wp/plugins/endvr-widget-gmap/
Description: Google Map widget function.
Author: Jason Loftis (JLOFT)
Author URI: http://jloft.com
Version: 1.0
*/

class endvr_widget_gmap extends WP_Widget {

    function endvr_widget_gmap() {
        parent::WP_Widget(false, $name = 'Endeavr Widget - Google Map');
    }

    function widget($args, $instance) {
    
		wp_enqueue_script('mobile-gmap', WP_PLUGIN_URL . '/endvr-gmap/js/jquery.gmap.js', array('jquery'), '1.0', true);
		wp_enqueue_script('gmap-api', 'http://maps.googleapis.com/maps/api/js?key=AIzaSyDfxBPeVbIF-t0SZ0-WkpYughLRTbOJoc4&sensor=false', array('jquery'), '1.0', true);
		
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        ?>
            <?php echo $before_widget; ?>
            
            <?php include_once plugins_url('ready-function.php', __FILE__); ?>
            
                <?php if ( $title )
                    echo $before_title . $title . $after_title;  else echo '<div class="widget-body clear">'; ?>                      
                    
				<div class="gmap" data-center="<?php echo get_option('gmap_street'); ?> <?php echo get_option('gmap_city'); ?> <?php echo get_option('gmap_state'); ?> <?php echo get_option('gmap_zip'); ?>" data-address="<?php echo get_option('gmap_street'); ?> <?php echo get_option('gmap_city'); ?> <?php echo get_option('gmap_state'); ?> <?php echo get_option('gmap_zip'); ?>" data-zoom="<?php echo get_option('gmap_zoom'); ?>" data-mapTitle="<?php echo get_option('gmap_location'); ?>" style="height:<?php echo get_option('gmap_height'); ?>;width:100%;"></div>
				<br />
			    <address class="endvr_contact connect_postal">
			    <i class="connect_icon">&nbsp;</i>
			    <?php echo get_option('gmap_street'); ?>, <?php echo get_option('gmap_city'); ?>, <?php echo get_option('gmap_state'); ?> <?php echo get_option('gmap_zip'); ?><br />
			      <a class="alignright" href="http://maps.google.com/maps?q=<?php echo get_option('gmap_location'); ?>+<?php echo get_option('gmap_street'); ?>+<?php echo get_option('gmap_city'); ?>+<?php echo get_option('gmap_state'); ?>+<?php echo get_option('gmap_zip'); ?>" title="Get Directions">Get Directions</a>
			    </address>

            <?php echo $after_widget; ?>

    	<?php
    	}

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        
        update_option('gmap_location', $_POST['gmap_location']);
        update_option('gmap_street', $_POST['gmap_street']);
        update_option('gmap_city', $_POST['gmap_city']);
        update_option('gmap_state', $_POST['gmap_state']);
        update_option('gmap_zip', $_POST['gmap_zip']);
        update_option('gmap_zoom', $_POST['gmap_zoom']);        
        update_option('gmap_height', $_POST['gmap_height']);
                
        return $instance;
        
    }

    function form($instance) {

        $title = esc_attr($instance['title']);
        ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
            
            <div style="margin-bottom: 5px;">
                <p class="gmap_options">
                    <label for="gmap_location">Location:</label>
                    <input type="text" name="gmap_location" id="gmap_location" class="widefat" value="<?php echo get_option('gmap_location'); ?>"/>
                </p>
            </div> 
            
            <div style="margin-bottom: 5px;">
                <p class="gmap_options">
                    <label for="gmap_street">Street Address:</label>
                    <input type="text" name="gmap_street" id="gmap_street" class="widefat" value="<?php echo get_option('gmap_street'); ?>"/>
                </p>
            </div> 
            
            <div style="margin-bottom: 5px;">
                <p class="gmap_options">
                    <label for="gmap_city">City:</label>
                    <input type="text" name="gmap_city" id="gmap_city" class="widefat" value="<?php echo get_option('gmap_city'); ?>"/>
                </p>
            </div> 
            
            <div style="margin-bottom: 5px;">
                <p class="gmap_options">
                    <label for="gmap_state">State:</label>
                    <input type="text" name="gmap_state" id="gmap_state" class="widefat" value="<?php echo get_option('gmap_state'); ?>"/>
                </p>
            </div> 
            
            <div style="margin-bottom: 5px;">
                <p class="gmap_options">
                    <label for="gmap_zip">Zip:</label>
                    <input type="text" name="gmap_zip" id="gmap_zip" class="widefat" value="<?php echo get_option('gmap_zip'); ?>"/>
                </p>
            </div>  
            
            <div style="margin-bottom: 5px;">
                <p class="gmap_options">
                    <label for="gmap_zoom">Zoom Level<br />(1-18 but 16 Recommended):</label>
                    <input type="text" name="gmap_zoom" id="gmap_zoom" class="widefat" value="<?php echo get_option('gmap_zoom'); ?>"/>
                </p>
            </div> 
            
            <div style="margin-bottom: 5px;">
                <p class="gmap_options">
                    <label for="gmap_height">Map Height (i.e. 100px):</label>
                    <input type="text" name="gmap_height" id="gmap_height" class="widefat" value="<?php echo get_option('gmap_height'); ?>"/>
                </p>
            </div>                                                                                

        <?php
    }

}

add_action('widgets_init', create_function('', 'return register_widget("endvr_widget_gmap");'));

?>