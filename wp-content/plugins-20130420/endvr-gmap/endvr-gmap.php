<?php
/**
 * @package endvr-gmap
 */
/*
Plugin Name: Endeavr Plugin - Google Maps
Plugin URI: http://endeavr.com/dev/wp/plugins/endvr-gmap/
Description: Google Map functions for this Endeavr client website. It includes a shortcode and a widget. This makes Google Maps responsive for mobile devices.
Author: Jason Loftis (JLOFT)
Author URI: http://jloft.com
Version: 1.0
*/

// Resource: Jochen Vandendriessche (http://builtbyrobot.com)

// Shortcode
include_once ( 'endvr-shortcode-gmap.php' );

// Widget
include_once ( 'endvr-widget-gmap.php' );

?>