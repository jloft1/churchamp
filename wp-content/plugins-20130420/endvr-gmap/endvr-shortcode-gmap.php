<?php
/**
 * @package endvr-gmap
 */
/*
Plugin Name: Endeavr Shortcode - Google Map
Plugin URI: http://endeavr.com/dev/wp/plugins/endvr-gmap/
Description: Google Map shortcode function.
Author: Jason Loftis (JLOFT)
Author URI: http://jloft.com
Version: 1.0
*/

// Resource: http://scribu.net/wordpress/optimal-script-loading.html

class endvr_shortcode_gmap {

	static $add_endvr_gmap;

	static function init() {
	
		add_shortcode('gmap', array(__CLASS__, 'endvr_gmap'));
		add_action('init', array(__CLASS__, 'register_script'));
		add_action('wp_footer', array(__CLASS__, 'print_script'));
	}
	
	static function endvr_gmap ( $atts ) {
	
		self::$add_endvr_gmap = true;
	
		$atts = shortcode_atts(
		array(
			'gmap_location' => 'FBC Prescott',
			'gmap_street' => '148 S Marina St',
			'gmap_city' => 'Prescott',
			'gmap_state' => 'Arizona',
			'gmap_zip' => '86303',
			'gmap_zoom' => '15',
			'gmap_height' => '550px'
		), 
		$atts);
		
		extract($atts);
		
		$html = "
		  <div class=\"gmap\" data-center=\"$gmap_street $gmap_city $gmap_state $gmap_zip\" data-address=\"$gmap_street $gmap_city $gmap_state $gmap_zip\" data-zoom=\"$gmap_zoom\" data-mapTitle=\"$gmap_location\" style=\"height:$gmap_height;width:100%;\"></div>
		    <br />
		    <address>
		      <strong>$gmap_location</strong><br />
		      $gmap_street<br />
		      $gmap_city, $gmap_state $gmap_zip
		    </address>
		    <a href=\"http://maps.google.com/maps?q=$gmap_location,+$gmap_street,+$gmap_city,+$gmap_state+$gmap_zip\" title=\"Get Directions\">Get Directions</a>
		";
		return $html;
		 
	}
	
	static function register_script() {
		wp_register_script('mobile-gmap', plugins_url('js/jquery.gmap.js', __FILE__), array('jquery'), '1.0', true );
		wp_register_script('gmap-api', 'http://maps.googleapis.com/maps/api/js?key=AIzaSyDfxBPeVbIF-t0SZ0-WkpYughLRTbOJoc4&sensor=false' );
	}
	
	static function print_script () {
	
		if ( ! self::$add_endvr_gmap )
			return;
			
		wp_print_scripts(array('mobile-gmap','gmap-api'));
		include_once WP_PLUGIN_URL . '/endvr-gmap/js/ready-func.html';
	}

}

endvr_shortcode_gmap::init();

?>