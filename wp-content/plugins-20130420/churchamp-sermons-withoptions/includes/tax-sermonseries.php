<?php
/**
 * Taxonomy ( Register Sermon Series )
 * @package  ChurchAmp_Sermons
 * @subpackage  Includes
 * @version  5.0.0
 * @since   1.0.0
 * @author  Endeavr Media <support@endeavr.com>
 * @copyright  Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   http://churchamp.com/plugins/sermons
 * @license  http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* register and define the taxonomy on the 'init' hook */
/* @example: http://codex.wordpress.org/Function_Reference/register_taxonomy */
add_action( 'init', 'endvr_register_tax_sermonseries' );
function endvr_register_tax_sermonseries() {

	/* set some variables */
	$singular_c_series 		= get_field('endvr_set_label_singular_capital_sermons_tax_series', 'options');
	$plural_c_series 		= get_field('endvr_set_label_plural_capital_sermons_tax_series', 'options');
	$archive 				= get_field('endvr_set_archive_slug_sermons', 'options');
	$base_series 			= get_field('endvr_set_base_slug_sermons_tax_series', 'options');

	$labels = array(
		'name'                       	=> __( $plural_c_series,                           		'churchamp-sermons' ),
		'singular_name'              	=> __( $singular_c_series,                            		'churchamp-sermons' ),
		'menu_name'                  	=> __( $plural_c_series,                           		'churchamp-sermons' ),
		'name_admin_bar'             	=> __( 'Portfolio',                            			'churchamp-sermons' ),
		'search_items'               	=> __( 'Search '.$plural_c_series.'',                    	'churchamp-sermons' ),
		'popular_items'              	=> __( 'Popular '.$plural_c_series.'',                   	'churchamp-sermons' ),
		'all_items'                  	=> __( 'All '.$plural_c_series.'',                       	'churchamp-sermons' ),
		'edit_item'                  	=> __( 'Edit '.$singular_c_series.'',                       'churchamp-sermons' ),
		'view_item'                  	=> __( 'View '.$singular_c_series.'',                       'churchamp-sermons' ),
		'update_item'                	=> __( 'Update '.$singular_c_series.'',                     'churchamp-sermons' ),
		'add_new_item'               	=> __( 'Add New '.$singular_c_series.'',                    'churchamp-sermons' ),
		'new_item_name'             	=> __( 'New '.$singular_c_series.' Name',                	'churchamp-sermons' ),
		'separate_items_with_commas' 	=> __( 'Separate '.$plural_c_series.' with Commas',      	'churchamp-sermons' ),
		'add_or_remove_items'        	=> __( 'Add or Remove '.$plural_c_series.'',             	'churchamp-sermons' ),
		'choose_from_most_used'      	=> __( 'Choose from the Most Used '.$plural_c_series.'',	'churchamp-sermons' ),
	);
	/* only 2 caps are needed: 'manage_sermons' and 'edit_sermons'. */
	$capabilities = array(
		'manage_terms' 			=> 'manage_sermons',
		'edit_terms'   			=> 'manage_sermons',
		'delete_terms' 			=> 'manage_sermons',
		'assign_terms' 			=> 'edit_sermons',
	);
	$rewrite = array(
		'slug'         			=> !empty( $base_series ) ? "{$archive}/{$base_series}" : $archive,
		'with_front'   			=> false,
		'hierarchical' 			=> false,
		'ep_mask'      			=> EP_NONE,
	);
	$args = array(
		'public'            		=> true,
		'show_ui'           		=> true,
		'show_in_nav_menus' 		=> true,
		'show_tagcloud'     		=> false,
		'show_admin_column' 		=> true,
		'hierarchical'      		=> true,
		'query_var'         		=> $base_series,
		'capabilities' 			=> $capabilities,
		'rewrite' 				=> $rewrite,
		'labels' 					=> $labels,
	);

	/* register the 'sermonseries' taxonomy. */
	register_taxonomy( 'sermonseries', array( 'sermons' ), $args );
}