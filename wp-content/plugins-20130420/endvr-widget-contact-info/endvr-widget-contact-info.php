<?php
/**
 * @package endvr-widget-contact-info
 */
/*
Plugin Name: Endeavr Widget - Contact Information
Plugin URI: http://endeavr.com/dev/wp/plugins/endvr-widget-contact-info/
Description: This is a widget for displaying contact information.
Author: Jason Loftis (JLOFT)
Author URI: http://jloft.com
Version: 1.0

Tutorial on Obfuscating Email in WordPress:
http://briteweb.com/blog/wordpress/protect-email-from-spambots-wordpress/
*/

class endvr_widget_contact_info extends WP_Widget {

    function endvr_widget_contact_info() {
        parent::WP_Widget(false, $name = 'Endeavr Widget - Contact Info');
    }

    function widget($args, $instance) {
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        ?>
            <?php echo $before_widget; ?>
                <?php if ( $title )
                    echo $before_title . $title . $after_title;  else echo '<div class="widget-body clear">'; ?>

                    <?php if ( get_option('phone') ) : ?>
                    <div class="endvr_contact connect_phone">
                    <i class="connect_icon">&nbsp;</i>
                    PH: &nbsp; <?php echo get_option('phone'); ?>
                    </div>
                    <?php endif; ?>
                    
                    <?php if ( get_option('fax') ) : ?>
                    <div class="endvr_contact connect_fax">
                    <i class="connect_icon">&nbsp;</i>
                    FX: &nbsp; <?php echo get_option('fax'); ?>
                    </div>
                    <?php endif; ?>
                    
                    <?php if ( get_option('postal') ) : ?>
                    <div class="endvr_contact connect_postal">
                    <i class="connect_icon">&nbsp;</i>
                    <?php echo get_option('postal'); ?>
                    </div>
                    <?php endif; ?>

                    <?php if ( get_option('email') ) : ?>
                    <div class="endvr_contact connect_email">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="mailto:<?php $emailaddy = get_option('email'); echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
                    </div>
                    <?php endif; ?>

                    <?php if ( get_option('contact_form_url') ) : ?>
                    <div class="endvr_contact connect_contact_form">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="<?php echo get_option('contact_form_url'); ?>" title="Contact Us" target="_self">Online Contact Form</a>
                    </div>
                    <?php endif; ?>

            <?php echo $after_widget; ?>
        <?php
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        
        update_option('phone', $_POST['phone']);
        update_option('fax', $_POST['fax']);
        update_option('postal', $_POST['postal']);
        update_option('email', $_POST['email']);
        update_option('contact_form_url', $_POST['contact_form_url']);
        
        return $instance;
    }

    function form($instance) {

        $title = isset($instance['title'] ) ? esc_attr( $instance['title'] ) : '';
        ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>

            <script type="text/javascript">
                (function($) {
                    $(function() {
                        $('.contact_options').hide();
                        $('.contact_title').toggle(
                            function(){ $(this).next().slideDown(100) },
                            function(){ $(this).next().slideUp(100) }
                        );
                    })
                })(jQuery)
            </script>
            
            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="contact_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Phone</a>
                <p class="contact_options">
                    <label for="phone">Phone Number:</label>
                    <input type="text" name="phone" id="phone" class="widefat" value="<?php echo get_option('phone'); ?>"/>
                </p>
            </div> 
            
            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="contact_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Fax</a>
                <p class="contact_options">
                    <label for="fax">Fax Number:</label>
                    <input type="text" name="fax" id="fax" class="widefat" value="<?php echo get_option('fax'); ?>"/>
                </p>
            </div>
            
            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="contact_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Address</a>
                <p class="contact_options">
                    <label for="postal">Postal Address:</label>
                    <input type="text" name="postal" id="postal" class="widefat" value="<?php echo get_option('postal'); ?>"/>
                </p>
            </div>
            
            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="contact_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Email</a>
                <p class="contact_options">
                    <label for="email">Email Address:</label>
                    <input type="text" name="email" id="email" class="widefat" value="<?php echo get_option('email'); ?>"/>
                </p>
            </div>
            
            <div style="margin-bottom: 5px;">
                <a href="javascript: void(0);" class="contact_title" style="font-size: 13px; display: block; margin-bottom: 5px;">Contact Form</a>
                <p class="contact_options">
                    <label for="contact_form_url">Contact Form's Page URL:</label>
                    <input type="text" name="contact_form_url" id="contact_form_url" class="widefat" value="<?php echo get_option('contact_form_url'); ?>"/>
                </p>
            </div>                                                           

        <?php
    }

}
add_action('widgets_init', create_function('', 'return register_widget("endvr_widget_contact_info");'));

?>