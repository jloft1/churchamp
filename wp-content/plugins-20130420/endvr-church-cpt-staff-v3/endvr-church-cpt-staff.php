<?php
/**
 * @package endvr-church-cpt-staff
 */
/*
Plugin Name: Endeavr - Church (Staff Directory Module v3)
Plugin URI: http://endeavr.com
Description: Essential functions for a standard Endeavr church website. [Staff Directory Module]
Author: Jason Loftis (JLOFT)
Author URI: http://jloft.com
Version: 3.0
*/

// Taxonomies
include_once ( 'taxonomies/tax-staff-role.php' );

// Post Types
include_once ( 'post-types/cpt-staff.php' );

// Metaboxes
include_once ( 'metaboxes/mb-staff-details.php' );
include_once ( 'metaboxes/mb-staff-photos.php' );

// Admin Styles
function admin_head_staff() {
        echo '<link rel="stylesheet" type="text/css" href="' .plugins_url('styles/style-staff.css', __FILE__). '">';
}
add_action('admin_head', 'admin_head_staff');

?>