<?php

// define the custom content type
// @ source: http://codex.wordpress.org/Function_Reference/register_post_type
add_action('init', 'endvr_init_staff');
function endvr_init_staff() 
{
  $labels = array(
    'name' 				=> _x('Staff', 'post type general name'),
    'singular_name' 		=> _x('Staff Person', 'post type singular name'),
    'add_new' 				=> _x('Add New', 'staff person'),
    'add_new_item' 			=> __('Add New Staff Person'),
    'edit_item' 			=> __('Edit Staff Person'),
    'new_item' 			=> __('New Staff Person'),
    'view_item' 			=> __('View Staff Person'),
    'search_items' 			=> __('Search Staff'),
    'not_found' 			=> __('No Staff found'),
    'not_found_in_trash' 	=> __('No Staff found in Trash'), 
    'parent_item_colon' 		=> __('Parent Directory')
  );
  $args = array(
    'labels' 				=> $labels,
    'public' 				=> true, // several of the other variables derive their values from 'public' by default
    'publicly_queryable' 	=> true,
    'show_ui' 				=> true,
    'show_in_nav_menus'		=> true,
    'show_in_menu' 			=> true,
    'show_in_admin_bar'		=> true,     
    'menu_position' 		=> 31, // should be 21-24 to appear between Pages and Comments in admin menu
    'menu_icon' 			=> '', // keep this line because it adds the .menu-icon-staff class to the <li> HTML    
    'capability_type' 		=> 'page',
    'hierarchical' 			=> true,
    'supports' 			=> array( 'title','thumbnail','editor','page-attributes' ),
    'taxonomies' 			=> array( 'staffrole' ),
    'has_archive' 			=> 'staff', // must be explicitly set in order for rewrite rules to work
    'rewrite' 				=> array( 'slug' => 'staff', 'with_front' => 'false' ),    
    'query_var' 			=> true,
    'can_export'			=> true
  );  
  register_post_type('staff',$args);  
}

// add filter to insure the text Staff Person, or staff person, is displayed when user updates a staff person 
add_filter('post_updated_messages', 'endvr_updated_messages_staff');
function endvr_updated_messages_staff( $messages ) {
global $post, $post_ID;
  $messages['staff'] = array(
    0 	=> '', // Unused. Messages start at index 1.
    1 	=> sprintf( __('Staff Person updated. <a href="%s">View staff person</a>'), esc_url( get_permalink($post_ID) ) ),
    2 	=> __('Custom field updated.'),
    3 	=> __('Custom field deleted.'),
    4 	=> __('Staff Person updated.'),
    		/* translators: %s: date and time of the revision */
    5 	=> isset($_GET['revision']) ? sprintf( __('Staff Person restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 	=> sprintf( __('Staff Person published. <a href="%s">View staff person</a>'), esc_url( get_permalink($post_ID) ) ),
    7 	=> __('Staff Person saved.'),
    8 	=> sprintf( __('Staff Person submitted. <a target="_blank" href="%s">Preview staff person</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 	=> sprintf( __('Staff Person scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview staff person</a>'),
      	// translators: Publish box date format, see http://php.net/date
      	date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 	=> sprintf( __('Staff Person draft updated. <a target="_blank" href="%s">Preview staff person</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
}
     
// assign custom text instructions to replace "Enter title here"
function endvr_replace_title_label_staff( $title ) {
     $screen = get_current_screen();
     if  ( 'staff' == $screen->post_type ) {
          $title = 'Enter Staff Member Name Here...';
          return $title;
     }
}
add_filter( 'enter_title_here', 'endvr_replace_title_label_staff' ); 

// assign a custom label to the Sub Title (not that relevant as long as the remove meta box function is active below this)
// @source: http://codex.wordpress.org/Function_Reference/remove_filter
function endvr_subtitle_label_staff() {
	echo 'Input a Subtitle for this Staff Member (Optional)';
}
function endvr_replace_subtitle_label_staff() {
     $screen = get_current_screen();
     if  ( 'staff' == $screen->post_type ) {
     	remove_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_default' );
     	add_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_staff' );
     }  
}
add_action( 'admin_head', 'endvr_replace_subtitle_label_staff' );

// this function removes the Sub Title metabox from this post type
// @source: http://www.webworkgarage.com/2012/07/how-to-remove-meta-boxes-from-custom-post-types-on-wordpress/
function endvr_remove_subtitle_meta_box_staff () {
	remove_meta_box( 'endvr_feature_subtitle', 'staff', 'normal' );
}
add_filter ( 'add_meta_boxes', 'endvr_remove_subtitle_meta_box_staff', 999 ); // note the 999 priority is crucial	

// assign a custom label to the Post Editor + place it in a sortable Meta Box
// @source: http://wordpress.org/support/topic/move-custom-meta-box-above-editor?replies=17
// @source: http://software.troydesign.it/php/wordpress/move-wp-visual-editor.html
add_action( 'add_meta_boxes', 'endvr_add_meta_box_editor_staff', 0 );
function endvr_add_meta_box_editor_staff() {
     $screen = get_current_screen();
     if  ( 'staff' == $screen->post_type ) {
		global $_wp_post_type_features;
		foreach ($_wp_post_type_features as $type => &$features) {
			if (isset($features['editor']) && $features['editor']) {
				unset($features['editor']);
				add_meta_box(
					'endvr_editor_description_staff',
					__('Staff Member Bio'),
					'endvr_meta_box_editor_staff',
					$type, 'normal', 'core'
				);
			}
		}
	}
	add_action( 'admin_head', 'endvr_admin_head_staff' ); //white background
}
function endvr_admin_head_staff() {
	?>
	<style type="text/css">
		.wp-editor-container{background-color:#fff;}
	</style>
	<?php
}
function endvr_meta_box_editor_staff( $post ) {
	echo '<div class="wp-editor-wrap">';
	wp_editor($post->post_content, 'content', array('dfw' => true, 'tabindex' => 1) );
	echo '</div>';
}

// redefine the way the content type's admin panel index listing is displayed
// @source: http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column  
// @source: http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/   
add_filter( 'manage_edit-staff_columns', 'endvr_set_custom_edit_columns_staff' );
add_action( 'manage_staff_posts_custom_column',  'endvr_custom_column_staff', 10, 2 );
 
function endvr_set_custom_edit_columns_staff($columns){
	unset($columns['date']);
	return $columns 
		+ array(
			'_endvr_staff_role' 		=> __('Position'),
			'_endvr_staff_phone' 		=> __('Phone'),
			'_endvr_staff_email'		=> __('Email'),
			'_endvr_staff_photo_thumb' 	=> __('Thumbnail'),
			'_endvr_staff_photo_full' 	=> __('Full Photo'),		
		);
}		

function endvr_custom_column_staff($column, $post_id){
    switch ( $column ) {
      case '_endvr_staff_role':
      	echo get_post_meta( $post_id , '_endvr_staff_role' , true );
        break;
      case '_endvr_staff_phone':
        	echo get_post_meta( $post_id , '_endvr_staff_phone' , true ); 
        break;
      case '_endvr_staff_email':
        	echo get_post_meta( $post_id , '_endvr_staff_email' , true ); 
        break; 
      case '_endvr_staff_photo_thumb': ?>
        	<img src="<?php echo get_post_meta( $post_id , '_endvr_staff_photo_thumb' , true ); ?>" width="133"> 
        <?php
        break;  
      case '_endvr_staff_photo_full': ?>
        	<img src="<?php echo get_post_meta( $post_id , '_endvr_staff_photo_full' , true ); ?>"width="100"> 
        <?php
        break;                     
    }
}         

// flush rewrite rules upon activation
// @source: http://codex.wordpress.org/Function_Reference/register_post_type#Flushing_Rewrite_on_Activation
function endvr_rewrite_flush_staff() {
	endvr_init_staff();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'endvr_rewrite_flush_staff' );     