<?php

// define the custom content type
add_action('init', 'staff_init');
function staff_init() 
{
  $labels = array(
    'name' => _x('Staff', 'post type general name'),
    'singular_name' => _x('Staff Person', 'post type singular name'),
    'add_new' => _x('Add New', 'staff person'),
    'add_new_item' => __('Add New Staff Person'),
    'edit_item' => __('Edit Staff Person'),
    'new_item' => __('New Staff Person'),
    'view_item' => __('View Staff Person'),
    'search_items' => __('Search Staff'),
    'not_found' =>  __('No Staff found'),
    'not_found_in_trash' => __('No Staff found in Trash'), 
    'parent_item_colon' => __('Parent Directory')
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true,
    'menu_position' => 22,
    'menu_icon' => plugins_url('/endvr-fw/modules/post-types/icons/staff.png'),    
    'query_var' => true,
    'rewrite' => array('slug' => 'staff', 'with_front' => FALSE),
    'capability_type' => 'post',
    'hierarchical' => true,
    'supports' => array('title','editor','page-attributes'),
    'has_archive' => true
  ); 
  register_post_type('staff',$args);
  flush_rewrite_rules();  
}

// add filter to insure the text Staff Person, or staff person, is displayed when user updates a staff person 
add_filter('post_updated_messages', 'staff_updated_messages');
function staff_updated_messages( $messages ) {
global $post, $post_ID;

  $messages['staff'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Staff Person updated. <a href="%s">View staff person</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Staff Person updated.'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Staff Person restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Staff Person published. <a href="%s">View staff person</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Staff Person saved.'),
    8 => sprintf( __('Staff Person submitted. <a target="_blank" href="%s">Preview staff person</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Staff Person scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview staff person</a>'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Staff Person draft updated. <a target="_blank" href="%s">Preview staff person</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
}

// assign a custom icon to the content type
add_action('admin_head', 'replace_staff_icon');
function replace_staff_icon() {
        global $post_type;
	?>
	<style>
	<?php if (($_GET['post_type'] == 'staff') || ($post_type == 'staff')) : ?>
	#icon-edit { background:transparent url('<?php echo plugins_url('/endvr-fw/modules/post-types/icons/staff_32.png');?>') no-repeat; }		
	<?php endif; ?>
        </style>
        <?php }
        
        
// redefine the way the content type's control panel index listing is displayed     
add_action("manage_posts_custom_column",  "staff_custom_columns");
add_filter("manage_edit-staff_columns", "staff_edit_columns");
 
function staff_edit_columns($columns){
  $columns = array(
    "cb" => "<input type=\"checkbox\" />",
    "title" => "Staff Person",
    "description" => "Description",
    "role" => "Role or Position",
    "phone" => "Phone",
    "email" => "Email",
    "photo" => "Photo",
  );
 
  return $columns;
}
function staff_custom_columns($column){
  global $post;
  global $mb_staff;
  $mb_staff->the_meta;
 
  switch ($column) {
    case "description":
      echo the_excerpt();
      break;
    case "role": 
    		$mb_staff->the_value('staff_role');
    		break;
    case "phone" :
    		$mb_staff->the_value('staff_phone');
		break;
    case "email" :
    		$mb_staff->the_value('staff_email');
		break;
    case "photo" : 
    		$mb_staff->the_value('staff_photo');
		break;     
  }
}        

?>