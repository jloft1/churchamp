<?php

// define the custom content type
add_action('init', 'staff_init');
function staff_init() 
{
  $labels = array(
    'name' => _x('Staff', 'post type general name'),
    'singular_name' => _x('Staff Person', 'post type singular name'),
    'add_new' => _x('Add New', 'staff person'),
    'add_new_item' => __('Add New Staff Person'),
    'edit_item' => __('Edit Staff Person'),
    'new_item' => __('New Staff Person'),
    'view_item' => __('View Staff Person'),
    'search_items' => __('Search Staff'),
    'not_found' =>  __('No Staff found'),
    'not_found_in_trash' => __('No Staff found in Trash'), 
    'parent_item_colon' => __('Parent Directory')
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true,
    'menu_position' => 22,
    'menu_icon' => plugins_url('/endvr-fw/modules/post-types/icons/staff_16.png'),    
    'query_var' => true,
    'rewrite' => array('slug' => 'staff', 'with_front' => FALSE),
    'capability_type' => 'page',
    'hierarchical' => true,
    'supports' => array('title','editor','page-attributes'),
    'has_archive' => true
  ); 
  register_post_type('staff',$args);
  flush_rewrite_rules();  
}

// add filter to insure the text Staff Person, or staff person, is displayed when user updates a staff person 
add_filter('post_updated_messages', 'staff_updated_messages');
function staff_updated_messages( $messages ) {
global $post, $post_ID;

  $messages['staff'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Staff Person updated. <a href="%s">View staff person</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Staff Person updated.'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Staff Person restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Staff Person published. <a href="%s">View staff person</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Staff Person saved.'),
    8 => sprintf( __('Staff Person submitted. <a target="_blank" href="%s">Preview staff person</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Staff Person scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview staff person</a>'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Staff Person draft updated. <a target="_blank" href="%s">Preview staff person</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
}

// assign a custom icon to the content type
add_action('admin_head', 'replace_staff_icon');
function replace_staff_icon() {
     global $post_type;
	?>
	<style>
		<?php if (($_GET['post_type'] == 'staff') || ($post_type == 'staff')) : ?>
		#icon-edit { background:transparent url('<?php echo plugins_url('/endvr-fw/modules/post-types/icons/staff_32.png');?>') no-repeat; }		
		<?php endif; ?>
     </style>
     <?php } 
      
// redefine the way the content type's control panel index listing is displayed
// http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column  
// http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/   

add_filter("manage_edit-staff_columns", "staff_columns_edit");
 
function staff_columns_edit($columns_edit){
  $columns_edit = array(	
    "cb" => "<input type=\"checkbox\" />",
    "title" => "Staff Person",
    "role" => "Role or Position",
    "phone" => "Phone",
    "email" => "Email",
    "id" => __('ID'), 
  );
 
  return $columns_edit;
}

add_action("manage_staff_pages_custom_column",  "staff_columns_display", 10, 2);
function staff_columns_display($columns_display, $post_id){

  global $posts;


  switch ($columns_display) {

    case "role": 
    		$role = get_post_meta(get_the_ID(), $mb_staff_details->get_the_id(), TRUE);
    		echo $role['staff_role'];

    		break;
    case "phone" :
		if(isset($mb_staff_details)) $mb_staff_details->the_value('staff_role');
		break;
    case "email" :
echo 'hello';
		break;   
    case "id":
    		echo $post_id;
    		break;		
  }
}        

?>