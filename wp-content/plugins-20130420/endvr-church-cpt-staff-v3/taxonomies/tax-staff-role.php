<?php
//hook into the init action and call endvr_create_taxonomy_staffrole when it fires
add_action( 'init', 'endvr_create_taxonomy_staffrole', 0 );

//create staffrole taxomony
function endvr_create_taxonomy_staffrole() 
{

	$labels = array(
		'name' 				=> _x( 'Staff Role', 'taxonomy general name' ),
		'singular_name' 		=> _x( 'Staff Role', 'taxonomy singular name' ),
		'search_items' 		=> __( 'Search for Staff Role' ),
		'all_items' 			=> __( 'All Staff Role' ),
		'parent_item' 			=> __( 'Parent Staff Role' ),
		'parent_item_colon' 	=> __( 'Parent Staff Role:' ),
		'edit_item' 			=> __( 'Edit Staff Role' ), 
		'update_item' 			=> __( 'Update Staff Role' ),
		'add_new_item' 		=> __( 'Add New Staff Role' ),
		'new_item_name' 		=> __( 'New Staff Role Name' ),
		'menu_name'			=> __( 'Staff Role' )
	);
	 	
	$args = array(
		'hierarchical' 		=> true, // if set to true, the taxonomy will function like categories, and false = functionality like tags
		'labels' 				=> $labels,
		'show_ui' 			=> true,
		'show_admin_column' 	=> true, // this will add a staffrole column to the index list for the staff post type in the admin panel
		'query_var' 			=> 'staffrole',
		'rewrite' 			=> array( 'slug' => 'staff/role' )	
	);
	
	register_taxonomy( 'staffrole', array( 'staff' ), $args );
	//flush_rewrite_rules();
}