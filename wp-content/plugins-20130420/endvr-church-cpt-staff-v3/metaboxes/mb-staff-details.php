<?php

/**
 * Register field groups
 * The register_field_group function accepts 1 array which holds the relevant data to register a field group
 * You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 * This code must run every time the functions.php file is read
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => '50e92735c62d8',
		'title' => 'Staff Details',
		'fields' => 
		array (
			0 => 
			array (
				'key' => '_endvr_staff_role',
				'label' => 'Position/Role',
				'name' => '_endvr_staff_role',
				'type' => 'text',
				'order_no' => 0,
				'instructions' => 'Primary responsibility with the church (i.e. Senior Pastor).',
				'required' => 0,
				'conditional_logic' => 
				array (
					'status' => 0,
					'rules' => 
					array (
						0 => 
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			1 => 
			array (
				'key' => '_endvr_staff_phone',
				'label' => 'Phone',
				'name' => '_endvr_staff_phone',
				'type' => 'text',
				'order_no' => 1,
				'instructions' => 'Primary phone number (format => XXX-XXX-XXXX).',
				'required' => 0,
				'conditional_logic' => 
				array (
					'status' => 0,
					'rules' => 
					array (
						0 => 
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
			2 => 
			array (
				'key' => '_endvr_staff_email',
				'label' => 'Email',
				'name' => '_endvr_staff_email',
				'type' => 'text',
				'order_no' => 2,
				'instructions' => 'Primary email address (format => name@churchdomain.org).',
				'required' => 0,
				'conditional_logic' => 
				array (
					'status' => 0,
					'rules' => 
					array (
						0 => 
						array (
							'field' => 'null',
							'operator' => '==',
							'value' => '',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'formatting' => 'none',
			),
		),
		'location' => 
		array (
			'rules' => 
			array (
				0 => 
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'staff',
					'order_no' => 1,
				),
			),
			'allorany' => 'all',
		),
		'options' => 
		array (
			'position' => 'advanced',
			'layout' => 'default',
			'hide_on_screen' => 
			array (
				1 => 'custom_fields',
				2 => 'discussion',
				3 => 'comments',
				4 => 'author',
				5 => 'format',
				6 => 'featured_image',
				7 => 'categories',
				8 => 'tags',
				9 => 'send-trackbacks',			
			),
		),
		'menu_order' => 1,
	));
}