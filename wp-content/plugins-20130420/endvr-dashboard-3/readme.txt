=== Endeavr Dashboard ===
Contributors: jloft / rocketfarmer**
Donate link: http://endeavr/wordpress/plugins/endeavr-dashboard/
Tags: admin,dashboard
Requires at least: 3.4
Tested up to: 3.5.1
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

**This is a fork of Rocket Farmer's rocket dashboard plugin ( http://storefrontthemes.com/plugins/rocket-dashboard/ ).

Provides a more influential WordPress dashboard experience through the use of color.

== Description ==

A fresh take on the WordPress Admin Dashboard. Endeavr Dashboard delivers a more influential working environment for WordPress through the use of color. Black on white is reserved for the main content window. The left nav is dark and all the icons have been replaced with scalable vector art through a custom icon font. The WP-Admin bar has also been given a facelift, as it's now bigger, brighter and much more fun. Just upload the plugin and activate. No additional settings required!

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the `endvr-dashboard` folder to the `/wp-content/plugins/` directory or just use the built in WordPress plugin upload tool.
2. Activate the plugin through the 'Plugins' menu in WordPress
3. That's it! Have fun.

== Changelog ==

= 1.0 =
* First release