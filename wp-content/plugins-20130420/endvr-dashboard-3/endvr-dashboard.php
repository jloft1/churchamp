<?php
/*
Plugin Name: Endeavr Dashboard - MP6 Edition (version 3 beta)
Plugin URI: http://endeavr/wordpress/plugins/endeavr-dashboard/
Description: A theme for the WP-Admin dashboard interface. It includes greater contrast and the use of an icon font for iconography. It relies on the MP6 plugin.
Author: Jason Loftis (jLOFT) of Endeavr
Author URI: http://endeavr.com
Version: 3.0
License: GPL2 or later
*/

define( 'ENDVR_DASHBOARD_URL', plugin_dir_url(__FILE__) );

function my_admin_head() {
        echo '<link rel="stylesheet" type="text/css" href="' .ENDVR_DASHBOARD_URL . 'endvr-dashboard.css'.'">';
}

add_action('admin_head', 'my_admin_head');

// Registers admin menu separators
add_action('admin_menu','admin_menu_separator');


// Create Admin Menu Separator
// @source: http://www.xldstudios.com/adding-a-separator-to-the-wordpress-admin-menu/
function add_admin_menu_separator($position) {
	global $menu;
	$index = 0;
	foreach($menu as $offset => $section) {
		if (substr($section[2],0,9)=='separator')
		    $index++;
		if ($offset>=$position) {
			$menu[$position] = array('','read',"separator{$index}",'','wp-menu-separator');
			break;
	    }
	}
	ksort( $menu );
}

// Adds Admin Menu Separators
function admin_menu_separator() {
	// Adds custom separator after comments
	add_admin_menu_separator(30);
}

// Remove the WordPress Admin Bar for all users
// @source: http://www.wpbeginner.com/wp-tutorials/how-to-disable-wordpress-admin-bar-for-all-users-except-administrators/
// show_admin_bar(false);

// Add Dashboard Widgets
include_once('widgets/endvr-dashboard-widgets.php');