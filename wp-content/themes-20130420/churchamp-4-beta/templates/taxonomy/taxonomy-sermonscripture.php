<?php
$scripture = $wp_query->queried_object;
$scripture_link = '<a href="/sermons/scripture/' . $scripture->slug . '" title="' . sprintf(__('View the Sermons in this Book of the Bible: %s', 'my_localization_domain'), $scripture->name) . '">' . $scripture->name . '</a>';
?>
		
<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title"><?php echo $scripture_link; ?></span><br>
					<span class="feature-subtitle">Sermon Scripture Archive</span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-link span4 visible-desktop">
				<span class="feature-subtitle">Find a Sermon&hellip;</span><br>
				<?php get_template_part('templates/menus/menu-sermons'); ?>
			</div><!-- /.feature-link			
		<?php tha_feature_bottom(); ?>	
		</div><!-- /.row -->
	</div><!-- /.container -->		
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span12'); ?> role="main">
	<div class="main-inner">
	
		<h3>Sermons in the Book of <?php echo $scripture->name; ?></h3><br>
			
			<?php
			$scripture_g = $scripture->slug;
			$ws_grid_columns_ss = 3;
			$ws_span_size_ss = ws_grid_class( $ws_grid_columns_ss );
			$ss_g_query_string = array(
			'sermonscripture' => $scripture_g,
			'posts_per_archive_page' => 100, // offset will not work unless this is a postive integer
			'orderby' => 'date', 
			'order' => 'DESC'
			);
			$ss_grid = new WP_Query( $ss_g_query_string );
			$ws_item_counter = 1;
			if ($ss_grid->have_posts()) {
				while ($ss_grid->have_posts()) {
					$ss_grid->the_post(); $do_not_duplicate = $post->ID;
					if( $ws_item_counter == 1 ) ws_open_row();
					get_template_part( 'templates/grids/grid-sermons-scripture' );
					if( $ws_item_counter % $ws_grid_columns_ss == 0 ) ws_close_row();
					if( $ws_item_counter % $ws_grid_columns_ss == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
					$ws_item_counter++;
				}
				if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
			} else {
				echo '<p>'.$scripture->name.' did not preach any sermons in this series.</p>';
			}
			?>

	</div><!-- /.main-inner -->		
</div><!-- /.main -->

<?php get_template_part('templates/structure/content-bottom-after'); ?>