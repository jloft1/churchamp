<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title"><?php echo ws_title(); ?></span><br>
					<span class="feature-subtitle"><?php the_field('_endvr_missionary_org'); ?><br class="visible-phone"> &#40;<?php the_field('_endvr_missionary_location'); ?>&#41;</span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-menu span4 visible-desktop">
				<span class="feature-subtitle">Find a Missionary by&hellip;</span><br>
				<?php get_template_part('templates/menus/menu-missionaries'); ?>
			</div><!-- /.feature-menu -->
		<?php tha_feature_bottom(); ?>	
		</div><!-- /.row -->
	</div><!-- /.container -->		
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span8'); ?> role="main">
	<div class="main-inner">
	
		<div class="row hidden-desktop">
			
			<div class="span12 feature-menu">
				<span class="feature-subtitle">Find a Missionary by&hellip;</span><br class="visible-phone"> <?php get_template_part('templates/menus/menu-missionaries'); ?>
			</div><!-- /.feature-menu -->
		
		</div><!-- /.row -->
		
		<hr class="hidden-desktop">	
		
<?php tha_entry_before(); ?>		
	
		<?php tha_entry_top(); ?>
		<div class="entry-content">
			<div class="missionary-profile">
				<h2><i class="icon-bookmark"></i>&nbsp; Missionary Profile</h2>
				<hr>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php 
				if ($post->post_content=='') : 
					echo 'Missionary information will be posted soon&#133;<br>';
				else :
					the_content();	 
				endif; 
				?>
				<?php endwhile; endif; ?>
			</div><!-- /.missionary-profile -->
		</div>
		<footer>
			 <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'ws'), 'after' => '</p></nav>')); ?>
		</footer>
		<?php tha_entry_bottom(); ?>

	</div><!-- /.main-inner -->	
</div><!-- /.main -->

<?php rewind_posts(); ?>

<?php while (have_posts()) : the_post(); ?>

<?php tha_sidebars_before(); ?>
<aside id="sidebar" <?php ws_sidebar_class('span4'); ?> role="complementary">
	<div class="sidebar-inner">
		<?php tha_sidebar_top(); ?>

			<section id="endvr-widget-missionary-photo" class="endvr-widget-missionary-photo widget well well-smaill">
				<div class="widget-inner">
					<img src="<?php the_field('_endvr_missionary_photo_full'); ?>" alt="<?php the_title(); ?> <?php the_field('_endvr_missionary_role'); ?>" width="340" />
				</div>
			</section>
			<section id="endvr-widget-missionary-details-org" class="endvr-widget-missionary-details-org widget">
				<div class="widget-inner">
					<h3 class="widget-title">Mission Information</h3>
					<?php if(get_field('_endvr_missionary_role')) { ?>
					<div class="endvr_mission">
		                    <span class="prefix">ROLE:</span> <?php the_field('_endvr_missionary_role'); ?>
					</div>
					<?php } ?>	
					<?php if(get_field('_endvr_missionary_location')) { ?>
					<div class="endvr_mission">
		                    <span class="prefix">LOCATION:</span> <?php the_field('_endvr_missionary_location'); ?>
					</div>
					<?php } ?>
					<?php $tax_missioncountry = wp_get_post_terms( $post->ID, 'missioncountry' ); foreach ($tax_missioncountry as $country) {
						$country_name_link = '<a href="/missions/missionaries/country/' . $country->slug . '" title="' . sprintf(__('View Missionary Directory by Country: %s', 'my_localization_domain'), $country->name) . '">' . $country->name . '</a>';
					} ?>
					<?php if ( $tax_missioncountry ) { ?>
						<span class="prefix">COUNTRY:</span> <?php echo $country_name_link; ?>	
					<?php } ?>
						<hr>			
					<?php $tax_missionagency = wp_get_post_terms( $post->ID, 'missionagency' ); foreach ($tax_missionagency as $agency) {
		                    $agency_id = $agency->term_id;	
		                    $agency_meta = get_option("missionagency_$agency_id");
		      			$agency_name_link = '<a href="/missions/missionaries/agency/' . $agency->slug . '" title="' . sprintf(__('View Missionary Directory by Agency: %s', 'my_localization_domain'), $agency->name) . '">' . $agency->name . '</a>';
		      			$agency_url_link = '<a href="' . esc_attr( $agency_meta['missionagency_website'] ).'" title="' . sprintf(__('Visit Mission Agency Website: %s', 'my_localization_domain'), $agency->name) . '" target="_blank">' . esc_attr( $agency_meta['missionagency_website'] ) . '</a>';	
					} ?>              
		               <?php if ( $tax_missionagency ) { ?>     
					<div class="endvr_mission">
		                    <span class="prefix">ORG:</span> <?php echo $agency_name_link; ?>
					</div>
					<div class="endvr_mission">
		                    <span class="prefix">URL:</span> <?php echo $agency_url_link; ?>
					</div>
					<?php } ?>
						<hr>
					<?php if(get_field('_endvr_missionary_project')) { ?>
					<div class="endvr_mission">
		                    <span class="prefix">PROJ:</span> <?php the_field('_endvr_missionary_project'); ?>
					</div>
					<?php } ?>
					<?php if(get_field('_endvr_missionary_website_project')) { ?>
					<div class="endvr_mission">
		                    <span class="prefix">URL:</span> 
		                    <a href="<?php the_field('_endvr_missionary_website_project'); ?>" title="<?php the_field('_endvr_missionary_project'); ?>" target="_blank">
		                    <?php the_field('_endvr_missionary_website_project'); ?></a>
					</div>
					<?php } ?>                           
				</div>				
			</section>
			<section id="endvr-widget-missionary-details-contact" class="endvr-widget-missionary-details-contact widget">
				<div class="widget-inner">
					<h3 class="widget-title">Contact Information</h3>
					<?php if(get_field('_endvr_missionary_phone_field')) { ?>
					<div class="endvr_contact connect_phone">
						<i class="icon-phone">&nbsp;</i>
		                    <span class="prefix">PH (Field):</span> <?php the_field('_endvr_missionary_phone_field'); ?>
					</div>
					<?php } ?>
					<?php if(get_field('_endvr_missionary_phone_home')) { ?>
					<div class="endvr_contact connect_phone">
						<i class="icon-phone">&nbsp;</i>
		                    <span class="prefix">PH (Home):</span> <?php the_field('_endvr_missionary_phone_home'); ?>
					</div>			
					<?php } ?>
					<?php if(get_field('_endvr_missionary_address_field')) { ?>
					<div class="endvr_contact connect_postal">
						<i class="icon-globe">&nbsp;</i>
		                    <span class="prefix">ADDRESS (Field):</span><br><span class="endvr_contact_connect_postal_address"><?php the_field('_endvr_missionary_address_field'); ?></span>
					</div>			
					<?php } ?>  
					<?php if(get_field('_endvr_missionary_address_home')) { ?>
					<div class="endvr_contact connect_postal">
						<i class="icon-map-marker">&nbsp;</i>
		                    <span class="prefix">ADDRESS (Home):</span><br><span class="endvr_contact_connect_postal_address"><?php the_field('_endvr_missionary_address_home'); ?></span>
					</div>			
					<?php } ?>
					<?php if(get_field('_endvr_missionary_email')) { ?>
					<div class="endvr_contact connect_email">
		                    <i class="icon-envelope-alt">&nbsp;</i>
		                    <a href="mailto:<?php $emailaddy = get_field('_endvr_missionary_email'); echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
		               </div>
		               <?php } ?>
					<?php if(get_field('_endvr_missionary_email_alt')) { ?>
					<div class="endvr_contact connect_email">
		                    <i class="icon-envelope">&nbsp;</i>
		                    <a href="mailto:<?php $emailaddy = get_field('_endvr_missionary_email_alt'); echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
		               </div>
		               <?php } ?>             		                           
				</div>
			</section>
	
	     <?php tha_sidebar_bottom(); ?>
	</div><!-- /.sidebar-inner -->
</aside><!-- /.sidebar -->
<?php tha_sidebars_after(); ?>

<?php endwhile; ?>	

<?php get_template_part('templates/structure/content-bottom-after'); ?>

</article><!-- /article -->
<?php tha_entry_after(); ?>