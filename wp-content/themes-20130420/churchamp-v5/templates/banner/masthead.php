<div id="masthead" <?php ws_masthead_class(); ?>>
	<div class="container">
	<div class="row">
	<div class="span12">
		<div class="row">
			<div id="brand" class="brand span6">
			      <a href="<?php echo home_url(); ?>/">
			        	<?php 
			        	$ws_brand = of_get_option('ws_brand');
			        	$ws_brand_font_text = of_get_option('ws_brand_font_text');
			        	$ws_brand_mark = of_get_option('ws_brand_mark');
			        	$ws_brand_logo = of_get_option('ws_brand_logo'); 
			        	?>    
			        		<?php if ( $ws_brand == 'one' ) : ?>
			        			<span class="brand_font_text"><?php echo $ws_brand_font_text; ?></span>
			        		<?php endif; ?>
			        		<?php if ( $ws_brand == 'two' ) : ?>
			        			<?php if ( $ws_brand_mark != '' ) : ?>
			        				<img class="brand_mark" src="<?php echo $ws_brand_mark; ?>" alt="<?php echo $ws_brand_font_text; ?>" /> 
			        			<?php endif; ?>
			        			<span class="brand_font_text"><?php echo $ws_brand_font_text; ?></span>
			        		<?php endif; ?>
			        		<?php if ( $ws_brand == 'three' ) : ?>
			        			<?php if ( $ws_brand_logo != '' ) : ?>
			        				<img class="brand_logo" src="<?php echo $ws_brand_logo; ?>" alt="<?php echo $ws_brand_font_text; ?>" /> 
			        			<?php endif; ?>
			        		<?php endif; ?>	
			      </a>
			</div><!-- /.brand -->
			<div id="leaderboard" class="leaderboard span6">
				<div class="nav-social visible-desktop">
	                    <a rel="tooltip" class="vimeo" href="https:/vimeo.com/fbcprescott" title="Vimeo Video Channel" target="_blank">
	                    	<span class="ss-icon ss-social">Vimeo</span>
	                    </a>
	                    <a rel="tooltip" class="facebook" href="https://www.facebook.com/pages/FBC-Prescott/190722620946354?ref=hl" title="Facebook Page" target="_blank">
	                    	<span class="ss-icon ss-social">Facebook</span>
	                    </a>
	                    <a rel="tooltip" class="contact" href="/about/contact/" title="Contact Us">
	                    	<span class="ss-icon">Email</span>
	                    </a>
	                    <a rel="tooltip" class="staff" href="/staff/" title="Staff Directory">
	                    	<span class="ss-icon">Avatars</span>
	                    </a>	                    
	                    <a rel="tooltip" class="location" href="/about/directions-map/" title="Get Map/Directions">
	                    	<span class="ss-icon">Location</span>
	                    </a>
	                    <a rel="tooltip" class="sermons" href="/sermons/" title="Listen to Sermons">
	                    	<span class="ss-icon">Microphone</span>
	                    </a>
	                    <a rel="tooltip" class="missions" href="/missions/missionaries/" title="Missionary Directory">
	                    	<span class="ss-icon">Globe</span>
	                    </a>	                    	 	                    	                    
                    </div>
			</div><!-- /.leaderboard -->
		</div><!-- /.row -->
	</div><!-- /.span12 -->	
	</div><!-- /.row -->
	</div><!-- /.container -->	
</div><!-- /.masthead -->