<div class="endvr-home-worship">
<div class="container">
<div class="row endvr-home-worship-block">
<div class="span12">
	
		<h2 class="text-center heading-highlight">Join us on Sunday Mornings at one of our Worship Environments</h2>
					
						
			<?php
			$ws_grid_columns_sunevent = 4;
			$ws_span_size_sunevent = ws_grid_class( $ws_grid_columns_sunevent );
			$ss_g_query_string = array(
			'sundayevent' => 'worship-environment',
			'posts_per_archive_page' => 4, // offset will not work unless this is a postive integer
			'orderby' => 'menu_order', 
			'order' => 'ASC'
			);
			$ss_grid = new WP_Query( $ss_g_query_string );
			$ws_item_counter = 1;
			if ($ss_grid->have_posts()) {
				while ($ss_grid->have_posts()) {
					$ss_grid->the_post(); $do_not_duplicate = $post->ID;
					if( $ws_item_counter == 1 ) ws_open_row();
					get_template_part( 'templates/grids/grid-sunday-event-home' );
					if( $ws_item_counter % $ws_grid_columns_sunevent == 0 ) ws_close_row();
					if( $ws_item_counter % $ws_grid_columns_sunevent == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
					$ws_item_counter++;
				}
				if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
			} else {
				echo '<p>There are no Sunday Events in '.$sunevent->name.' sunevent.</p>';
			}
			?>	
			
</div>
</div>
</div>
</div>
								
			<?php
			$ss_l_query_string = array(
			'post_type' => 'sermons',
			'posts_per_archive_page' => 1,
			'orderby' => 'date', 
			'order' => 'DESC'
			);
			$ss_latest = new WP_Query( $ss_l_query_string );
			while ($ss_latest->have_posts()) : $ss_latest->the_post(); $do_not_duplicate = $post->ID; ?>
			
	

<div class="endvr-home-sermon">
<div class="container">
<div class="row endvr-home-sermon-block">
<div class="span12">		
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
				<h2 class="text-center heading-highlight">We put our Sermons online each week&hellip; Here's the latest:</h2>
			
					<div class="row">
					
						<div class="ss span4">
							<div class="media-frame">
							<?php $tax_sermon_series = wp_get_post_terms( $post->ID, 'sermonseries' ); foreach ($tax_sermon_series as $series) { 
								$series_id = $series->term_id;
								$series_img = get_field('_endvr_taxonomy_image_sermonseries','sermonseries_'.$series_id.''); ?>
								<img src="<?php echo $series_img; ?>">
							<?php } ?>
							</div>
						</div>
						
							<hr class="visible-phone">
							
						<div class="sd span4">	
						
							<div class="sdr header clearfix">
								<div class="sdi alignleft">
									<!--<i class="icon-bookmark"></i>&nbsp;-->
								</div>
								<div class="sdt alignleft">
									<h3>
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
									</h3>
									<?php if (function_exists('the_subtitle')) {
										if ( get_post_meta($post->ID, 'endvr_feature_subtitle', true) ) { ?>					
											<h4>
												<?php the_subtitle(); ?>	
											</h4>								
										<?php } else { }
									} ?>	
								</div>
							</div>
	
						
									
							
							<div class="sdr content clearfix">
							
								<div>
									<i class="icon-folder-close"></i>&nbsp;
									<span class="prefix">Series :</span>								
									<?php $tax_sermon_series = wp_get_post_terms( $post->ID, 'sermonseries' ); foreach ($tax_sermon_series as $series) {				
										$series_link = '<a href="/sermons/' . $series->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series->name) . '">' . $series->name . '</a>';
										echo $series_link;
									} ?>
								</div>
								<div>
									<i class="icon-user"></i>&nbsp;
									<span class="prefix">Speaker :</span>
									<?php $tax_sermon_speaker = wp_get_post_terms( $post->ID, 'sermonspeaker' ); foreach ($tax_sermon_speaker as $speaker) {				
										$speaker_link = '<a href="/staff/' . $speaker->slug . '" title="' . sprintf(__('View profile page for this sermon speaker: %s', 'my_localization_domain'), $speaker->name) . '">' . $speaker->name . '</a>';
										echo $speaker_link;
									}  ?>
								</div>
								<?php if (get_field('_endvr_sermon_ref')) { ?>
									<div class="visible-desktop">
										<i class="icon-book"></i>&nbsp;
										<span class="prefix">Scripture :</span>								
										<?php the_field('_endvr_sermon_ref'); ?>
									</div>
								<?php } ?>
								<div>
									<i class="icon-calendar"></i>&nbsp;
									<span class="prefix">Date :</span>
									<?php the_time('M d, Y'); ?>
								</div>												
							</div><!-- /.sermon-detail -->

							<div class="sdr footer clearfix">
								<a href="<?php the_permalink(); ?>" title="Get the MP3 &amp; Notes for the Sermon: <?php the_title(); ?>" class="btn btn-primary btn-large">
									Get the MP3 &amp; Notes
								</a>
							</div><!-- /.sdr.footer -->
						
						</div><!-- /.span4 -->		
						
							<hr class="visible-phone">					
						
						<div class="sv span4"> <!-- requires Responsive Video Shortcodes plugin @source: http://wordpress.org/extend/plugins/responsive-video-shortcodes/ -->
							<?php if (get_field('_endvr_sermon_video')) { ?>
								<div class="media video media-frame">
									<?php
									$endvr_sermon_video_src = get_field('_endvr_sermon_video');
									echo do_shortcode('[video align="left" aspect_ratio="4:3" width="100"]'.$endvr_sermon_video_src.'[/video]');
									?>
								</div><!-- /.media -->
							<?php } else {
								echo '<p>There is no video available for this sermon at this time.</p>';
							} ?>
						</div><!-- /.sermon-video -->
						
					</div><!-- /.row -->
					
				
			
			</article><!-- #post-<?php the_ID(); ?> -->
				
			<?php endwhile; ?>
			
</div>
</div>
</div>
</div>