<?php
/**
 * The template used for displaying posts in a grid.
 */
global $columns;
global $size;
global $counter;
global $location;
global $tax_sermon_series;
global $series;
global $series_link;
global $mb_sermons;
$mb_sermons->the_meta();
?>
<div class="grid-item column content-grid-endvr grid-sermons <?php echo $size; ?><?php if( $counter % $columns == 0 ) echo ' last'; ?>">
	<div class="article-wrap">
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>					
			<div class="entry-content">
				<h2 class="entry-title">
					<span class="grid_sermon_title"><?php the_title(); ?><br />&#40;<?php $mb_sermons->the_value('sermon_ref'); ?> &#41;</span>	
				</h2>
				<span class="content-grid-tagline grid_sermon_by">
					A Sermon by<br />
					<?php $tax_sermon_speaker = get_terms('sermon_speaker'); foreach ($tax_sermon_speaker as $speaker); echo $speaker->name; ?>
				</span>
				<span class="grid_sermon_date"><?php the_time('Y - M - d'); ?></span>
			</div><!-- .entry-content -->
		</article><!-- #post-<?php the_ID(); ?> -->
		</a><!-- end anchor -->
	</div><!-- .article-wrap (end) -->
</div><!-- .grid-item (end) -->