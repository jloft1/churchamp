<?php
/**
 * The template file for the Sermons Archive.
 * 
 * WARNING: This template file is a core part of the 
 * Theme Blvd WordPress Framework. This framework is 
 * designed around this file NEVER being altered. It 
 * is advised that any edits to the way this file 
 * displays its content be done with via hooks and filters.
 * 
 * @author		Jason Bobich
 * @copyright	Copyright (c) Jason Bobich
 * @link		http://jasonbobich.com
 * @link		http://themeblvd.com
 * @package 	Theme Blvd WordPress Framework
 */

// Header
get_header(); ?>

<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>

<?php
// Engage Post Type's Metaboxes
global $mb_sermons;
$mb_sermons->the_meta();
?>
	
	<!-- FEATURED (start) -->
		
		<div id="featured">
			<div class="featured-inner has_page_featured">
				<div class="featured-content">
					<div class="element element-headline featured-entry-title">
						<h1 class="entry-title"><?php echo apply_filters( 'the_title', $term->name ); ?> <!-- alternatively, one could use PHP printf( $term->name ); PHP --></h1>
						<p class="tagline"><?php echo apply_filters( 'the_title', $term->description ); ?></p>
					</div><!-- .element (end) -->	
					<aside id="featured-search" class="widget widget_search">
						<div class="widget-inner">
							<div class="themeblvd-search">
							    <form method="get" action="http://fbcp.jloft.com">
							        <fieldset>
							            <span class="input-wrap">
							            	<input type="text" class="search-input" name="s" onblur="if (this.value == '') {this.value = 'Search the site...';}" onfocus="if(this.value == 'Search the site...') {this.value = '';}" value="Search the site...">
							            </span>
							            <span class="submit-wrap">
							            	<input type="submit" class="submit" value="">
							            </span>
							        </fieldset>
							    </form>
							</div>
						</div>
					</aside> <!-- #featured-search (end) -->				
					<div class="clear"></div>
				</div><!-- .featured-content (end) -->
				<div class="secondary-bg"></div>
			</div><!-- .featured-inner (end) -->
		</div><!-- #featured (end) -->
		
	<!-- FEATURED (end) -->
	
	<!-- MAIN (start) -->
	
	<div id="main" class="full_width">
		<div class="main-inner">
			<div class="main-content">
				<div class="grid-protection">
					<div class="main-top"></div><!-- .main-top (end) -->	
	
	<div id="sidebar_layout">
		<div class="sidebar_layout-inner">
			<div class="grid-protection">
		
								
				<!-- CONTENT (start) -->
	
				<div id="content" role="main">
					<div class="inner">
						<?php themeblvd_content_top(); ?>
						<div class="primary-post-grid post_grid_paginated post_grid<?php echo themeblvd_get_classes( 'element_post_grid_paginated', true ); ?>">
							<div class="grid-protection">
							
								<?php
								print apply_filters( 'taxonomy-images-queried-term-image', '', array(
								'before' => '<div class="taxonomy_image">',
								'after' => '</div>',
								'image_size' => 'large',
								) ); ?>
								
								<hr />
								<h3>Available Sermons</h3>
								<p>Click on the sermon box that interests you to listen to the audio, watch the video, and/or view the sermon outline.</p>
								
								<?php	
									$ss = $term->slug;
									$columns = 4;
									$size = themeblvd_grid_class( $columns );
									$posts_per_page = -1;
									$query_string = array(
									'sermon_series' => $ss,
									'posts_per_page' => -1,
									'orderby' => 'date', 
									'order' => 'DESC'
									);
									query_posts( $query_string );
									$counter = 1;
									if ( have_posts() ) {
										while ( have_posts() ) {
											the_post();
											if( $counter == 1 ) themeblvd_open_row();
											get_template_part( 'content-grid-sermons' );
											if( $counter % $columns == 0 ) themeblvd_close_row();
											if( $counter % $columns == 0 && $posts_per_page != $counter ) themeblvd_open_row();
											$counter++;
										}
										if( ($counter-1) != $posts_per_page ) themeblvd_close_row();
									} else {
										echo '<p>'.themeblvd_get_local( 'archive_no_posts' ).'</p>';
									}
								?>
								
							</div><!-- .grid-protection (end) -->
							<?php themeblvd_pagination(); ?>
						</div><!-- .post_grid (end) -->
					</div><!-- .inner (end) -->
				</div><!-- #content (end) -->
					
				<!-- CONTENT (end) -->
				
							
			</div><!-- .grid-protection (end) -->
		</div><!-- .sidebar_layout-inner (end) -->
	</div><!-- .sidebar-layout-wrapper (end) -->
	
					<div class="main-bottom"></div><!-- .main-bottom (end) -->						
						<div class="clear"></div>
				</div><!-- .grid-protection (end) -->
			</div><!-- .main-content (end) -->
		</div><!-- .main-inner (end) -->
	</div><!-- #main (end) -->
	
	<!-- MAIN (end) -->

<?php	
// Footer
get_footer();
?>