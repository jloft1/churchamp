<?php
/**
 * The template used for displaying page content for single-missionaries.php
 */
// Engage Post Type's Metaboxes
global $mb_missionary_details;
$mb_missionary_details->the_meta();
global $mb_missionary_photos;
$mb_missionary_photos->the_meta();
?>
<div class="article-wrap">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry-content">
		
			<h2>MISSION SUMMARY</h2>
			
			<?php if ($mb_missionary_details->have_value('missionary_people_group')) { ?>
				<h4>People Group Served</h4>
				<p><?php $mb_missionary_details->the_value('missionary_people_group'); ?></p>
			<?php } ?>
			
			<?php if ($mb_missionary_details->have_value('missionary_ministry_focus')) { ?>
				<h4>Ministry Focus</h4>
				<p><?php $mb_missionary_details->the_value('missionary_ministry_focus'); ?></p>
			<?php } ?>	

			<?php if ($mb_missionary_details->have_value('missionary_challenges')) { ?>
				<h4>Unique Challenges</h4>
				<p><?php $mb_missionary_details->the_value('missionary_challenges'); ?></p>
			<?php } ?>

			<?php if ($mb_missionary_details->have_value('missionary_prayer_focus')) { ?>
				<h4>Prayer Need</h4>
				<p><?php $mb_missionary_details->the_value('missionary_prayer_focus'); ?></p>
			<?php } ?>

			<hr>
			
			<h2>MISSION DETAILS</h2>							
			
			<?php the_content(); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', TB_GETTEXT_DOMAIN ), 'after' => '</div>' ) ); ?>
			<?php edit_post_link( __( 'Edit', TB_GETTEXT_DOMAIN ), '<span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-content -->
	</article><!-- #post-<?php the_ID(); ?> -->
</div><!-- .article-wrap (end) -->