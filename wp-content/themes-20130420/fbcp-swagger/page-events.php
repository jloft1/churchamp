<?php
/**
Template Name: Event Calendar Template
 * 
 * WARNING: This template file is a core part of the 
 * Theme Blvd WordPress Framework. This framework is 
 * designed around this file NEVER being altered. It 
 * is advised that any edits to the way this file 
 * displays its content be done with via hooks and filters.
 * 
 * @author		Jason Bobich
 * @copyright	Copyright (c) Jason Bobich
 * @link		http://jasonbobich.com
 * @link		http://themeblvd.com
 * @package 	Theme Blvd WordPress Framework
 */

// Header
get_header(); 
	
	the_post(); ?>
	
	<!-- FEATURED (start) -->
		
		<div id="featured">
			<div class="featured-inner has_page_featured">
				<div class="featured-content">
					<div class="element element-headline featured-entry-title">
						<h1 class="entry-title">Event Calendar</h1>
						<p class="tagline">We have an active church life...</p>
					</div><!-- .element (end) -->	
					<aside id="featured-search" class="widget widget_search">
						<div class="widget-inner">
							<div class="themeblvd-search">
							    <form method="get" action="http://fbcp.jloft.com">
							        <fieldset>
							            <span class="input-wrap">
							            	<input type="text" class="search-input" name="s" onblur="if (this.value == '') {this.value = 'Search the site...';}" onfocus="if(this.value == 'Search the site...') {this.value = '';}" value="Search the site...">
							            </span>
							            <span class="submit-wrap">
							            	<input type="submit" class="submit" value="">
							            </span>
							        </fieldset>
							    </form>
							</div>
						</div>
					</aside> <!-- #featured-search (end) -->				
					<div class="clear"></div>
				</div><!-- .featured-content (end) -->
				<div class="secondary-bg"></div>
			</div><!-- .featured-inner (end) -->
		</div><!-- #featured (end) -->
		
	<!-- FEATURED (end) -->
	
	<?php
	// Start main area
	themeblvd_main_start();
	themeblvd_main_top();
	
	// Breadcrumbs
	themeblvd_breadcrumbs();
	
	// Before sidebar+content layout
	themeblvd_before_layout();
	?>
	
	<div id="sidebar_layout">
		<div class="sidebar_layout-inner">
			<div class="grid-protection">

				<?php themeblvd_fixed_sidebars( 'left' ); ?>
				
				<!-- CONTENT (start) -->
	
				<div id="content" role="main">
					<div class="inner">
						<?php themeblvd_content_top(); ?>			
						<?php get_template_part( 'content', themeblvd_get_part( 'page' ) ); ?>
						<?php themeblvd_page_footer(); ?>
						<?php if( themeblvd_supports( 'comments', 'pages' ) ) comments_template( '', true ); ?>
					</div><!-- .inner (end) -->
				</div><!-- #content (end) -->
					
				<!-- CONTENT (end) -->	
					
				<!-- CONTENT (end) -->	
				
				<?php themeblvd_fixed_sidebars( 'right' ); ?>
			
			</div><!-- .grid-protection (end) -->
		</div><!-- .sidebar_layout-inner (end) -->
	</div><!-- .sidebar-layout-wrapper (end) -->
	
	<?php	
	// End main area
	themeblvd_main_bottom();
	themeblvd_main_end();
	
// Footer
get_footer();