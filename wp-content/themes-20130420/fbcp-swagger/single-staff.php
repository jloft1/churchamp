<?php
/**
 * The template file for Staff Member pages.
 * 
 * WARNING: This template file is a core part of the 
 * Theme Blvd WordPress Framework. This framework is 
 * designed around this file NEVER being altered. It 
 * is advised that any edits to the way this file 
 * displays its content be done with via hooks and filters.
 * 
 * @author		Jason Bobich
 * @copyright	Copyright (c) Jason Bobich
 * @link		http://jasonbobich.com
 * @link		http://themeblvd.com
 * @package 	Theme Blvd WordPress Framework
 */

// Header
get_header(); ?>

<?php
// Engage Post Type's Metaboxes
global $mb_staff_details;
$mb_staff_details->the_meta();
global $mb_staff_photos;
$mb_staff_photos->the_meta();
?>
	
	<!-- FEATURED (start) -->
		
		<div id="featured">
			<div class="featured-inner has_page_featured">
				<div class="featured-content">
					<div class="element element-headline featured-entry-title">
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<p class="tagline"><?php $mb_staff_details->the_value('staff_role'); ?></p>
					</div><!-- .element (end) -->	
					<aside id="featured-search" class="widget widget_search">
						<div class="widget-inner">
							<div class="themeblvd-search">
							    <form method="get" action="http://fbcp.jloft.com">
							        <fieldset>
							            <span class="input-wrap">
							            	<input type="text" class="search-input" name="s" onblur="if (this.value == '') {this.value = 'Search the site...';}" onfocus="if(this.value == 'Search the site...') {this.value = '';}" value="Search the site...">
							            </span>
							            <span class="submit-wrap">
							            	<input type="submit" class="submit" value="">
							            </span>
							        </fieldset>
							    </form>
							</div>
						</div>
					</aside> <!-- #featured-search (end) -->				
					<div class="clear"></div>
					<a class="archive_link" href="<?php bloginfo('url');?>/staff/" title="Return to Staff Directory">Return to Staff Directory</a>
				</div><!-- .featured-content (end) -->
				<div class="secondary-bg"></div>
			</div><!-- .featured-inner (end) -->
		</div><!-- #featured (end) -->
		
	<!-- FEATURED (end) -->
	
	<?php
	// Start main area
	themeblvd_main_start();
	themeblvd_main_top();
	
	// Breadcrumbs
	themeblvd_breadcrumbs();
	
	// Before sidebar+content layout
	themeblvd_before_layout();
	?>
	
	<div id="sidebar_layout">
		<div class="sidebar_layout-inner">
			<div class="grid-protection">

				<?php themeblvd_fixed_sidebars( 'left' ); ?>
				
				<!-- CONTENT (start) -->
	
				<div id="content" role="main">
					<div class="inner">
						<?php themeblvd_content_top(); ?>	
						<?php get_template_part( 'content', themeblvd_get_part( 'page' ) ); ?>
						<?php themeblvd_page_footer(); ?>
						<?php if( themeblvd_supports( 'comments', 'pages' ) ) comments_template( '', true ); ?>
					</div><!-- .inner (end) -->
				</div><!-- #content (end) -->
					
				<!-- CONTENT (end) -->	
					
				<!-- CONTENT (end) -->	
				
<div class="fixed-sidebar right-sidebar">
<div class="fixed-sidebar-inner">
<div class="widget-area widget-area-fixed">
	<aside id="endvr-widget-staff-details" class="widget">
		<div class="widget-inner">
			<h3 class="widget-title">Contact Information</h3>
			<?php if ($mb_staff_details->have_value('staff_phone')) { ?>
			<div class="endvr_contact connect_phone">
				<i class="connect_icon">&nbsp;</i>
                    PH: &nbsp; <?php $mb_staff_details->the_value('staff_phone'); ?>
			</div>
			<?php } ?>
			<?php if ($mb_staff_details->have_value('staff_email')) { ?>
			<div class="endvr_contact connect_email">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="mailto:<?php $mb_staff_email = $mb_staff_details->the_meta(); $emailaddy = $mb_staff_email['staff_email']; echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
               </div>
               <?php } ?>
		</div>
	</aside>	
	<aside id="endvr-widget-staff-photo" class="widget">
		<div class="widget-inner">
			<img src="<?php $mb_staff_photos->the_value('staff_photo_full'); ?>" alt="<?php the_title(); ?> <?php $mb_staff_details->the_value('staff_role'); ?>" width="300" />
		</div>
	</aside>	
</div><!-- .widget_area (end) -->
</div><!-- .fixed-sidebar-inner (end) -->
</div><!-- .fixed-sidebar (end) -->		
			
			</div><!-- .grid-protection (end) -->
		</div><!-- .sidebar_layout-inner (end) -->
	</div><!-- .sidebar-layout-wrapper (end) -->
	
	<?php	
	// End main area
	themeblvd_main_bottom();
	themeblvd_main_end();
	
// Footer
get_footer();