<?php
/**
 * The template file for Sermon pages.
 * 
 * WARNING: This template file is a core part of the 
 * Theme Blvd WordPress Framework. This framework is 
 * designed around this file NEVER being altered. It 
 * is advised that any edits to the way this file 
 * displays its content be done with via hooks and filters.
 * 
 * @author		Jason Bobich
 * @copyright	Copyright (c) Jason Bobich
 * @link		http://jasonbobich.com
 * @link		http://themeblvd.com
 * @package 	Theme Blvd WordPress Framework
 */

// Header
get_header(); ?>

<?php
// Engage Post Type's Metaboxes
global $mb_sermons;
$mb_sermons->the_meta();

$tax_sermon_speaker = get_terms('sermon_speaker','number=1');
?>
	
	<!-- FEATURED (start) -->
		
		<div id="featured">
			<div class="featured-inner has_page_featured">
				<div class="featured-content">
					<div class="element element-headline featured-entry-title">
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<p class="tagline">
						<?php echo get_term_by('slug',$tax_sermon_series,$wp_query->query_vars['sermon_series']);
?>
						</p>
					</div><!-- .element (end) -->	
					<aside id="featured-search" class="widget widget_search">
						<div class="widget-inner">
							<div class="themeblvd-search">
							    <form method="get" action="http://fbcp.jloft.com">
							        <fieldset>
							            <span class="input-wrap">
							            	<input type="text" class="search-input" name="s" onblur="if (this.value == '') {this.value = 'Search the site...';}" onfocus="if(this.value == 'Search the site...') {this.value = '';}" value="Search the site...">
							            </span>
							            <span class="submit-wrap">
							            	<input type="submit" class="submit" value="">
							            </span>
							        </fieldset>
							    </form>
							</div>
						</div>
					</aside> <!-- #featured-search (end) -->				
					<div class="clear"></div>
				</div><!-- .featured-content (end) -->
				<div class="secondary-bg"></div>
			</div><!-- .featured-inner (end) -->
		</div><!-- #featured (end) -->
		
	<!-- FEATURED (end) -->
	
	<?php
	// Start main area
	themeblvd_main_start();
	themeblvd_main_top();
	
	// Breadcrumbs
	themeblvd_breadcrumbs();
	
	// Before sidebar+content layout
	themeblvd_before_layout();
	?>
	
	<div id="sidebar_layout">
		<div class="sidebar_layout-inner">
			<div class="grid-protection">

				<?php themeblvd_fixed_sidebars( 'left' ); ?>
				
				<!-- CONTENT (start) -->
	
				<div id="content" role="main">
					<div class="inner">
						<?php themeblvd_content_top(); ?>	
						<?php get_template_part( 'content-sermon' ); ?>
						<?php themeblvd_page_footer(); ?>
						<?php if( themeblvd_supports( 'comments', 'pages' ) ) comments_template( '', true ); ?>
					</div><!-- .inner (end) -->
				</div><!-- #content (end) -->
					
				<!-- CONTENT (end) -->	
					
				<!-- CONTENT (end) -->	
				
<div class="fixed-sidebar right-sidebar">
<div class="fixed-sidebar-inner">
<div class="widget-area widget-area-fixed">
	<aside id="endvr-widget-staff-details" class="widget">
		<div class="widget-inner">
			<a href="<?php bloginfo('url'); ?>/sermons/" title="Return to Sermon Archive">Return to Sermon Archive</a>
		</div>
	</aside>

	<aside id="endvr-widget-sermon-details" class="widget">
		<div class="widget-inner">
			<h3 class="widget-title">Sermon Details</h3>
			<ul class="sermon_details">
				<li class="sermon_title"><span class="prefix">Sermon Title:</span> <?php the_title(); ?></li>
				<li class="sermon_speaker"><span class="prefix">Sermon Speaker:</span>
					<?php $tax_sermon_speaker = get_terms('sermon_speaker'); foreach ($tax_sermon_speaker as $speaker) {				
					$speaker_link = '<a href="/staff/' . $speaker->slug . '" title="' . sprintf(__('View profile page for this sermon speaker: %s', 'my_localization_domain'), $speaker->name) . '">' . $speaker->name . '</a>';
					} echo $speaker_link; ?>
				</li>
				<li class="sermon_series"><span class="prefix">Sermon Series:</span>
					<?php $tax_sermon_series = get_terms('sermon_series'); foreach ($tax_sermon_series as $series) {				
					$series_link = '<a href="/sermon-series/' . $series->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series->name) . '">' . $series->name . '</a>';
					} echo $series_link; ?>
				</li>
				<li class="sermon_video"><span class="prefix">Browse Sermon Videos @ Vimeo.com:</span> <a href="http://vimeo.com/fbcprescott" title="Browse Sermon Videos @ Vimeo.com">http://vimeo.com/fbcprescott/</a></li>
				<li class="sermon_doc"><span class="prefix">Browse Sermon Outlines @ Scribd.com:</span> <a href="http://scribd.com/fbcprescott/" title="Browse Sermon Outlines @ Scribd.com">http://scribd.com/fbcprescott/</a></li>
			</ul>
		</div>
	</aside>	
</div><!-- .widget_area (end) -->
</div><!-- .fixed-sidebar-inner (end) -->
</div><!-- .fixed-sidebar (end) -->		
			
			</div><!-- .grid-protection (end) -->
		</div><!-- .sidebar_layout-inner (end) -->
	</div><!-- .sidebar-layout-wrapper (end) -->
	
	<?php	
	// End main area
	themeblvd_main_bottom();
	themeblvd_main_end();
	
// Footer
get_footer();