<?php
/**
 * The template file for Staff Member pages.
 * 
 * WARNING: This template file is a core part of the 
 * Theme Blvd WordPress Framework. This framework is 
 * designed around this file NEVER being altered. It 
 * is advised that any edits to the way this file 
 * displays its content be done with via hooks and filters.
 * 
 * @author		Jason Bobich
 * @copyright	Copyright (c) Jason Bobich
 * @link		http://jasonbobich.com
 * @link		http://themeblvd.com
 * @package 	Theme Blvd WordPress Framework
 */

// Header
get_header(); ?>

<?php
// Engage Post Type's Metaboxes
global $mb_staff_details;
$mb_staff_details->the_meta();
global $mb_staff_photos;
$mb_staff_photos->the_meta();
$meta_sd = get_post_meta(get_the_ID(), $mb_staff_details->get_the_ID(), TRUE);
?>
	
	<!-- FEATURED (start) -->
		
		<div id="featured">
			<div class="featured-inner has_page_featured">
				<div class="featured-content">
					<div class="element element-headline featured-entry-title">
					<h1 class="entry-title"><?php the_title(); ?></h1>
					<p class="tagline"><?php $mb_staff_details->the_field('staff_role'); $mb_staff_details->the_value(); ?></p>
					</div><!-- .element (end) -->					
					<div class="clear"></div>
				</div><!-- .featured-content (end) -->
				<div class="secondary-bg"></div>
			</div><!-- .featured-inner (end) -->
		</div><!-- #featured (end) -->
		
	<!-- FEATURED (end) -->
	
	<?php
	// Start main area
	themeblvd_main_start();
	themeblvd_main_top();
	
	// Breadcrumbs
	themeblvd_breadcrumbs();
	
	// Before sidebar+content layout
	themeblvd_before_layout();
	?>
	
	<div id="sidebar_layout">
		<div class="sidebar_layout-inner">
			<div class="grid-protection">

				<?php themeblvd_fixed_sidebars( 'left' ); ?>
				
				<!-- CONTENT (start) -->
	
				<div id="content" role="main">
					<div class="inner">
						<?php themeblvd_content_top(); ?>			
						<?php get_template_part( 'content', themeblvd_get_part( 'page' ) ); ?>
						<?php themeblvd_page_footer(); ?>
						<?php if( themeblvd_supports( 'comments', 'pages' ) ) comments_template( '', true ); ?>
					</div><!-- .inner (end) -->
				</div><!-- #content (end) -->
					
				<!-- CONTENT (end) -->	
					
				<!-- CONTENT (end) -->	
				
				<?php themeblvd_fixed_sidebars( 'right' ); ?>
				
<?php
// Display the content we want to show

function themeblvd_fixed_sidebars_content() {
	?>
<div class="fixed-sidebar right-sidebar">
<div class="fixed-sidebar-inner">
<div class="widget-area widget-area-fixed">
	<aside id="search-2" class="widget widget_search">
		<div class="widget-inner">
			<div class="themeblvd-search">
			    <form method="get" action="http://fbcp.jloft.com">
			        <fieldset>
			            <span class="input-wrap">
			            	<input type="text" class="search-input" name="s" onblur="if (this.value == '') {this.value = 'Search the site...';}" onfocus="if(this.value == 'Search the site...') {this.value = '';}" value="Search the site...">
			            </span>
			            <span class="submit-wrap">
			            	<input type="submit" class="submit" value="">
			            </span>
			        </fieldset>
			    </form>
			</div>
		</div>
	</aside>
	<aside id="endvr-widget-staff" class="widget widget_endvr_staff">
		<div class="widget-inner">
			<div class="endvr-widget-staff">
			    <p>Hello.</p>
			</div>
		</div>
	</aside>	
</div><!-- .widget_area (end) -->
</div><!-- .fixed-sidebar-inner (end) -->
</div><!-- .fixed-sidebar (end) -->
	<?php
}

// Remove all other actions and add in our custom function */

function themeblvd_custom_fixed_sidebars() {
	remove_all_actions( 'themeblvd_fixed_sidebars' ); // Remove all other other actions
	add_action( 'themeblvd_fixed_sidebars', 'themeblvd_fixed_sidebars_content' ); // Add our custom function
}

// Hook everything in after theme has been loaded */

add_action( 'after_setup_theme', 'themeblvd_custom_fixed_sidebars' );
?>
			
			</div><!-- .grid-protection (end) -->
		</div><!-- .sidebar_layout-inner (end) -->
	</div><!-- .sidebar-layout-wrapper (end) -->
	
	<?php	
	// End main area
	themeblvd_main_bottom();
	themeblvd_main_end();
	
// Footer
get_footer();