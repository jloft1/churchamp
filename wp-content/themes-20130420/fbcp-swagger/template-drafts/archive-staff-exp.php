<?php
/**
 * The template file for Staff Member pages.
 * 
 * WARNING: This template file is a core part of the 
 * Theme Blvd WordPress Framework. This framework is 
 * designed around this file NEVER being altered. It 
 * is advised that any edits to the way this file 
 * displays its content be done with via hooks and filters.
 * 
 * @author		Jason Bobich
 * @copyright	Copyright (c) Jason Bobich
 * @link		http://jasonbobich.com
 * @link		http://themeblvd.com
 * @package 	Theme Blvd WordPress Framework
 */

// Header
get_header(); ?>

<?php
// Engage Post Type's Metaboxes
global $mb_staff_details;
$mb_staff_details->the_meta();
global $mb_staff_photos;
$mb_staff_photos->the_meta();
?>
	
	<!-- FEATURED (start) -->
		
		<div id="featured">
			<div class="featured-inner has_page_featured">
				<div class="featured-content">
					<div class="element element-headline featured-entry-title">
						<h1 class="entry-title">Staff Directory</h1>
						<p class="tagline">Get to know our leaders</p>
					</div><!-- .element (end) -->	
					<aside id="featured-search" class="widget widget_search">
						<div class="widget-inner">
							<div class="themeblvd-search">
							    <form method="get" action="http://fbcp.jloft.com">
							        <fieldset>
							            <span class="input-wrap">
							            	<input type="text" class="search-input" name="s" onblur="if (this.value == '') {this.value = 'Search the site...';}" onfocus="if(this.value == 'Search the site...') {this.value = '';}" value="Search the site...">
							            </span>
							            <span class="submit-wrap">
							            	<input type="submit" class="submit" value="">
							            </span>
							        </fieldset>
							    </form>
							</div>
						</div>
					</aside> <!-- #featured-search (end) -->				
					<div class="clear"></div>
				</div><!-- .featured-content (end) -->
				<div class="secondary-bg"></div>
			</div><!-- .featured-inner (end) -->
		</div><!-- #featured (end) -->
		
	<!-- FEATURED (end) -->
	
	<!-- MAIN (start) -->
	
	<div id="main" class="full_width">
		<div class="main-inner">
			<div class="main-content">
				<div class="grid-protection">
					<div class="main-top"></div><!-- .main-top (end) -->	
	
	<div id="sidebar_layout">
		<div class="sidebar_layout-inner">
			<div class="grid-protection">
		
								
				<!-- CONTENT (start) -->
		
				<div id="content" role="main">
					<div class="inner">
						<div class="primary-post-grid post_grid_paginated post_grid">
							<div class="grid-protection">
		  
					<div class="grid-row">
					
						<div class="grid-item column grid_4">
							<div class="article-wrap">
								<article id="post-717" class="post-717 post type-post status-publish format-standard hentry category-portfolio">
									<div class="entry-content">
										<div class="featured-image-wrapper attachment-grid_4 wp-post-image"><div class="featured-image"><div class="featured-image-inner">
											<a href="" class="thumbnail">
											<img src="" alt="" />
											</a>
										</div><!-- .featured-image-inner (end) --></div><!-- .featured-image (end) --></div><!-- .featured-image-wrapper (end) -->				
										<h2 class="entry-title"><a href="" title=""></a></h2>
									</div><!-- .entry-content (end) -->
								</article><!-- .post (end) -->
							</div><!-- .article-wrap (end) -->
						</div><!-- .grid-item (end) -->
						
						<div class="grid-item column grid_4">
							<div class="article-wrap">
								<article id="post-717" class="post-717 post type-post status-publish format-standard hentry category-portfolio">
									<div class="entry-content">
										<div class="featured-image-wrapper attachment-grid_4 wp-post-image"><div class="featured-image"><div class="featured-image-inner">
											<a href="" class="thumbnail">
											<img src="" alt="" />
											</a>
										</div><!-- .featured-image-inner (end) --></div><!-- .featured-image (end) --></div><!-- .featured-image-wrapper (end) -->				
										<h2 class="entry-title"><a href="" title=""></a></h2>
									</div><!-- .entry-content (end) -->
								</article><!-- .post (end) -->
							</div><!-- .article-wrap (end) -->
						</div><!-- .grid-item (end) -->
						
						
						<div class="grid-item column grid_4 last">
							<div class="article-wrap">
								<article id="post-717" class="post-717 post type-post status-publish format-standard hentry category-portfolio">
									<div class="entry-content">
										<div class="featured-image-wrapper attachment-grid_4 wp-post-image"><div class="featured-image"><div class="featured-image-inner">
											<a href="" class="thumbnail">
											<img src="" alt="" />
											</a>
										</div><!-- .featured-image-inner (end) --></div><!-- .featured-image (end) --></div><!-- .featured-image-wrapper (end) -->				
										<h2 class="entry-title"><a href="" title=""></a></h2>
									</div><!-- .entry-content (end) -->
								</article><!-- .post (end) -->
							</div><!-- .article-wrap (end) -->
						</div><!-- .grid-item (end) -->
						
						
						<div class="clear"></div>
					
					</div><!-- .grid-row (end) -->
		
								<div class="clear">
							</div><!-- .post_grid (end) -->
						</div><!-- .grid-protection (end) -->
					</div><!-- .inner (end) -->
				</div><!-- #content (end) -->
					
				<!-- CONTENT (end) -->
				
							
			</div><!-- .grid-protection (end) -->
		</div><!-- .sidebar_layout-inner (end) -->
	</div><!-- .sidebar-layout-wrapper (end) -->
	
					<div class="main-bottom"></div><!-- .main-bottom (end) -->						
						<div class="clear"></div>
				</div><!-- .grid-protection (end) -->
			</div><!-- .main-content (end) -->
		</div><!-- .main-inner (end) -->
	</div><!-- #main (end) -->
	
	<!-- MAIN (end) -->
		
		
		
		
		
		
		
		
	
	<div id="sidebar_layout">
		<div class="sidebar_layout-inner">
			<div class="grid-protection">

				<?php themeblvd_fixed_sidebars( 'left' ); ?>
				
				<!-- CONTENT (start) -->
	
				<div id="content" role="main">
					<div class="inner">
						<?php themeblvd_content_top(); ?>	
						<?php get_template_part( 'content', themeblvd_get_part( 'page' ) ); ?>
						<?php themeblvd_page_footer(); ?>
						<?php if( themeblvd_supports( 'comments', 'pages' ) ) comments_template( '', true ); ?>
					</div><!-- .inner (end) -->
				</div><!-- #content (end) -->
					
				<!-- CONTENT (end) -->	
					
				<!-- CONTENT (end) -->	
				
<div class="fixed-sidebar right-sidebar">
<div class="fixed-sidebar-inner">
<div class="widget-area widget-area-fixed">
	<aside id="endvr-widget-staff-details" class="widget">
		<div class="widget-inner">
			<h3 class="widget-title">Contact Information</h3>
			<div class="endvr_contact connect_phone">
				<i class="connect_icon">&nbsp;</i>
                    PH: &nbsp; <?php $mb_staff_details->the_value('staff_phone'); ?>
			</div>
			<div class="endvr_contact connect_email">
                    <i class="connect_icon">&nbsp;</i>
                    <a href="mailto:<?php $mb_staff_email = $mb_staff_details->the_meta(); $emailaddy = $mb_staff_email['staff_email']; echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
                    </div>
		</div>
	</aside>	
	<aside id="endvr-widget-staff-photo" class="widget">
		<div class="widget-inner">
			<img src="<?php $mb_staff_photos->the_value('staff_photo_full'); ?>" alt="<?php the_title(); ?> <?php $mb_staff_details->the_value('staff_role'); ?>" width="300" />
		</div>
	</aside>	
</div><!-- .widget_area (end) -->
</div><!-- .fixed-sidebar-inner (end) -->
</div><!-- .fixed-sidebar (end) -->		
			
			</div><!-- .grid-protection (end) -->
		</div><!-- .sidebar_layout-inner (end) -->
	</div><!-- .sidebar-layout-wrapper (end) -->
	
	<?php	
	// End main area
	themeblvd_main_bottom();
	themeblvd_main_end();
	
// Footer
get_footer();