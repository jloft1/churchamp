<?php
/**
 * The template used for displaying posts in a grid.
 */
global $columns;
global $size;
global $counter;
global $location;
global $mb_staff_details;
$mb_staff_details->the_meta();
global $mb_staff_photos;
$mb_staff_photos->the_meta();
?>
<div class="grid-item column content-grid-endvr grid-staff <?php echo $size; ?><?php if( $counter % $columns == 0 ) echo ' last'; ?>">
	<div class="article-wrap">
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>					
			<div class="entry-content">
				<img src="<?php $mb_staff_photos->the_value('staff_photo_thumb'); ?>" alt="<?php the_title(); ?> <?php $mb_staff_details->the_value('staff_role'); ?>" width="<?php echo $size; ?>" />
				<h2 class="entry-title">

					<?php the_title(); ?><br />
					<span class="content-grid-tagline"><?php $mb_staff_details->the_value('staff_role'); ?></span>

				</h2>
			</div><!-- .entry-content -->
		</article><!-- #post-<?php the_ID(); ?> -->
		</a><!-- end anchor -->
	</div><!-- .article-wrap (end) -->
</div><!-- .grid-item (end) -->