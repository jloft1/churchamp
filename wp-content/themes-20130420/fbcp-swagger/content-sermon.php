<?php
/**
 * The template used for displaying single post content in single.php
 */
global $location;

// Engage Post Type's Metaboxes
global $mb_sermons;
$mb_sermons->the_meta();
$tax_sermon_series = get_query_var($wp_query->query_vars['sermon_series']);
$tax_sermon_speaker = get_query_var($wp_query->query_vars['sermon_speaker']);
?>

<div class="article-wrap single-post">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry-content">
			<?php if ($mb_sermons->have_value('sermon_audio')) { ?>
				<h3>Sermon Audio</h3>
				<div class="media">
					<?php $mb_sermons->the_value('sermon_audio'); ?> 
				</div>
			<?php } ?>
			<hr />
			<?php if ($mb_sermons->have_value('sermon_video')) { ?>
				<h3>Sermon Video</h3>
				<div class="media video">
					<?php $mb_sermons->the_value('sermon_video'); ?>
				</div>
			<?php } ?>
			<hr />
			<?php if ($mb_sermons->have_value('sermon_doc')) { ?>
				<h3>Sermon Outline</h3>
				<div class="media scribd">
					<?php $mb_sermons->the_value('sermon_doc'); ?>
				</div>
			<?php } ?>
			<hr />
			<?php if ($mb_sermons->have_value('sermon_scrip')) { ?>
				<h3>Sermon Scripture</h3>
				<div class="media esv">
					<?php $mb_sermon_scrip = $mb_sermons->the_value('sermon_scrip');
					 echo do_shortcode($mb_sermon_scrip);
					?>
				</div>
			<?php } ?>
			<?php themeblvd_blog_tags(); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', TB_GETTEXT_DOMAIN ), 'after' => '</div>' ) ); ?>
			<?php edit_post_link( __( 'Edit', TB_GETTEXT_DOMAIN ), '<span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-content -->
	</article><!-- #post-<?php the_ID(); ?> -->
</div><!-- .article-wrap (end) -->