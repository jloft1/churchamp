<?php
/*-----------------------------------------------------------------------------------*/
/* Example #1: Importing a custom stylesheet
/* 
/* The theme and the framework include several different stylesheets and custom styles 
/* associated with the Theme Options page. 
/* 
/* So, here we are calling a stylesheet after all styles of theme. Note that if we use 
/* WordPress wp_enque_style here and hooked into the wp_print_style action, this 
/* stylesheet would not come after the theme's custom CSS inserted from the Theme Options 
/* page. So, in this case we must hook it into wp_head and give it a prioirty of atleast 
/* 11 or higher.
/*-----------------------------------------------------------------------------------*/

function themeblvd_custom_css() {	
	echo '<link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/custom.css" type="text/css" />';
	echo "\n"; // line break for neatness
}
add_action( 'wp_head', 'themeblvd_custom_css', 11 ); // Note: Priority 11 (explanation above)

/*-----------------------------------------------------------------------------------*/
/* Example #2: Adding content to a theme hook
/* 
/* In this example we'll simply hook a custom function into one of the framework's 
/* built-in actions. Make sure to view the "Code Customizations" section of your 
/* documenation for a full breakdown of all available theme hooks.
/*-----------------------------------------------------------------------------------*/

function themeblvd_featured_end_custom () {
if ( ! is_front_page() ) {
	?>
	<aside id="featured-search" class="widget widget_search">
		<div class="widget-inner">
			<div class="themeblvd-search">
			    <form method="get" action="http://fbcp.jloft.com">
			        <fieldset>
			            <span class="input-wrap">
			            	<input type="text" class="search-input" name="s" onblur="if (this.value == '') {this.value = 'Search the site...';}" onfocus="if(this.value == 'Search the site...') {this.value = '';}" value="Search the site...">
			            </span>
			            <span class="submit-wrap">
			            	<input type="submit" class="submit" value="">
			            </span>
			        </fieldset>
			    </form>
			</div>
		</div>
	</aside>
	<?php
}
}
add_action( 'themeblvd_featured_end', 'themeblvd_featured_end_custom' );


/*-----------------------------------------------------------------------------------*/
/* Example #3: Modifying the content of a hook already being used
/* 
/* In this example, we'll take a theme hook "themeblvd_header_addon" that is already
/* used in the theme, remove the theme's action on it, and add in our own custom 
/* content. 
/* 
/* This can all be a little confusing because WordPress calls functions.php
/* of the child theme BEFORE functions.php of the main theme. So, for anything here
/* in the child theme to override, we need to hook it all in after the theme has been 
/* loaded with the action "after_setup_theme".
/*-----------------------------------------------------------------------------------*/

/* Display the content we want to show */
/*
function themeblvd_custom_header_addon_content() {
	?>
	<div style="float:right;width:60%;">
		<h3>Here is some custom content added from functions.php of the child theme example.</h3>
	</div>
	<?php
}
*/

/* Remove all other actions and add in our custom function */
/*
function themeblvd_custom_header_addon() {
	remove_all_actions( 'themeblvd_header_addon' ); // Remove all other other actions
	add_action( 'themeblvd_header_addon', 'themeblvd_custom_header_addon_content' ); // Add our custom function
}
*/

/* Hook everything in after theme has been loaded */
/*
add_action( 'after_setup_theme', 'themeblvd_custom_header_addon' );
*/

/*-----------------------------------------------------------------------------------*/
/* JLOFT Code
/*-----------------------------------------------------------------------------------*/

/* Define Child Directory */
define( 'FBCP_CHILDTHEME_URL', get_stylesheet_directory_uri() );

?>