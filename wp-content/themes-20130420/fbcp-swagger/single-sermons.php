<?php
/**
 * The template file for Sermon pages.
 * 
 * WARNING: This template file is a core part of the 
 * Theme Blvd WordPress Framework. This framework is 
 * designed around this file NEVER being altered. It 
 * is advised that any edits to the way this file 
 * displays its content be done with via hooks and filters.
 * 
 * @author		Jason Bobich
 * @copyright	Copyright (c) Jason Bobich
 * @link		http://jasonbobich.com
 * @link		http://themeblvd.com
 * @package 	Theme Blvd WordPress Framework
 */

// Header
get_header(); ?>

<?php
// Engage Post Type's Metaboxes
global $mb_sermons;
$mb_sermons->the_meta();
?>
	
	<!-- FEATURED (start) -->
		
		<div id="featured">
			<div class="featured-inner has_page_featured">
				<div class="featured-content">
					<div class="element element-headline featured-entry-title">
						<h1 class="entry-title"><?php the_title(); ?> &#40;<?php $mb_sermons->the_value('sermon_ref'); ?> &#41;</h1>
						<p class="tagline">
							A Sermon by&nbsp;
							<?php $tax_sermon_speaker = get_terms('sermon_speaker'); foreach ($tax_sermon_speaker as $speaker) {				
							$speaker_link = '<a href="/staff/' . $speaker->slug . '" title="' . sprintf(__('View profile page for this sermon speaker: %s', 'my_localization_domain'), $speaker->name) . '">' . $speaker->name . '</a>';
							} echo $speaker_link; ?>
							&nbsp;in the Series&nbsp;							
							<?php $tax_sermon_series = get_terms('sermon_series'); foreach ($tax_sermon_series as $series) {				
							$series_link = '<a href="/sermon-series/' . $series->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series->name) . '">' . $series->name . '</a>';
							} echo $series_link; ?>
						</p>
					</div><!-- .element (end) -->	
					<aside id="featured-search" class="widget widget_search">
						<div class="widget-inner">
							<div class="themeblvd-search">
							    <form method="get" action="http://fbcp.jloft.com">
							        <fieldset>
							            <span class="input-wrap">
							            	<input type="text" class="search-input" name="s" onblur="if (this.value == '') {this.value = 'Search the site...';}" onfocus="if(this.value == 'Search the site...') {this.value = '';}" value="Search the site...">
							            </span>
							            <span class="submit-wrap">
							            	<input type="submit" class="submit" value="">
							            </span>
							        </fieldset>
							    </form>
							</div>
						</div>
					</aside> <!-- #featured-search (end) -->
					<div class="clear"></div>
					<a class="archive_link" href="<?php bloginfo('url');?>/sermons/" title="Return to Sermon Archive">Return to Sermon Archive</a>
				</div><!-- .featured-content (end) -->
				<div class="secondary-bg"></div>
			</div><!-- .featured-inner (end) -->
		</div><!-- #featured (end) -->
		
	<!-- FEATURED (end) -->
	
<!-- MAIN (start) -->
	
	<div id="main" class="full_width">
		<div class="main-inner">
			<div class="main-content">
				<div class="grid-protection">
					<div class="main-top"></div><!-- .main-top (end) -->	
	
	<div id="sidebar_layout">
		<div class="sidebar_layout-inner">
			<div class="grid-protection">
		
								
				<!-- CONTENT (start) -->
	
				<div id="content" role="main">
					<div class="inner">
						<?php themeblvd_content_top(); ?>
						<div class="primary-post-grid post_grid_paginated post_grid<?php echo themeblvd_get_classes( 'element_post_grid_paginated', true ); ?>">
							<div class="grid-protection">
								
								<div class="article-wrap single-post">
									<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
										<div class="entry-content">
											<?php if ($mb_sermons->have_value('sermon_audio')) { ?>
												<div class="media audio">
													<h3 class="sermon_audio">Sermon Audio</h3>
													<span class="sermon_external_link"><span class="prefix podcast">Podcast :</span> Subscribe @ Feedburner (<a href="http://feedburner.com/fbcprescott" title="Subscribe to the Sermon Podcast @ Feedburner.com">http://feedburner.com/fbcprescott/</a>)</span>
													 	<div id="jquery_jplayer_1" class="jp-jplayer"></div>
														  <div id="jp_container_1" class="jp-audio">
														    <div class="jp-type-single">
														      <div class="jp-gui jp-interface">
														        <ul class="jp-controls">
														          <li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
														          <li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
														          <li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
														          <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
														          <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
														          <li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
														        </ul>
														        <div class="jp-progress">
														          <div class="jp-seek-bar">
														            <div class="jp-play-bar"></div>
														          </div>
														        </div>
														        <div class="jp-volume-bar">
														          <div class="jp-volume-bar-value"></div>
														        </div>
														        <div class="jp-time-holder">
														          <div class="jp-current-time"></div>
														          <div class="jp-duration"></div>
														          <ul class="jp-toggles">
														            <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
														            <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
														          </ul>
														        </div>
														      </div>
														      <div class="jp-title">
														        <ul>
														          <li><?php the_title(); ?> &#40;<?php $mb_sermons->the_value('sermon_ref'); ?> &#41;</li>
														        </ul>
														      </div>
														      <div class="jp-no-solution">
														        <span>Update Required</span>
														        To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
														      </div>
														    </div>
														  </div>
												</div>
											<?php } ?>
											<br />
											<hr />
											<?php if ($mb_sermons->have_value('sermon_video')) { ?>
												<div class="media video">
													<h3 class="sermon_video">Sermon Video</h3>
													<span class="sermon_external_link"><span class="prefix vimeo">Videos :</span> Watch @ Vimeo (<a href="http://vimeo.com/fbcprescott" title="Watch Sermon Videos @ Vimeo.com">http://vimeo.com/fbcprescott/</a>)</span>
													<div class="themeblvd-video-wrapper">
													<div class="video-inner">
													<?php $mb_sermons->the_value('sermon_video'); ?>
													</div>
													</div>
												</div>
											<?php } ?>
											<br />
											<hr />
											<?php if ($mb_sermons->have_value('sermon_doc')) { ?>
												<div class="media scribd">
													<h3 class="sermon_outline">Sermon Outline</h3>
													<span class="sermon_external_link"><span class="prefix scribd">Outlines :</span> Download @ Scribd (<a href="http://scribd.com/fbcprescott/" title="Download Sermon Outlines @ Scribd.com">http://scribd.com/fbcprescott/</a>)</span>
													<?php $mb_sermons->the_value('sermon_doc'); ?>
												</div>
											<?php } ?>
											<br />
											<hr />
											<?php if ($mb_sermons->have_value('sermon_scrip')) { ?>
												<div class="media esv">
													<h3 class="sermon_scrip">Sermon Scripture</h3>
													<span class="sermon_external_link"><span class="prefix esv">Online Bible :</span> Read @ ESV (<a href="http://esvbible.org" title="Read the Bible Online @ ESVBible.org">http://esvbible.org/</a>)</span>											
													<?php $mb_sermon_scrip = $mb_sermons->the_value('sermon_scrip');
													 echo do_shortcode($mb_sermon_scrip);
													?>
												</div>
											<?php } ?>
											<?php themeblvd_blog_tags(); ?>
											<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', TB_GETTEXT_DOMAIN ), 'after' => '</div>' ) ); ?>
											<?php edit_post_link( __( 'Edit', TB_GETTEXT_DOMAIN ), '<span class="edit-link">', '</span>' ); ?>
										</div><!-- .entry-content -->
									</article><!-- #post-<?php the_ID(); ?> -->
									
								</div><!-- .article-wrap (end) -->
							</div><!-- .grid-protection (end) -->
						</div><!-- .post_grid (end) -->
					</div><!-- .inner (end) -->
				</div><!-- #content (end) -->
					
				<!-- CONTENT (end) -->
				
							
			</div><!-- .grid-protection (end) -->
		</div><!-- .sidebar_layout-inner (end) -->
	</div><!-- .sidebar-layout-wrapper (end) -->
	
					<div class="main-bottom"></div><!-- .main-bottom (end) -->						
						<div class="clear"></div>
				</div><!-- .grid-protection (end) -->
			</div><!-- .main-content (end) -->
		</div><!-- .main-inner (end) -->
	</div><!-- #main (end) -->
	
	<!-- MAIN (end) -->

<?php		
// Footer
get_footer();
?>