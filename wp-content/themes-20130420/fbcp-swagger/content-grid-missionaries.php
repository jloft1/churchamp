<?php
/**
 * The template used for displaying posts in a grid.
 */
global $columns;
global $size;
global $counter;
global $location;
global $mb_missionary_details;
$mb_missionary_details->the_meta();
global $mb_missionary_photos;
$mb_missionary_photos->the_meta();
$terms = get_the_terms($post->ID,'mission_country');
foreach ($terms as $term) { $mission_country = $term->name; }
?>
<div class="grid-item column content-grid-endvr grid-missionary <?php echo $size; ?><?php if( $counter % $columns == 0 ) echo ' last'; ?>">
	<div class="article-wrap">
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>					
			<div class="entry-content">
				<img src="<?php $mb_missionary_photos->the_value('missionary_photo_thumb'); ?>" alt="<?php the_title(); ?> <?php $mb_missionary_details->the_value('missionary_role'); ?>" width="100%" />
				<h2 class="entry-title">

					<?php the_title(); ?><br />
					<ul>
						<li class="content-grid-tagline"><?php $mb_missionary_details->the_value('missionary_org'); ?></li>
						<li><?php $mb_missionary_details->the_value('missionary_role'); ?></li>
						<li><?php $mb_missionary_details->the_value('missionary_location'); ?></li>
					</ul>
					
				</h2>
			</div><!-- .entry-content -->
		</article><!-- #post-<?php the_ID(); ?> -->
		</a><!-- end anchor -->
	</div><!-- .article-wrap (end) -->
</div><!-- .grid-item (end) -->