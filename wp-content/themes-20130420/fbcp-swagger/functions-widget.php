<?php
/*-----------------------------------------------------------------------------------*/
/* Example #1: Importing a custom stylesheet
/* 
/* The theme and the framework include several different stylesheets and custom styles 
/* associated with the Theme Options page. 
/* 
/* So, here we are calling a stylesheet after all styles of theme. Note that if we use 
/* WordPress wp_enque_style here and hooked into the wp_print_style action, this 
/* stylesheet would not come after the theme's custom CSS inserted from the Theme Options 
/* page. So, in this case we must hook it into wp_head and give it a prioirty of atleast 
/* 11 or higher.
/*-----------------------------------------------------------------------------------*/

function themeblvd_custom_css() {	
	echo '<link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/custom.css" type="text/css" />';
	echo "\n"; // line break for neatness
}
add_action( 'wp_head', 'themeblvd_custom_css', 11 ); // Note: Priority 11 (explanation above)

/*-----------------------------------------------------------------------------------*/
/* Example #2: Adding content to a theme hook
/* 
/* In this example we'll simply hook a custom function into one of the framework's 
/* built-in actions. Make sure to view the "Code Customizations" section of your 
/* documenation for a full breakdown of all available theme hooks.
/*-----------------------------------------------------------------------------------*/

/*
function themeblvd_header_menu_addon_custom () {
	?>
<ul id="primary-menu" class="sf-menu custom">
<li>
<a href="http://fuller.edu/dmin">Fuller Theological Seminary Doctor of Ministry</a>
</li>
</ul>
	<?php
}
add_action( 'themeblvd_header_menu_addon', 'themeblvd_header_menu_addon_custom' );
*/

/*-----------------------------------------------------------------------------------*/
/* Example #3: Modifying the content of a hook already being used
/* 
/* In this example, we'll take a theme hook "themeblvd_header_addon" that is already
/* used in the theme, remove the theme's action on it, and add in our own custom 
/* content. 
/* 
/* This can all be a little confusing because WordPress calls functions.php
/* of the child theme BEFORE functions.php of the main theme. So, for anything here
/* in the child theme to override, we need to hook it all in after the theme has been 
/* loaded with the action "after_setup_theme".
/*-----------------------------------------------------------------------------------*/



/*-----------------------------------------------------------------------------------*/
/* JLOFT Code
/*-----------------------------------------------------------------------------------*/

/* Define Child Directory */
define( 'FBCP_CHILDTHEME_URL', get_stylesheet_directory_uri() );

function home_page_menu_args( $args ) {
$args['show_home'] = true;
return $args;
}
add_filter( 'wp_page_menu_args', 'home_page_menu_args' );

function enable_more_buttons($buttons) {
  $buttons[] = 'hr';
 return $buttons;
}
add_filter("mce_buttons", "enable_more_buttons");

/* Custom Widgets */
	
function fbcp_widget_setup () {	
	/*------------------------------------------------------*/
	/* Admin Hooks, Filters, and Files
	/*------------------------------------------------------*/
	
	// Include files
	require_once( FBCP_CHILDTHEME_URL . '/widgets/fbcp-widget-contact.php' );
	}