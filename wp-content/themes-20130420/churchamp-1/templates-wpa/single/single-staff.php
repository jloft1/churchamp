<?php
// Engage Post Type's Metaboxes
global $mb_staff_details;
$mb_staff_details->the_meta();
global $mb_staff_photos;
$mb_staff_photos->the_meta();
?>

<div id="main" class="span8" role="main">
	<?php tha_content_top(); ?>
	
		<?php while (have_posts()) : the_post(); ?>
		  <?php the_content(); ?>
		  <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
		<?php endwhile; ?>
		
	<?php tha_content_bottom(); ?>
</div><!-- /#main -->

<?php tha_sidebars_before(); ?>
<aside id="sidebar" class="span4" role="complementary">
	<?php tha_sidebar_top(); ?>
	
	<section id="endvr-widget-staff-details" class="widget">
		<div class="widget-inner">
			<h3 class="widget-title">Contact Information</h3>
			<?php if ($mb_staff_details->have_value('staff_phone')) { ?>
			<div class="endvr_contact connect_phone">
				<i class="icon-phone">&nbsp;</i>
                    PH: &nbsp; <?php $mb_staff_details->the_value('staff_phone'); ?>
			</div>
			<?php } ?>
			<?php if ($mb_staff_details->have_value('staff_email')) { ?>
			<div class="endvr_contact connect_email">
                    <i class="icon-envelope-alt">&nbsp;</i>
                    <a href="mailto:<?php $mb_staff_email = $mb_staff_details->the_meta(); $emailaddy = $mb_staff_email['staff_email']; echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
               </div>
               <?php } ?>
		</div>
	</section>	
	<section id="endvr-widget-staff-photo" class="widget">
		<div class="widget-inner">
			<img src="<?php $mb_staff_photos->the_value('staff_photo_full'); ?>" alt="<?php the_title(); ?> <?php $mb_staff_details->the_value('staff_role'); ?>" width="300" />
		</div>
	</section>
     	
     <?php tha_sidebar_bottom(); ?>
</aside>
<?php tha_sidebars_after(); ?>	