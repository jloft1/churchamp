<?php
// Engage Post Type's Metaboxes
global $mb_sermons;
$mb_sermons->the_meta();
?>

<?php
if ( current_theme_supports( 'tha_hooks', 'feature' ) ) {
	function ws_page_title_top_sermon() { 
		echo '<div class="row"><div class="feature-header span8" role="heading"><h1 class="feature-title">'; 
	}
	function ws_page_title_sermon() { 
		echo roots_title();
		echo '<p class="tagline">A Sermon by&nbsp;';
		$tax_sermon_speaker = get_terms('sermon_speaker'); foreach ($tax_sermon_speaker as $speaker) {				
				$speaker_link = '<a href="/staff/' . $speaker->slug . '" title="' . sprintf(__('View profile page for this sermon speaker: %s', 'my_localization_domain'), $speaker->name) . '">' . $speaker->name . '</a>';
				} 
		echo $speaker_link;
		echo '&nbsp;in the Series&nbsp;';							
		$tax_sermon_series = get_terms('sermon_series'); foreach ($tax_sermon_series as $series) {				
				$series_link = '<a href="/sermon-series/' . $series->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series->name) . '">' . $series->name . '</a>';
				} 
		echo $series_link;
		echo '</p>';
	}
	function ws_page_title_bottom_sermon() { 
		echo '</h1></div><!-- /.feature-header -->';
		echo '<div class="feature-search span4 visible-desktop">';
		get_template_part('templates/meta/searchform');
		echo '</div><!-- /.span4 --></div><!-- /.row -->';
	 }
	add_action( 'tha_feature_top', 'ws_page_title_top_sermon' );	
	add_action( 'tha_feature', 'ws_page_title_sermon' );	
	add_action( 'tha_feature_bottom', 'ws_page_title_bottom_sermon' );
	function ws_page_title_output_sermon() {
		do_action( 'ws_page_title_top_sermon' );	
		do_action( 'ws_page_title_sermon' );	
		do_action( 'ws_page_title_bottom_sermon' );		
	}	
}
?>

<div id="main" class="span12" role="main">
	<?php tha_content_top(); ?>
	
											<?php if ($mb_sermons->have_value('sermon_audio')) { ?>
												<div class="media audio">
													<h3 class="sermon_audio">Sermon Audio</h3>
													<span class="sermon_external_link"><span class="prefix podcast">Podcast :</span> Subscribe @ Feedburner (<a href="http://feedburner.com/fbcprescott" title="Subscribe to the Sermon Podcast @ Feedburner.com">http://feedburner.com/fbcprescott/</a>)</span>
													 	<div id="jquery_jplayer_1" class="jp-jplayer"></div>
														  <div id="jp_container_1" class="jp-audio">
														    <div class="jp-type-single">
														      <div class="jp-gui jp-interface">
														        <ul class="jp-controls">
														          <li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
														          <li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
														          <li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
														          <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
														          <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
														          <li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
														        </ul>
														        <div class="jp-progress">
														          <div class="jp-seek-bar">
														            <div class="jp-play-bar"></div>
														          </div>
														        </div>
														        <div class="jp-volume-bar">
														          <div class="jp-volume-bar-value"></div>
														        </div>
														        <div class="jp-time-holder">
														          <div class="jp-current-time"></div>
														          <div class="jp-duration"></div>
														          <ul class="jp-toggles">
														            <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
														            <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
														          </ul>
														        </div>
														      </div>
														      <div class="jp-title">
														        <ul>
														          <li><?php the_title(); ?> &#40;<?php $mb_sermons->the_value('sermon_ref'); ?> &#41;</li>
														        </ul>
														      </div>
														      <div class="jp-no-solution">
														        <span>Update Required</span>
														        To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
														      </div>
														    </div>
														  </div>
												</div>
											<?php } ?>
											<br />
											<hr />
											<?php if ($mb_sermons->have_value('sermon_video')) { ?>
												<div class="media video">
													<h3 class="sermon_video">Sermon Video</h3>
													<span class="sermon_external_link"><span class="prefix vimeo">Videos :</span> Watch @ Vimeo (<a href="http://vimeo.com/fbcprescott" title="Watch Sermon Videos @ Vimeo.com">http://vimeo.com/fbcprescott/</a>)</span>
													<div class="themeblvd-video-wrapper">
													<div class="video-inner">
													<?php $mb_sermons->the_value('sermon_video'); ?>
													</div>
													</div>
												</div>
											<?php } ?>
											<br />
											<hr />
											<?php if ($mb_sermons->have_value('sermon_doc')) { ?>
												<div class="media scribd">
													<h3 class="sermon_outline">Sermon Outline</h3>
													<span class="sermon_external_link"><span class="prefix scribd">Outlines :</span> Download @ Scribd (<a href="http://scribd.com/fbcprescott/" title="Download Sermon Outlines @ Scribd.com">http://scribd.com/fbcprescott/</a>)</span>
													<?php $mb_sermons->the_value('sermon_doc'); ?>
												</div>
											<?php } ?>
											<br />
											<hr />
											<?php if ($mb_sermons->have_value('sermon_scrip')) { ?>
												<div class="media esv">
													<h3 class="sermon_scrip">Sermon Scripture</h3>
													<span class="sermon_external_link"><span class="prefix esv">Online Bible :</span> Read @ ESV (<a href="http://esvbible.org" title="Read the Bible Online @ ESVBible.org">http://esvbible.org/</a>)</span>											
													<?php $mb_sermon_scrip = $mb_sermons->the_value('sermon_scrip');
													 echo do_shortcode($mb_sermon_scrip);
													?>
												</div>
											<?php } ?>
		
	<?php tha_content_bottom(); ?>
</div><!-- /#main -->	