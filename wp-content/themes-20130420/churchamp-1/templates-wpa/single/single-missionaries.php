<?php
// Engage Post Type's Metaboxes
global $mb_missionary_details;
$mb_missionary_details->the_meta();
global $mb_missionary_photos;
$mb_missionary_photos->the_meta();
global $mb_missionary_news;
$mb_missionary_news->the_meta();
?>

<div id="main" class="span8" role="main">
	<?php tha_content_top(); ?>
	
		<?php while (have_posts()) : the_post(); ?>
		  <?php the_content(); ?>
		  <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
		<?php endwhile; ?>
		
	<?php tha_content_bottom(); ?>
</div><!-- /#main -->

<?php tha_sidebars_before(); ?>
<aside id="sidebar" class="span4" role="complementary">
	<?php tha_sidebar_top(); ?>
	
	<section id="endvr-widget-missionary-photo" class="widget">
		<div class="widget-inner">
			<img src="<?php $mb_missionary_photos->the_value('missionary_photo_thumb'); ?>" alt="<?php the_title(); ?> <?php $mb_missionary_details->the_value('missionary_role'); ?>" width="280" />
		</div>
	</section>
	<section id="endvr-widget-missionary-details-org" class="widget">
		<div class="widget-inner">
			<h3 class="widget-title">Mission Information</h3>
			<?php if ($mb_missionary_details->have_value('missionary_role')) { ?>
			<div class="endvr_missio">
                    <span class="prefix">ROLE:</span> <?php $mb_missionary_details->the_value('missionary_role'); ?>
			</div>
			<?php } ?>	
			<?php if ($mb_missionary_details->have_value('missionary_location')) { ?>
			<div class="endvr_missio">
                    <span class="prefix">LOC:</span> <?php $mb_missionary_details->the_value('missionary_location'); ?>
			</div>
			<?php } ?>
				<hr>			
			<?php if ($mb_missionary_details->have_value('missionary_org')) { ?>
			<div class="endvr_missio">
                    <span class="prefix">ORG:</span> <?php $mb_missionary_details->the_value('missionary_org'); ?>
			</div>
			<?php } ?>
			<?php if ($mb_missionary_details->have_value('missionary_website_org')) { ?>
			<div class="endvr_missio">
                    <span class="prefix">URL:</span> 
                    <a href="<?php $mb_missionary_details->the_value('missionary_website_org'); ?>" title="<?php $mb_missionary_details->the_value('missionary_org'); ?>">
                    <?php $mb_missionary_details->the_value('missionary_website_org'); ?></a>
			</div>
			<?php } ?>
				<hr>
			<?php if ($mb_missionary_details->have_value('missionary_project')) { ?>
			<div class="endvr_missio">
                    <span class="prefix">PROJ:</span> <?php $mb_missionary_details->the_value('missionary_project'); ?>
			</div>
			<?php } ?>
			<?php if ($mb_missionary_details->have_value('missionary_website_project')) { ?>
			<div class="endvr_missio">
                    <span class="prefix">URL:</span> 
                    <a href="<?php $mb_missionary_details->the_value('missionary_website_project'); ?>" title="<?php $mb_missionary_details->the_value('missionary_project'); ?>">
                    <?php $mb_missionary_details->the_value('missionary_website_project'); ?></a>
			</div>
			<?php } ?>                           
		</div>				
	</section>
	<section id="endvr-widget-missionary-details-contact" class="widget">
		<div class="widget-inner">
			<h3 class="widget-title">Contact Information</h3>
			<?php if ($mb_missionary_details->have_value('missionary_phone_field')) { ?>
			<div class="endvr_contact connect_phone">
				<i class="icon-phone">&nbsp;</i>
                    <span class="prefix">PH (Field):</span> <?php $mb_missionary_details->the_value('missionary_phone_field'); ?>
			</div>
			<?php } ?>
			<?php if ($mb_missionary_details->have_value('missionary_phone_home')) { ?>
			<div class="endvr_contact connect_phone">
				<i class="icon-phone">&nbsp;</i>
                    <span class="prefix">PH (Home):</span> <?php $mb_missionary_details->the_value('missionary_phone_home'); ?>
			</div>			
			<?php } ?>
			<?php if ($mb_missionary_details->have_value('missionary_address_field')) { ?>
			<div class="endvr_contact connect_postal">
				<i class="icon-globe">&nbsp;</i>
                    <span class="prefix">ADDRESS (Field):</span><br><span class="endvr_contact_connect_postal_address"><?php $mb_missionary_details->the_value('missionary_address_field'); ?></span>
			</div>			
			<?php } ?>  
			<?php if ($mb_missionary_details->have_value('missionary_address_home')) { ?>
			<div class="endvr_contact connect_postal">
				<i class="icon-map-marker">&nbsp;</i>
                    <span class="prefix">ADDRESS (Home):</span><br><span class="endvr_contact_connect_postal_address"><?php $mb_missionary_details->the_value('missionary_address_home'); ?></span>
			</div>			
			<?php } ?>
			<?php if ($mb_missionary_details->have_value('missionary_email')) { ?>
			<div class="endvr_contact connect_email">
                    <i class="icon-envelope-alt">&nbsp;</i>
                    <a href="mailto:<?php $mb_missionary_email = $mb_missionary_details->the_meta(); $emailaddy = $mb_missionary_email['missionary_email']; echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
               </div>
               <?php } ?>
			<?php if ($mb_missionary_details->have_value('missionary_email_alt')) { ?>
			<div class="endvr_contact connect_email">
                    <i class="icon-envelope">&nbsp;</i>
                    <a href="mailto:<?php $mb_missionary_email = $mb_missionary_details->the_meta(); $emailaddy = $mb_missionary_email['missionary_email_alt']; echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
               </div>
               <?php } ?>             		                           
		</div>
	</section>
	<section id="endvr-widget-missionary-news" class="widget">
		<div class="widget-inner">
			<h3 class="widget-title">Newsletters</h3>	
			<ul class="endvr_news">	
				<?php while ($mb_missionary_news->have_fields('docs')) { ?>
					<li>
						<i class="icon-bullhorn">&nbsp;</i>
						<a href="http://docs.google.com/viewer?url=<?php $mb_missionary_news->the_value('missionary_news_doc'); ?>" title="Newsletter: <?php $mb_missionary_news->the_value('missionary_news_date'); ?> - <?php the_title(); ?>">
						NEWS &#40;<?php $mb_missionary_news->the_value('missionary_news_date'); ?>&#41;
						</a>
					</li>
				<?php } ?>
			</ul>
		</div>
	</section>
     	
     <?php tha_sidebar_bottom(); ?>
</aside>
<?php tha_sidebars_after(); ?>	