<div id="main" class="span12" role="main">
	<?php tha_content_top(); ?>
	
		<h2>Missionaries Supported by FBC Prescott</h2><br>
		<?php
		$ws_grid_columns = 4;
		$ws_span_size = ws_grid_class( $ws_grid_columns );
		$posts_per_page = -1;
		$query_string = array(
		'post_type' => 'missionaries',
		'posts_per_page' => -1,
		'orderby' => 'title', 
		'order' => 'ASC'
		);
		query_posts( $query_string );
		$ws_item_counter = 1;
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				if( $ws_item_counter == 1 ) ws_open_row();
				get_template_part( 'templates/grids/grid-missionaries' );
				if( $ws_item_counter % $ws_grid_columns == 0 ) ws_close_row();
				if( $ws_item_counter % $ws_grid_columns == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
				$ws_item_counter++;
			}
			if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
		} else {
			echo '<p>Apologies, but there are no missionaries to display.</p>';
		}
		?>

	<?php tha_content_bottom(); ?>
</div><!-- /#main -->