<?php
// Engage Post Type's Metaboxes
global $mb_sermons;
$mb_sermons->the_meta();
?>

<div id="main" class="span12" role="main">
	<?php tha_content_top(); ?>
	
		<?php $tax_sermon_series = get_terms('sermon_series'); foreach ($tax_sermon_series as $series) { ?>
		
		<?php
		$series_link = '<a href="/sermon-series/' . $series->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series->name) . '">' . $series->name . '</a>';
		?>
		
		<h2><?php echo $series_link; ?></h2><br>
		<?php
		$ss = $series->slug;
		$ws_grid_columns = 4;
		$ws_span_size = ws_grid_class( $ws_grid_columns );
		$posts_per_page = -1;
		$query_string = array(
		'sermon_series' => $ss,
		'posts_per_page' => -1,
		'orderby' => 'date', 
		'order' => 'DESC'
		);
		query_posts( $query_string );
		$ws_item_counter = 1;
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				if( $ws_item_counter == 1 ) ws_open_row();
				get_template_part( 'templates/grids/grid-sermons' );
				if( $ws_item_counter % $ws_grid_columns == 0 ) ws_close_row();
				if( $ws_item_counter % $ws_grid_columns == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
				$ws_item_counter++;
			}
			if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
		} else {
			echo '<p>Apologies, but there are no sermons to display.</p>';
		}
		?>
		
		<?php } ?>

	<?php tha_content_bottom(); ?>
</div><!-- /#main -->