<?php
/**
 * The template used for displaying staff in a grid.
 */
global $ws_grid_columns;
$ws_span_size = ws_grid_class( $ws_grid_columns );
global $mb_missionary_details;
$mb_missionary_details->the_meta();
global $mb_missionary_photos;
$mb_missionary_photos->the_meta();
$terms = get_the_terms($post->ID,'mission_country');
foreach ($terms as $term) { $mission_country = $term->name; }
?>
<div class="grid-item <?php echo $ws_span_size; ?>">
	<a class="gi-anchor well" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>					
			<img class="gi-img" src="<?php $mb_missionary_photos->the_value('missionary_photo_thumb'); ?>" alt="<?php the_title(); ?> <?php $mb_missionary_details->the_value('missionary_role'); ?>">
			<header class="gi-heading">
				<h3 class="gi-title"><?php the_title(); ?></h3>
				<ul class="gi-tagline">
					<li><?php $mb_missionary_details->the_value('missionary_org'); ?></li>
					<li><?php $mb_missionary_details->the_value('missionary_role'); ?></li>
					<li><?php $mb_missionary_details->the_value('missionary_location'); ?></li>
				</ul>
			</header>
		</article><!-- #post-<?php the_ID(); ?> -->
	</a><!-- end anchor -->
</div><!-- .grid-item (end) -->