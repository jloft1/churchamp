<?php
/**
 * The template used for displaying staff in a grid.
 */
global $ws_grid_columns;
$ws_span_size = ws_grid_class( $ws_grid_columns );
global $mb_staff_details;
$mb_staff_details->the_meta();
global $mb_staff_photos;
$mb_staff_photos->the_meta();
?>
<div class="grid-item <?php echo $ws_span_size; ?>">
	<a class="gi-anchor well" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>					
			<img class="gi-img" src="<?php $mb_staff_photos->the_value('staff_photo_thumb'); ?>" alt="<?php the_title(); ?> <?php $mb_staff_details->the_value('staff_role'); ?>">
			<header class="gi-heading">
				<h3 class="gi-title"><?php the_title(); ?></h3>
				<span class="gi-tagline"><?php $mb_staff_details->the_value('staff_role'); ?></span>
			</header>
		</article><!-- #post-<?php the_ID(); ?> -->
	</a><!-- end anchor -->
</div><!-- .grid-item (end) -->