<div id="masthead" <?php ws_masthead_class(); ?>>
	<div class="container">
	<div class="row">
	<div class="span12">
		<div class="row">
			<div id="brand" class="span6">
			      <a href="<?php echo home_url(); ?>/">
			        	<?php 
			        	$ws_brand = of_get_option('ws_brand');
			        	$ws_brand_font_text = of_get_option('ws_brand_font_text');
			        	$ws_brand_mark = of_get_option('ws_brand_mark');
			        	$ws_brand_logo = of_get_option('ws_brand_logo'); 
			        	?>    
			        		<?php if ( $ws_brand == 'one' ) : ?>
			        			<span class="brand_font_text"><?php echo $ws_brand_font_text; ?></span>
			        		<?php endif; ?>
			        		<?php if ( $ws_brand == 'two' ) : ?>
			        			<?php if ( $ws_brand_mark != '' ) : ?>
			        				<img class="brand_mark" src="<?php echo $ws_brand_mark; ?>" alt="<?php echo $ws_brand_font_text; ?>" /> 
			        			<?php endif; ?>
			        			<span class="brand_font_text"><?php echo $ws_brand_font_text; ?></span>
			        		<?php endif; ?>
			        		<?php if ( $ws_brand == 'three' ) : ?>
			        			<?php if ( $ws_brand_logo != '' ) : ?>
			        				<img class="brand_logo" src="<?php echo $ws_brand_logo; ?>" alt="<?php echo $ws_brand_font_text; ?>" /> 
			        			<?php endif; ?>
			        		<?php endif; ?>	
			      </a>
			</div>
			<div id="leaderboard" class="span6">
				<section id="endvr_social_media-banner" class="widget widget_endvr_social_media visible-desktop">                                                   
                    
                    <div class="endvr_social connect_twitter">
                    <a href="http://twitter.com/fbcprescott/" title="Twitter" target="_blank"><i class="connect_icon">&nbsp;</i></a>
                    </div>
                    
                    <div class="endvr_social connect_facebook">
                    <a href="http://facebook.com/fbcprescott/" title="Facebook" target="_blank"><i class="connect_icon">&nbsp;</i></a>
                    </div>

                    <div class="endvr_social connect_scribd">
                    <a href="http://scribd.com/fbcprescott/" title="Scribd" target="_blank"><i class="connect_icon">&nbsp;</i></a>
                    </div>
                    
                    <div class="endvr_social connect_vimeo">
                    <a href="http://vimeo.com/fbcprescott/" title="Vimeo" target="_blank"><i class="connect_icon">&nbsp;</i></a>
                    </div>
                    
                    </section>
			</div>
		</div>
	</div>	
	</div>
	</div>	
</div>