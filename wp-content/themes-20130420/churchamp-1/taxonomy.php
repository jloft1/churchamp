<?php 
$slug = get_query_var($wp_query->query_vars['taxonomy']);
$term = get_term_by('slug',$slug,$wp_query->query_vars['taxonomy']);
$tax = $term->taxonomy;
get_template_part( 'templates/taxonomy/taxonomy', $tax ); 
?>