<div id="main" class="span12" role="main">
	<?php tha_content_top(); ?>

		<?php if (!have_posts()) : ?>
		  <div class="alert alert-block fade in">
		    <a class="close" data-dismiss="alert">&times;</a>
		    <p><?php _e('Sorry, no results were found.', 'roots'); ?></p>
		  </div>
		  <?php get_search_form(); ?>
		<?php endif; ?>
		
		<?php while (have_posts()) : the_post(); ?>
		  <?php tha_entry_before(); ?>
		  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		  <?php tha_entry_top(); ?>
		    <header>
		      <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
		    </header>
		    <div class="entry-summary">
		      <?php the_excerpt(); ?>
		    </div>
		    <?php tha_entry_bottom(); ?>
		    <a href="<?php the_permalink(); ?>" title="READ MORE of <?php the_title(); ?>" class="btn readmore">Read More</a>
		  </article>
		  <?php tha_entry_after(); ?>
		<hr>  
		<?php endwhile; ?>
		
		<?php if ($wp_query->max_num_pages > 1) : ?>
		  <nav id="post-nav">
		    <ul class="pager">
		      <?php if (get_next_posts_link()) : ?>
		        <li class="previous"><?php next_posts_link(__('&larr; Older posts', 'roots')); ?></li>
		      <?php else: ?>
		        <li class="previous disabled"><a><?php _e('&larr; Older posts', 'roots'); ?></a></li>
		      <?php endif; ?>
		      <?php if (get_previous_posts_link()) : ?>
		        <li class="next"><?php previous_posts_link(__('Newer posts &rarr;', 'roots')); ?></li>
		      <?php else: ?>
		        <li class="next disabled"><a><?php _e('Newer posts &rarr;', 'roots'); ?></a></li>
		      <?php endif; ?>
		    </ul>
		  </nav>
		<?php endif; ?>

	<?php tha_content_bottom(); ?>
</div><!-- /#main -->