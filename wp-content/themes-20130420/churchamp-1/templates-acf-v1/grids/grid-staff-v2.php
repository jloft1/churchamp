<?php
/**
 * The template used for displaying staff in a grid.
 */
global $ws_grid_columns;
$ws_span_size = ws_grid_class( $ws_grid_columns );
$staff_photo_thumb = wp_get_attachment_image_src(get_field('_endvr_staff_photo_thumb'), 'full');
?>
<div class="grid-item <?php echo $ws_span_size; ?>">
	<a class="gi-anchor well" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>					
			<img class="gi-img" src="<?php echo $staff_photo_thumb[0]; ?>" alt="<?php the_title(); ?> <?php the_field('_endvr_staff_role'); ?>">
			<header class="gi-heading">
				<h3 class="gi-title"><?php the_title(); ?></h3>
				<span class="gi-tagline"><?php the_field('_endvr_staff_role'); ?></span>
			</header>
		</article><!-- #post-<?php the_ID(); ?> -->
	</a><!-- end anchor -->
</div><!-- .grid-item (end) -->