<?php
/**
 * The template used for displaying staff in a grid.
 */
global $ws_grid_columns;
$ws_span_size = ws_grid_class( $ws_grid_columns );
global $tax_sermon_series;
global $series;
global $series_link;
?>
<div class="grid-item <?php echo $ws_span_size; ?>">
	<a class="gi-anchor well" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="gi-heading">
				<h3 class="gi-title"><?php the_title(); ?></h3>
				<span class="gi-tagline">&#40;<?php the_field('_endvr_sermon_ref'); ?> &#41;</span>
			</header>
			<span class="gi-details">
				<span class="prefix">A Sermon by</span><br>
				<span class="gi-speaker"><i><?php $tax_sermon_speaker = get_terms('sermon_speaker'); foreach ($tax_sermon_speaker as $speaker); echo $speaker->name; ?></i></span>
			</span>
			<span class="gi-date"><?php the_time('Y - M - d'); ?></span>
		</article><!-- #post-<?php the_ID(); ?> -->
	</a><!-- end anchor -->
</div><!-- .grid-item (end) -->