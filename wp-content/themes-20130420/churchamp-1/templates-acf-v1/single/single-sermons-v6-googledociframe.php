<div id="main" class="main span12" role="main">
	<?php tha_content_top(); ?>
	
	<?php if ( function_exists('wordstrap_breadcrumbs') ) { wordstrap_breadcrumbs(); } ?>	
	
	<div class="sermon-external-link">
		<span class="prefix">Archive :</span>
		<a class="archive_link" href="<?php bloginfo('url');?>/sermons/" title="Return to Sermon Archive">Return to Sermons Index Page</a>
	</div>
		
	<div class="row">
	
		<div class="sermon-detail span6">
			<h3><i class="icon-bookmark"></i>&nbsp; Sermon Details</h3>
			<div>
				<span class="prefix">Speaker :</span>
				<?php $tax_sermon_speaker = get_terms('sermon_speaker'); foreach ($tax_sermon_speaker as $speaker) {				
				$speaker_link = '<a href="/staff/' . $speaker->slug . '" title="' . sprintf(__('View profile page for this sermon speaker: %s', 'my_localization_domain'), $speaker->name) . '">' . $speaker->name . '</a>';
				} echo $speaker_link; ?>
			</div>	
			<div>
				<span class="prefix series">Series :</span>								
				<?php $tax_sermon_series = get_terms('sermon_series'); foreach ($tax_sermon_series as $series) {				
				$series_link = '<a href="/sermon-series/' . $series->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series->name) . '">' . $series->name . '</a>';
				} echo $series_link; ?>
			</div>
			<div>
				<span class="prefix scripture">Scripture :</span>								
				<?php the_field('_endvr_sermon_ref'); ?>
			</div>												
		</div><!-- /.sermon-details -->

		<div class="sermon-audio span6">
			<h3><i class="icon-headphones"></i>&nbsp; Sermon Audio</h3>
				<?php
					$endvr_sermon_audio_id = get_field('_endvr_sermon_audio');
					$endvr_sermon_audio_src = wp_get_attachment_url( $endvr_sermon_audio_id );
				?>	
			<div class="sermon-external-link">
				<span class="prefix">Download :</span> 
				Right click and select "Save Link As..."<br>
				&#40;<a href="<?php echo $endvr_sermon_audio_src; ?>" title="Download the Sermon Audio" target="_blank"><?php echo $endvr_sermon_audio_src; ?></a>&#41;
			</div>
<!--			
			<div class="sermon-external-link">
				<span class="prefix">Podcast :</span> 
				Subscribe @ Feedburner 
				&#40;<a href="http://feedburner.com/fbcprescott" title="Subscribe to the Sermon Podcast @ Feedburner.com">http://feedburner.com/fbcprescott/</a>&#41;
			</div>
-->			
			<?php if (get_field('_endvr_sermon_audio')) { ?>
				<div class="media audio">
					<?php echo do_shortcode('[audio src="'.$endvr_sermon_audio_src.'"]'); ?>
				</div><!-- /.media -->
			<?php } else {
				echo '<p>There is no audio file available for this sermon at this time.</p>';	
			} ?>
			<!--
				Proper way to use medialement with ACF: http://support.advancedcustomfields.com/discussion/3195/acf-with-audio-player/p1
				Necessary edit to mediaelement plugin: http://wordpress.org/support/topic/undefined-variable-notices-after-update
			-->
		</div><!-- /.sermon-audio -->

	</div><!-- /.row -->
	
	<hr>		

		<div class="sermon-video">
			<h3><i class="icon-facetime-video"></i>&nbsp; Sermon Video</h3>
			<div class="sermon-external-link">
				<span class="prefix">Channel :</span> 
				Go to the FBC Prescott Video Channel 
				&#40;<a href="http://vimeo.com/fbcprescott" title="Go to the FBC Prescott Video Channel @ Vimeo" target="_blank">http://vimeo.com/fbcprescott/</a>&#41;
			</div>
			<div class="sermon-external-link">
				<span class="prefix">Video :</span> 
				Watch this Sermon Video 
				&#40;<a href="<?php the_field('_endvr_sermon_video'); ?>" title="Watch this sermon video @ Vimeo" target="_blank"><?php the_field('_endvr_sermon_video'); ?></a>&#41;
			</div>
			<?php if (get_field('_endvr_sermon_video')) { ?>
				<div class="media video">
					<?php
					$endvr_sermon_video_src = get_field('_endvr_sermon_video');
					echo do_shortcode('[video align="left" aspect_ratio="4:3" width="100"]'.$endvr_sermon_video_src.'[/video]');
					?>
				</div><!-- /.media -->
			<?php } else {
				echo '<p>There is no video available for this sermon at this time.</p>';
			} ?>
		</div><!-- /.sermon-video .span6 -->
	
	<hr>

		<div class="sermon-doc">
			<h3><i class="icon-edit"></i>&nbsp; Sermon Outline</h3>
			<div class="sermon_external_link">
				<span class="prefix">Outlines :</span> 
				Download Sermon Outlines @ Google Docs
				&#40;<a href="https://docs.google.com/folder/d/0B0iORpZkAY4dTkRWMWZjalhrdE0/edit?usp=sharing" title="Download Sermon Outlines @ Scribd.com" target="_blank">http://goo.gl/ygV9t</a>&#41;
			</div>
			<?php if (get_field('_endvr_sermon_doc')) { ?>
				<div class="media doc">
					<?php the_field('_endvr_sermon_doc'); ?>			
				</div><!-- /.media -->
			<?php } else {
				echo '<p>There is no outline document available for this sermon at this time.</p>';
			} ?>
		</div>

		<br />
		<hr />
		<?php if (get_field('_endvr_sermon_ref')) { ?>
			<div class="media esv">
				<h3 class="sermon_scrip"><i class="icon-book"></i>&nbsp; Sermon Scripture</h3>
				<span class="sermon_external_link"><span class="prefix esv">Online Bible :</span> Read @ ESV (<a href="http://esvbible.org" title="Read the Bible Online @ ESVBible.org">http://esvbible.org/</a>)</span>											
				<?php $mb_sermon_scrip = the_field('_endvr_sermon_ref');
				 echo do_shortcode($mb_sermon_scrip);
				?>
			</div>
		<?php } ?>
		
	<?php tha_content_bottom(); ?>
</div><!-- /#main -->