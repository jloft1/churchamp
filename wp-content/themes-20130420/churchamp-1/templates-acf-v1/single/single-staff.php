<div id="main" class="main span8" role="main">
	<div class="standardtxt">
	<?php tha_content_top(); ?>
	
		<?php if ( function_exists('wordstrap_breadcrumbs') ) { wordstrap_breadcrumbs(); } ?>
	
		<?php while (have_posts()) : the_post(); ?>
		  <?php tha_entry_before(); ?>
		  <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
		    <?php tha_entry_top(); ?>		    
		    <div class="entry-content">
		      <?php the_content(); ?>
		    </div>
		    <footer>
		      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
		    </footer>
		    <?php tha_entry_bottom(); ?>
		  </article>
		  <?php tha_entry_after(); ?>
		<?php endwhile; ?>

	<?php tha_content_bottom(); ?>
	</div><!-- /.standardtxt -->
</div><!-- /.main -->

<?php tha_sidebars_before(); ?>
<aside id="sidebar" class="sidebar span4" role="complementary">
	<?php tha_sidebar_top(); ?>
	
	<section id="endvr-widget-staff-details" class="widget">
		<div class="widget-inner">
			<h3 class="widget-title">Contact Information</h3>
			<ul>
				<?php if (get_field('_endvr_staff_phone')) { ?>
				<li class="staff-phone">
					<i class="icon-phone i-circle i-invert">&nbsp;</i>
	                    &nbsp;PH: &nbsp; <?php the_field('_endvr_staff_phone'); ?>
				</li>
				<?php } ?>
				<?php if (get_field('_endvr_staff_email')) { ?>
				<li class="staff-email">
	                    <i class="icon-envelope-alt i-circle i-invert">&nbsp;</i>
	                    &nbsp;<a href="mailto:<?php $emailaddy = get_field('_endvr_staff_email'); echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
	               </li>
	               <?php } ?>
			</ul>
		</div>
	</section>	
	<section id="endvr-widget-staff-photo" class="widget">
		<div class="widget-inner">
			<img src="<?php the_field('_endvr_staff_photo_full'); ?>" alt="<?php the_title(); ?> <?php the_field('_endvr_staff_role'); ?>" width="340" />
		</div>
	</section>
     	
     <?php tha_sidebar_bottom(); ?>
</aside>
<?php tha_sidebars_after(); ?>