<div id="main" class="main span8" role="main">
	<?php tha_content_top(); ?>
	
		<?php while (have_posts()) : the_post(); ?>
		  <?php the_content(); ?>
		  <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
		<?php endwhile; ?>
		
	<?php tha_content_bottom(); ?>
</div><!-- /#main -->

<?php tha_sidebars_before(); ?>
<aside id="sidebar" class="sidebar span4" role="complementary">
	<?php tha_sidebar_top(); ?>
	
	<section id="endvr-widget-missionary-photo" class="widget">
		<div class="widget-inner">
			<img src="<?php the_field('_endvr_missionary_photo_thumb'); ?>" alt="<?php the_title(); ?> <?php the_field('_endvr_missionary_role'); ?>" width="280" />
		</div>
	</section>
	<section id="endvr-widget-missionary-details-org" class="widget">
		<div class="widget-inner">
			<h3 class="widget-title">Mission Information</h3>
			<?php if(get_field('_endvr_missionary_role')) { ?>
			<div class="endvr_missio">
                    <span class="prefix">ROLE:</span> <?php the_field('_endvr_missionary_role'); ?>
			</div>
			<?php } ?>	
			<?php if(get_field('_endvr_missionary_location')) { ?>
			<div class="endvr_missio">
                    <span class="prefix">LOC:</span> <?php the_field('_endvr_missionary_location'); ?>
			</div>
			<?php } ?>
				<hr>			
			<?php if(get_field('_endvr_missionary_org')) { ?>
			<div class="endvr_missio">
                    <span class="prefix">ORG:</span> <?php the_field('_endvr_missionary_org'); ?>
			</div>
			<?php } ?>
			<?php if(get_field('_endvr_missionary_website_org')) { ?>
			<div class="endvr_missio">
                    <span class="prefix">URL:</span> 
                    <a href="<?php the_field('_endvr_missionary_website_org'); ?>" title="<?php the_field('_endvr_missionary_org'); ?>">
                    <?php the_field('_endvr_missionary_website_org'); ?></a>
			</div>
			<?php } ?>
				<hr>
			<?php if(get_field('_endvr_missionary_project')) { ?>
			<div class="endvr_missio">
                    <span class="prefix">PROJ:</span> <?php the_field('_endvr_missionary_project'); ?>
			</div>
			<?php } ?>
			<?php if(get_field('_endvr_missionary_website_project')) { ?>
			<div class="endvr_missio">
                    <span class="prefix">URL:</span> 
                    <a href="<?php the_field('_endvr_missionary_website_project'); ?>" title="<?php the_field('_endvr_missionary_project'); ?>">
                    <?php the_field('_endvr_missionary_website_project'); ?></a>
			</div>
			<?php } ?>                           
		</div>				
	</section>
	<section id="endvr-widget-missionary-details-contact" class="widget">
		<div class="widget-inner">
			<h3 class="widget-title">Contact Information</h3>
			<?php if(get_field('_endvr_missionary_phone_field')) { ?>
			<div class="endvr_contact connect_phone">
				<i class="icon-phone">&nbsp;</i>
                    <span class="prefix">PH (Field):</span> <?php the_field('_endvr_missionary_phone_field'); ?>
			</div>
			<?php } ?>
			<?php if(get_field('_endvr_missionary_phone_home')) { ?>
			<div class="endvr_contact connect_phone">
				<i class="icon-phone">&nbsp;</i>
                    <span class="prefix">PH (Home):</span> <?php the_field('_endvr_missionary_phone_home'); ?>
			</div>			
			<?php } ?>
			<?php if(get_field('_endvr_missionary_address_field')) { ?>
			<div class="endvr_contact connect_postal">
				<i class="icon-globe">&nbsp;</i>
                    <span class="prefix">ADDRESS (Field):</span><br><span class="endvr_contact_connect_postal_address"><?php the_field('_endvr_missionary_address_field'); ?></span>
			</div>			
			<?php } ?>  
			<?php if(get_field('_endvr_missionary_address_home')) { ?>
			<div class="endvr_contact connect_postal">
				<i class="icon-map-marker">&nbsp;</i>
                    <span class="prefix">ADDRESS (Home):</span><br><span class="endvr_contact_connect_postal_address"><?php the_field('_endvr_missionary_address_home'); ?></span>
			</div>			
			<?php } ?>
			<?php if(get_field('_endvr_missionary_email')) { ?>
			<div class="endvr_contact connect_email">
                    <i class="icon-envelope-alt">&nbsp;</i>
                    <a href="mailto:<?php $emailaddy = get_field('_endvr_missionary_email'); echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
               </div>
               <?php } ?>
			<?php if(get_field('_endvr_missionary_email_alt')) { ?>
			<div class="endvr_contact connect_email">
                    <i class="icon-envelope">&nbsp;</i>
                    <a href="mailto:<?php $emailaddy = get_field('_endvr_missionary_email_alt'); echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
               </div>
               <?php } ?>             		                           
		</div>
	</section>
	<section id="endvr-widget-missionary-news" class="widget">
		<div class="widget-inner">
			<h3 class="widget-title">Newsletters</h3>	
			<ul class="endvr_news">	
			<?php $rows = get_field('_endvr_missionary_newsletters');
				if($rows) {
					foreach($rows as $row) {
					echo '
					<li>
						<i class="icon-bullhorn">&nbsp;</i>
						<a href="http://docs.google.com/viewer?url='. $row['_endvr_missionary_news_doc'].'" title="Newsletter: '. $row['_endvr_missionary_news_date'].' - '.the_title().'">
						NEWS &#40;'. $row['_endvr_missionary_news_date'].'&#41;
						</a>
					</li>
					';
				}
			} ?>
			</ul>
		</div>
	</section>
     	
     <?php tha_sidebar_bottom(); ?>
</aside>
<?php tha_sidebars_after(); ?>	