<div <?php ws_navbar_class(); ws_affix(); ?>>
  <div <?php ws_navbarinner_class(); ?>>
    <div class="container">
    <a class="brand" href="<?php echo home_url(); ?>/">
        	<?php 
        	$ws_brand = of_get_option('ws_brand');
        	$ws_brand_font_text = of_get_option('ws_brand_font_text');
        	$ws_brand_mark = of_get_option('ws_brand_mark');
        	$ws_brand_logo = of_get_option('ws_brand_logo'); 
        	$ws_navbarbrand = of_get_option('ws_navbarbrand');
        	$ws_navbarbrandlogo = of_get_option('ws_navbarbrandlogo');
        	?>    
        		<?php if ( $ws_navbarbrand == 'masthead-brand' ) { ?>
	        		<?php if ( $ws_brand == 'one' ) : ?>
	        			<span class="brand_font_text"><?php echo $ws_brand_font_text; ?></span>
	        		<?php endif; ?>
	        		<?php if ( $ws_brand == 'two' ) : ?>
	        			<?php if ( $ws_brand_mark != '' ) : ?>
	        				<img class="brand_mark" src="<?php echo $ws_brand_mark; ?>" alt="<?php echo $ws_brand_font_text; ?>" /> 
	        			<?php endif; ?>
	        			<span class="brand_font_text"><?php echo $ws_brand_font_text; ?></span>
	        		<?php endif; ?>
	        		<?php if ( $ws_brand == 'three' ) : ?>
	        			<?php if ( $ws_brand_logo != '' ) : ?>
	        				<img class="brand_logo" src="<?php echo $ws_brand_logo; ?>" alt="<?php echo $ws_brand_font_text; ?>" /> 
	        			<?php endif; ?>
	        		<?php endif; ?>
	        	<?php } ?>	
	        	<?php if ( $ws_navbarbrand == 'navbar-brand' ) : ?>
	        		<img class="brand_logo" src="<?php echo $ws_navbarbrandlogo; ?>" alt="<?php echo $ws_brand_font_text; ?>" />
	        	<?php endif; ?>     			
      </a><!-- /.brand -->
      <div class="mobile-navbar">
	      <div class="mobile-menu">  	   
	      	<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
	      		<strong>MENU</strong>
	      		<i class="icon-list-ul"></i>
	      	</a><!-- /.btn-navbar -->
	      </div>
	      <div class="mobile-quicklinks">    
	      	<a class="btn btn-navbar btn-search" data-toggle="collapse" data-target=".search-collapse">
	      		<i class="icon-search"></i>
	      	</a><!-- /.btn-search -->   
	      	<a class="btn btn-navbar btn-contact" data-toggle="collapse" data-target=".contact-collapse">
	      		<i class="icon-envelope-alt"></i>
	      	</a><!-- /.btn-contact -->	
	      	<a class="btn btn-navbar ss-icon ss-social visible-phone btn-facebook" href="https://www.facebook.com/pages/First-Baptist-Church/113383848684823?fref=ts" title="Facebook" target="_blank">
               	<i class="ss-facebook">Facebook</i>
               </a>
	      	<a class="btn btn-navbar ss-icon ss-social visible-phone btn-vimeo" href="http://vimeo.com/fbcprescott" title="Vimeo" target="_blank">
               	<i class="ss-vimeo">Vimeo</i>
               </a>               
	      </div>
      </div><!-- /.mobile-navbar -->
      <nav class="nav-main nav-collapse collapse" role="navigation">
        <?php
          if (has_nav_menu('primary_navigation')) {
            wp_nav_menu(array( 'theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'link_before' => '<span>', 'link_after' => '</span>' ));
          }
        ?>
      </nav><!-- /.nav-collapse -->
      <nav class="nav-main ql-collapse collapse search-collapse" role="navigation">
         <?php get_template_part('templates/meta/navbar-search'); ?>
      </nav><!-- /.search-collapse -->
      <nav class="nav-main ql-collapse collapse contact-collapse" role="navigation">
         <?php get_template_part('templates/meta/navbar-contact'); ?>
      </nav><!-- /.contact-collapse -->            
    </div><!-- /.container -->
  </div><!-- /.navbar-inner -->
</div><!-- /.navbar -->