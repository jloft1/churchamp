<?php while (have_posts()) : the_post(); ?>
<?php tha_entry_before(); ?>
<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title"><?php echo roots_title(); ?></span><br>
					<span class="feature-subtitle"><?php the_field('_endvr_staff_role'); ?></span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-search span4 visible-desktop">
				<?php get_template_part('templates/meta/searchform'); ?>
			</div><!-- /.feature-search -->
			<div class="feature-link span4 visible-desktop">
				<a href="../" title="View Staff Directory">View Staff Directory</a>
			</div><!-- /.feature-link -->
		<?php tha_feature_bottom(); ?>	
		</div><!-- /.row -->
	</div><!-- /.container -->		
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span8'); ?> role="main">
	<div class="main-inner">
	
		<?php tha_entry_top(); ?>
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
		<footer>
			 <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
		</footer>
		<?php tha_entry_bottom(); ?>

	</div><!-- /.main-inner -->	
</div><!-- /.main -->

<?php tha_sidebars_before(); ?>
<aside id="sidebar" <?php ws_sidebar_class('span4'); ?> role="complementary">
	<div class="sidebar-inner">
		<?php tha_sidebar_top(); ?>

			<section id="endvr-widget-staff-details" class="widget">
				<div class="widget-inner">
					<h3 class="widget-title">Contact Information</h3>
					<ul>
						<?php if (get_field('_endvr_staff_phone')) { ?>
						<li class="staff-phone">
							<i class="icon-phone i-circle i-invert">&nbsp;</i>
			                    &nbsp;PH: &nbsp; <?php the_field('_endvr_staff_phone'); ?>
						</li>
						<?php } ?>
						<?php if (get_field('_endvr_staff_email')) { ?>
						<li class="staff-email">
			                    <i class="icon-envelope-alt i-circle i-invert">&nbsp;</i>
			                    &nbsp;<a href="mailto:<?php $emailaddy = get_field('_endvr_staff_email'); echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
			               </li>
			               <?php } ?>
					</ul>
				</div>
			</section>	
			<section id="endvr-widget-staff-photo" class="widget">
				<div class="widget-inner">
					<img src="<?php the_field('_endvr_staff_photo_full'); ?>" alt="<?php the_title(); ?> <?php the_field('_endvr_staff_role'); ?>" width="340" />
				</div>
			</section>
	
	     <?php tha_sidebar_bottom(); ?>
	</div><!-- /.sidebar-inner -->
</aside><!-- /.sidebar -->
<?php tha_sidebars_after(); ?>

<?php get_template_part('templates/structure/content-bottom-after'); ?>

</article><!-- /article -->
<?php tha_entry_after(); ?>
<?php endwhile; ?>