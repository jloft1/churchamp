<div id="main" class="span8" role="main">
	<?php tha_content_top(); ?>
	
		<?php while (have_posts()) : the_post(); ?>
		  <?php tha_entry_before(); ?>
		  <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
		    <?php tha_entry_top(); ?>
		    <div class="entry-content">
		      <?php the_content(); ?>
		    </div>
		    <?php tha_entry_bottom(); ?>
		  </article>
		  <?php tha_entry_after(); ?>
		<?php endwhile; ?>

	<?php tha_content_bottom(); ?>
</div><!-- /#main -->

<?php get_template_part('templates/structure/sidebar'); ?>