<?php while (have_posts()) : the_post(); ?>
<?php tha_entry_before(); ?>
<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title"><?php echo roots_title(); ?></span><br>
					<span class="feature-subtitle"><?php the_field('_endvr_missionary_org'); ?> &#40;<?php the_field('_endvr_missionary_location'); ?>&#41;</span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-search span4 visible-desktop">
				<?php get_template_part('templates/meta/searchform'); ?>
			</div><!-- /.feature-search -->
			<div class="feature-link span4 visible-desktop">
				<a href="../" title="View Missionary Directory">View Missionary Directory</a>
			</div><!-- /.feature-link -->
		<?php tha_feature_bottom(); ?>	
		</div><!-- /.row -->
	</div><!-- /.container -->		
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span8'); ?> role="main">
	<div class="main-inner">
	
		<?php tha_entry_top(); ?>
		<div class="entry-content">
			<div class="missionary-profile">
				<h2><i class="icon-bookmark"></i>&nbsp; Missionary Profile</h3>
				<?php the_content(); ?>
			</div><!-- /.missionary-profile -->
		</div>
		<footer>
			 <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
		</footer>
		<?php tha_entry_bottom(); ?>

	</div><!-- /.main-inner -->	
</div><!-- /.main -->

<?php tha_sidebars_before(); ?>
<aside id="sidebar" <?php ws_sidebar_class('span4'); ?> role="complementary">
	<div class="sidebar-inner">
		<?php tha_sidebar_top(); ?>

			<section id="endvr-widget-missionary-photo" class="widget">
				<div class="widget-inner">
					<img src="<?php the_field('_endvr_missionary_photo_full'); ?>" alt="<?php the_title(); ?> <?php the_field('_endvr_missionary_role'); ?>" width="340" />
				</div>
			</section>
			<section id="endvr-widget-missionary-details-org" class="widget">
				<div class="widget-inner">
					<h3 class="widget-title">Mission Information</h3>
					<?php if(get_field('_endvr_missionary_role')) { ?>
					<div class="endvr_missio">
		                    <span class="prefix">ROLE:</span> <?php the_field('_endvr_missionary_role'); ?>
					</div>
					<?php } ?>	
					<?php if(get_field('_endvr_missionary_location')) { ?>
					<div class="endvr_missio">
		                    <span class="prefix">LOC:</span> <?php the_field('_endvr_missionary_location'); ?>
					</div>
					<?php } ?>
						<hr>			
					<?php if(get_field('_endvr_missionary_org')) { ?>
					<div class="endvr_missio">
		                    <span class="prefix">ORG:</span> <?php the_field('_endvr_missionary_org'); ?>
					</div>
					<?php } ?>
					<?php if(get_field('_endvr_missionary_website_org')) { ?>
					<div class="endvr_missio">
		                    <span class="prefix">URL:</span> 
		                    <a href="<?php the_field('_endvr_missionary_website_org'); ?>" title="<?php the_field('_endvr_missionary_org'); ?>">
		                    <?php the_field('_endvr_missionary_website_org'); ?></a>
					</div>
					<?php } ?>
						<hr>
					<?php if(get_field('_endvr_missionary_project')) { ?>
					<div class="endvr_missio">
		                    <span class="prefix">PROJ:</span> <?php the_field('_endvr_missionary_project'); ?>
					</div>
					<?php } ?>
					<?php if(get_field('_endvr_missionary_website_project')) { ?>
					<div class="endvr_missio">
		                    <span class="prefix">URL:</span> 
		                    <a href="<?php the_field('_endvr_missionary_website_project'); ?>" title="<?php the_field('_endvr_missionary_project'); ?>">
		                    <?php the_field('_endvr_missionary_website_project'); ?></a>
					</div>
					<?php } ?>                           
				</div>				
			</section>
			<section id="endvr-widget-missionary-details-contact" class="widget">
				<div class="widget-inner">
					<h3 class="widget-title">Contact Information</h3>
					<?php if(get_field('_endvr_missionary_phone_field')) { ?>
					<div class="endvr_contact connect_phone">
						<i class="icon-phone">&nbsp;</i>
		                    <span class="prefix">PH (Field):</span> <?php the_field('_endvr_missionary_phone_field'); ?>
					</div>
					<?php } ?>
					<?php if(get_field('_endvr_missionary_phone_home')) { ?>
					<div class="endvr_contact connect_phone">
						<i class="icon-phone">&nbsp;</i>
		                    <span class="prefix">PH (Home):</span> <?php the_field('_endvr_missionary_phone_home'); ?>
					</div>			
					<?php } ?>
					<?php if(get_field('_endvr_missionary_address_field')) { ?>
					<div class="endvr_contact connect_postal">
						<i class="icon-globe">&nbsp;</i>
		                    <span class="prefix">ADDRESS (Field):</span><br><span class="endvr_contact_connect_postal_address"><?php the_field('_endvr_missionary_address_field'); ?></span>
					</div>			
					<?php } ?>  
					<?php if(get_field('_endvr_missionary_address_home')) { ?>
					<div class="endvr_contact connect_postal">
						<i class="icon-map-marker">&nbsp;</i>
		                    <span class="prefix">ADDRESS (Home):</span><br><span class="endvr_contact_connect_postal_address"><?php the_field('_endvr_missionary_address_home'); ?></span>
					</div>			
					<?php } ?>
					<?php if(get_field('_endvr_missionary_email')) { ?>
					<div class="endvr_contact connect_email">
		                    <i class="icon-envelope-alt">&nbsp;</i>
		                    <a href="mailto:<?php $emailaddy = get_field('_endvr_missionary_email'); echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
		               </div>
		               <?php } ?>
					<?php if(get_field('_endvr_missionary_email_alt')) { ?>
					<div class="endvr_contact connect_email">
		                    <i class="icon-envelope">&nbsp;</i>
		                    <a href="mailto:<?php $emailaddy = get_field('_endvr_missionary_email_alt'); echo antispambot($emailaddy, 1); ?>"><?php echo antispambot($emailaddy, 0); ?></a>
		               </div>
		               <?php } ?>             		                           
				</div>
			</section>
	
	     <?php tha_sidebar_bottom(); ?>
	</div><!-- /.sidebar-inner -->
</aside><!-- /.sidebar -->
<?php tha_sidebars_after(); ?>

<?php get_template_part('templates/structure/content-bottom-after'); ?>

</article><!-- /article -->
<?php tha_entry_after(); ?>
<?php endwhile; ?>	