<div id="main" class="main span12" role="main">
	<?php tha_content_top(); ?>
	
	<?php if ( function_exists('wordstrap_breadcrumbs') ) { wordstrap_breadcrumbs(); } ?>	
	
	<div class="sermon-external-link">
		<span class="prefix">Archive :</span>
		<a class="archive_link" href="<?php bloginfo('url');?>/sermons/" title="Return to Sermon Archive">Return to Sermons Index Page</a>
	</div>
		
	<div class="row">
	
		<div class="sermon-detail-audio span6">
		
			<div class="sermon-detail">
				<h3><i class="icon-bookmark"></i>&nbsp; Sermon Details</h3>
				<div>
					<span class="prefix">Speaker :</span>
					<?php $tax_sermon_speaker = get_terms('sermon_speaker'); foreach ($tax_sermon_speaker as $speaker) {				
					$speaker_link = '<a href="/staff/' . $speaker->slug . '" title="' . sprintf(__('View profile page for this sermon speaker: %s', 'my_localization_domain'), $speaker->name) . '">' . $speaker->name . '</a>';
					} echo $speaker_link; ?>
				</div>	
				<div>
					<span class="prefix series">Series :</span>								
					<?php $tax_sermon_series = get_terms('sermon_series'); foreach ($tax_sermon_series as $series) {				
					$series_link = '<a href="/sermon-series/' . $series->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series->name) . '">' . $series->name . '</a>';
					} echo $series_link; ?>
				</div>
				<div>
					<span class="prefix scripture">Scripture :</span>								
					<?php the_field('_endvr_sermon_ref'); ?>
				</div>												
			</div><!-- /.sermon-details -->
	
			<div class="sermon-audio">
				<h3><i class="icon-headphones"></i>&nbsp; Sermon Audio</h3>
				<div class="sermon-external-link">
					<span class="prefix">Podcast :</span> 
					Subscribe @ Feedburner 
					&#40;<a href="http://feedburner.com/fbcprescott" title="Subscribe to the Sermon Podcast @ Feedburner.com">http://feedburner.com/fbcprescott/</a>&#41;
				</div>
				<?php if (get_field('_endvr_sermon_audio')) { ?>
					<div class="media">
						<?php
						$endvr_sermon_audio_src = get_field('_endvr_sermon_audio');
						echo do_shortcode('[audio src="'.$endvr_sermon_audio_src.'"]');
						?>
					</div><!-- /.media -->
				<?php } else {
					echo '<p>There is no audio file available for this sermon at this time.</p>';	
				} ?>
				<!--
					Proper way to use medialement with ACF: http://support.advancedcustomfields.com/discussion/3195/acf-with-audio-player/p1
					Necessary edit to mediaelement plugin: http://wordpress.org/support/topic/undefined-variable-notices-after-update
				-->
			</div><!-- /.sermon-audio -->
		
		</div><!-- /.sermon-detail-audio .span6 -->

		<div class="sermon-video span6">
			<h3><i class="icon-facetime-video"></i>&nbsp; Sermon Video</h3>
			<div class="sermon-external-link">
				<span class="prefix">Videos :</span> 
				Watch @ Vimeo 
				&#40;<a href="http://vimeo.com/fbcprescott" title="Watch Sermon Videos @ Vimeo.com">http://vimeo.com/fbcprescott/</a>&#41;
			</div>
			<?php if (get_field('_endvr_sermon_video')) { ?>
				<div class="media">
					<?php
					$endvr_sermon_video_src = get_field('_endvr_sermon_video');
					echo do_shortcode('[video type="video/vimeo" src="'.$endvr_sermon_video_src.'"]');
					?>
				</div><!-- /.media -->
			<?php } ?>
		</div><!-- /.sermon-video .span6 -->
		
	</div><!-- /.row -->
	
	<hr>

		<?php if (get_field('_endvr_sermon_doc')) { ?>
			<div class="media scribd span12">
				<h3 class="sermon_outline"><i class="icon-edit"></i>&nbsp; Sermon Outline</h3>
				<span class="sermon_external_link"><span class="prefix scribd">Outlines :</span> Download @ Scribd (<a href="http://scribd.com/fbcprescott/" title="Download Sermon Outlines @ Scribd.com">http://scribd.com/fbcprescott/</a>)</span>
				<?php the_field('_endvr_sermon_doc'); ?>
			</div>
		<?php } ?>
		<br />
		<hr />
		<?php if (get_field('_endvr_sermon_ref')) { ?>
			<div class="media esv span12">
				<h3 class="sermon_scrip"><i class="icon-book"></i>&nbsp; Sermon Scripture</h3>
				<span class="sermon_external_link"><span class="prefix esv">Online Bible :</span> Read @ ESV (<a href="http://esvbible.org" title="Read the Bible Online @ ESVBible.org">http://esvbible.org/</a>)</span>											
				<?php $mb_sermon_scrip = the_field('_endvr_sermon_ref');
				 echo do_shortcode($mb_sermon_scrip);
				?>
			</div>
		<?php } ?>
		
	<?php tha_content_bottom(); ?>
</div><!-- /#main -->	