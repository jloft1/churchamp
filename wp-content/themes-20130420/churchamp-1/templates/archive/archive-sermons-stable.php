<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title">Sermon Directory</span><br>
					<span class="feature-subtitle">Browse our archive of Sunday Messages</span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-search span4 visible-desktop">
				<?php get_template_part('templates/meta/searchform'); ?>
			</div><!-- /.feature-search -->
			<div class="feature-link span4 visible-desktop">
				Click on title for audio/video
			</div><!-- /.feature-link			
		<?php tha_feature_bottom(); ?>	
		</div><!-- /.row -->
	</div><!-- /.container -->		
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span12'); ?> role="main">
	<div class="main-inner">

		<?php // requires Cagtegory Order and Taxonomy Terms Order plugin @source: http://wordpress.org/extend/plugins/taxonomy-terms-order/
		$args = array( 'orderby' => 'term_order', 'order' => 'DESC' );
		$tax_sermon_series = get_terms( 'sermonseries', $args ); foreach ($tax_sermon_series as $series) { 	
		?>
		
			<?php
			$series_link = '<a href="/sermons/' . $series->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series->name) . '">' . $series->name . '</a>';
			?>
			
			<h2><?php echo $series_link; ?></h2><br>
			
			<?php
			$ss = $series->slug;
			$ws_grid_columns = 4;
			$ws_span_size = ws_grid_class( $ws_grid_columns );
			$posts_per_page = -1;
			$query_string = array(
			'sermonseries' => $ss,
			'posts_per_page' => -1,
			'orderby' => 'date', 
			'order' => 'DESC'
			);
			query_posts( $query_string );
			$ws_item_counter = 1;
			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post();
					if( $ws_item_counter == 1 ) ws_open_row();
					get_template_part( 'templates/grids/grid-sermons' );
					if( $ws_item_counter % $ws_grid_columns == 0 ) ws_close_row();
					if( $ws_item_counter % $ws_grid_columns == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
					$ws_item_counter++;
				}
				if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
			} else {
				echo '<p>Apologies, but there are no sermons to display.</p>';
			}
			?>
		
		<?php } ?>

	</div><!-- /.main-inner -->		
</div><!-- /.main -->

<?php get_template_part('templates/structure/sidebar'); ?>

<?php get_template_part('templates/structure/content-bottom-after'); ?>