<?php
$speaker = $wp_query->queried_object;
$speaker_link = '<a href="/staff/' . $speaker->slug . '" title="' . sprintf(__('View the Staff Profile for this Speaker: %s', 'my_localization_domain'), $speaker->name) . '">' . $speaker->name . '</a>';
?>
		
<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title"><?php echo $speaker_link; ?></span><br>
					<span class="feature-subtitle">Sermon Speaker Archive</span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-menu span4 visible-desktop">
				<span class="feature-subtitle">Find a Sermon by&hellip;</span><br>
				<?php get_template_part('templates/menus/menu-sermons'); ?>
			</div><!-- /.feature-menu -->				
		<?php tha_feature_bottom(); ?>	
		</div><!-- /.row -->
	</div><!-- /.container -->		
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span12'); ?> role="main">
	<div class="main-inner">
	
		<div class="row hidden-desktop">
			
			<div class="span12 feature-menu">
				<span class="feature-subtitle">Find a Sermon by&hellip;</span><br class="visible-phone"> <?php get_template_part('templates/menus/menu-sermons'); ?>
			</div><!-- /.feature-menu -->
		
		</div><!-- /.row -->
		
		<hr class="hidden-desktop">	
	
		<h3>Sermons Preached by <?php echo $speaker->name; ?></h3>
		
		<?php
		$args_grid = array( 'orderby' => 'term_order', 'order' => 'DESC', 'number' => '100' ); // note the magic of exclude... offset did not work
		$tax_sermon_series_grid = get_terms( 'sermonseries', $args_grid ); foreach ($tax_sermon_series_grid as $series_grid) { 	
		?>
		
			<?php
			$series_grid_link = '<a href="/sermons/' . $series_grid->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series_grid->name) . '">' . $series_grid->name . '</a>';
			?>
			
			<hr>
			
			<h2><?php echo $series_grid_link; ?></h2><br>
			
			<?php
			$ss_g = $series_grid->slug;
			$speaker_g = $speaker->slug;
			$ws_grid_columns_ss = 3;
			$ws_span_size_ss = ws_grid_class( $ws_grid_columns_ss );
			$ss_g_query_string = array(
			'sermonseries' => $ss_g,
			'sermonspeaker' => $speaker_g,
			'posts_per_archive_page' => 100, // offset will not work unless this is a postive integer
			'orderby' => 'date', 
			'order' => 'DESC'
			);
			$ss_grid = new WP_Query( $ss_g_query_string );
			$ws_item_counter = 1;
			if ($ss_grid->have_posts()) {
				while ($ss_grid->have_posts()) {
					$ss_grid->the_post(); $do_not_duplicate = $post->ID;
					if( $ws_item_counter == 1 ) ws_open_row();
					get_template_part( 'templates/grids/grid-sermons' );
					if( $ws_item_counter % $ws_grid_columns_ss == 0 ) ws_close_row();
					if( $ws_item_counter % $ws_grid_columns_ss == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
					$ws_item_counter++;
				}
				if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
			} else {
				echo '<p>'.$speaker->name.' did not preach any sermons in this series.</p>';
			}
			?>
		
		<?php } ?>

	</div><!-- /.main-inner -->		
</div><!-- /.main -->

<?php get_template_part('templates/structure/sidebar'); ?>

<?php get_template_part('templates/structure/content-bottom-after'); ?>