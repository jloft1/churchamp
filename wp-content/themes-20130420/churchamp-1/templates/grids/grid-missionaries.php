<?php
/**
 * The template used for displaying missionaries in a grid.
 */
$ws_grid_columns_md = 4;
$ws_span_size_md = ws_grid_class( $ws_grid_columns_md ); 
?>
<div class="grid-item <?php echo $ws_span_size_md; ?>">
	<a class="gi-anchor well" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>					
			<img class="gi-img" src="<?php the_field('_endvr_missionary_photo_thumb'); ?>" alt="<?php the_title(); ?> <?php the_field('_endvr_missionary_role'); ?>">
			<header class="gi-heading">
				<h3 class="gi-title"><?php the_title(); ?></h3>
				<ul class="gi-tagline">
					<li><?php the_field('_endvr_missionary_org'); ?></li>
					<!--<li><?php the_field('_endvr_missionary_role'); ?></li>-->
					<li><?php the_field('_endvr_missionary_location'); ?></li>
				</ul>
			</header>
		</article><!-- #post-<?php the_ID(); ?> -->
	</a><!-- end anchor -->
</div><!-- .grid-item (end) -->