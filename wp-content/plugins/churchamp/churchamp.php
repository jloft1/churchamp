<?php
/**
 * Plugin Name: 	Endeavr - ChurchAmp
 * Plugin URI: 	http://churchamp.com/plugins/
 * Description: 	The Core ChurchAmp Plugin. It activates its essential features.
 * Version: 		1.0.0
 * Author: 		Endeavr Media (Jason Loftis / jLoft)
 * Author URI: 	http://endeavr.com
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * The plugin activation class utilized by ChurchAmp is TGM-Plugin-Activation by
 * Thomas Griffin <thomas@thomasgriffinmedia.com> + Gary Jones <gamajo@gamajo.com>
 * @example       	https://github.com/thomasgriffin/TGM-Plugin-Activation
 *
 * @package		churchamp
 * @version		1.0.0
 * @since			1.0.0
 * @author		Endeavr Media <support@endeavr.com>
 * @copyright		Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link			http://churchamp.com/plugins/core
 * @license	   	http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 */

/* Define the ChurchAmp Plugin path. */
define( 'ENDVR_CA_PATH', trailingslashit( plugin_dir_path( __FILE__ ) ) );

/* Include the TGM_Plugin_Activation class. */
require_once ( ENDVR_CA_PATH . 'tgmpa/class-tgm-plugin-activation.php' );

add_action( 'tgmpa_register', 'endvr_register_required_plugins_churchamp' );

/* Register the required plugins for this theme. */
function endvr_register_required_plugins_churchamp() {

	/* Array of plugin arrays. Required keys are name and slug.If the source is NOT from the .org repo, then source is also required. */
	$plugins = array(

		array(
			'name' 					=> 'MP6',
			'slug' 					=> 'mp6',
			'required' 				=> true,
		),
		array(
			'name'     				=> 'Endeavr Dashboard',
			'slug'     				=> 'endeavr-dashboard',
			'source'   				=> ENDVR_CA_PATH . 'plugins/endeavr-dashboard.zip',
			'required' 				=> true,
			'version' 				=> '3.0.0',
			'force_activation' 			=> true,
			'force_deactivation' 		=> true,
			'external_url' 			=> '',
		),
		array(
			'name'     				=> 'Advanced Custom Fields Lite',
			'slug'     				=> 'acf-lite',
			'source'   				=> ENDVR_CA_PATH . 'plugins/acf-lite.zip',
			'required' 				=> true,
			'version' 				=> '4.0.2',
			'force_activation' 			=> true,
			'force_deactivation' 		=> true,
			'external_url' 			=> '',
		),
		array(
			'name'     				=> 'ChurchAmp Staff',
			'slug'     				=> 'churchamp-staff',
			'source'   				=> ENDVR_CA_PATH . 'plugins/churchamp-staff.zip',
			'required' 				=> true,
			'version' 				=> '5.0.0',
			'force_activation' 			=> true,
			'force_deactivation' 		=> true,
			'external_url' 			=> '',
		),
		array(
			'name'     				=> 'ChurchAmp Ministries',
			'slug'     				=> 'churchamp-ministries',
			'source'   				=> ENDVR_CA_PATH . 'plugins/churchamp-ministries.zip',
			'required' 				=> true,
			'version' 				=> '5.0.0',
			'force_activation' 			=> true,
			'force_deactivation' 		=> true,
			'external_url' 			=> '',
		),
		array(
			'name'     				=> 'ChurchAmp Sermons',
			'slug'     				=> 'churchamp-sermons',
			'source'   				=> ENDVR_CA_PATH . 'plugins/churchamp-sermons.zip',
			'required' 				=> true,
			'version' 				=> '5.0.0',
			'force_activation' 			=> true,
			'force_deactivation' 		=> true,
			'external_url' 			=> '',
		),
		array(
			'name'     				=> 'ChurchAmp Events',
			'slug'     				=> 'churchamp-events',
			'source'   				=> ENDVR_CA_PATH . 'plugins/churchamp-events.zip',
			'required' 				=> true,
			'version' 				=> '5.0.0',
			'force_activation' 			=> true,
			'force_deactivation' 		=> true,
			'external_url' 			=> '',
		),
		array(
			'name'     				=> 'ChurchAmp Widgets',
			'slug'     				=> 'churchamp-widgets',
			'source'   				=> ENDVR_CA_PATH . 'plugins/churchamp-widgets.zip',
			'required' 				=> true,
			'version' 				=> '5.0.0',
			'force_activation' 			=> true,
			'force_deactivation' 		=> true,
			'external_url' 			=> '',
		),
		array(
			'name'     				=> 'ChurchAmp Missions',
			'slug'     				=> 'churchamp-missions',
			'source'   				=> ENDVR_CA_PATH . 'plugins/churchamp-missions.zip',
			'required' 				=> true,
			'version' 				=> '5.0.0',
			'force_activation' 			=> true,
			'force_deactivation' 		=> true,
			'external_url' 			=> '',
		),
		array(
			'name'     				=> 'ChurchAmp Sundays',
			'slug'     				=> 'churchamp-sundays',
			'source'   				=> ENDVR_CA_PATH . 'plugins/churchamp-sundays.zip',
			'required' 				=> true,
			'version' 				=> '5.0.0',
			'force_activation' 			=> true,
			'force_deactivation' 		=> true,
			'external_url' 			=> '',
		),
		array(
			'name' 					=> 'ESV Bible Shortcode for WordPress',
			'slug' 					=> 'esv-bible-shortcode-for-wordpress',
			'required' 				=> true,
		),
		array(
			'name' 					=> 'Google Doc Embedder',
			'slug' 					=> 'google-document-embedder',
			'required' 				=> true,
		),
		array(
			'name' 					=> 'Responsive Video Shortcodes',
			'slug' 					=> 'responsive-video-shortcodes',
			'required' 				=> true,
		),
		array(
			'name' 					=> 'Rewrite Rules Inspector',
			'slug' 					=> 'rewrite-rules-inspector',
			'required' 				=> true,
		),
		array(
			'name' 					=> 'Optimize Database After Deleting Revisions',
			'slug' 					=> 'rvg-optimize-database',
			'required' 				=> true,
		),
		array(
			'name' 					=> 'WordPress Importer',
			'slug' 					=> 'wordpress-importer',
			'required' 				=> true,
		),
		array(
			'name' 					=> 'Custom Post Type Archives in Nav Menus',
			'slug' 					=> 'cpt-archives-in-nav-menus',
			'required' 				=> true,
		),

	);

	/* Change this to your theme text domain, used for internationalising strings */
	$theme_text_domain = 'endvr';

	/* Array of configuration settings. Amend each line as needed. */
	$config = array(
		'domain'       		=> $theme_text_domain,         	// Text domain - likely want to be the same as your theme.
		'default_path' 		=> '',                         	// Default absolute path to pre-packaged plugins
		'parent_menu_slug' 		=> 'themes.php', 				// Default parent menu slug
		'parent_url_slug' 		=> 'themes.php', 				// Default parent URL slug
		'menu'         		=> 'install-required-plugins', 	// Menu slug
		'has_notices'      		=> true,                       	// Show admin notices or not
		'is_automatic'    		=> false,					   	// Automatically activate plugins after installation or not
		'message' 			=> '',						// Message to output right before the plugins table
		'strings'      		=> array(
			'page_title'                       			=> __( 'Install Required Plugins', $theme_text_domain ),
			'menu_title'                       			=> __( 'Install Plugins', $theme_text_domain ),
			'installing'                       			=> __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
			'oops'                             			=> __( 'Something went wrong with the plugin API.', $theme_text_domain ),
			'notice_can_install_required'     				=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
			'notice_can_install_recommended'				=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_install'  					=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
			'notice_can_activate_required'    				=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_can_activate_recommended'				=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_activate' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
			'notice_ask_to_update' 						=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_update' 						=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
			'install_link' 					  			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
			'activate_link' 				  			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
			'return'                           			=> __( 'Return to Required Plugins Installer', $theme_text_domain ),
			'plugin_activated'                 			=> __( 'Plugin activated successfully.', $theme_text_domain ),
			'complete' 								=> __( 'All plugins installed and activated successfully. %s', $theme_text_domain ), // %1$s = dashboard link
			'nag_type'								=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
		)
	);

	tgmpa( $plugins, $config );

}